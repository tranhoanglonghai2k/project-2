/*
 * Created Date: 10-10-2022, 9:44:16 pm
 * Author: Peter
 * Email: phantrung696@gmail.com
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

// import { flatten } from 'flattenjs'
import { getAuthTokenSession, getDataSession, getLangSession, refreshPage } from '@Core/helper/Session'
import i18n from 'i18next'
import Backend from 'i18next-xhr-backend'
import { useCallback, useEffect } from 'react'
import { initReactI18next } from 'react-i18next'
// import { i18nextPlugin } from 'translation-check'
const langSession = getLangSession()
const userLang = navigator.language || navigator.userLanguage

console.log('============= userLang', userLang?.split('-')?.shift())
console.log('============= langSession', langSession)

i18n
	// load translation using xhr -> see /public/locales
	// learn more: https://github.com/i18next/i18next-xhr-backend
	.use(Backend)
	// pass the i18n instance to react-i18next.
	.use(initReactI18next)
	// init i18next
	// for all options read: https://www.i18next.com/overview/configuration-options
	// .use(i18nextPlugin)
	.init({
		backend: {
			loadPath: () => {
				return '/locales/{{lng}}/{{ns}}.json'
			}
		},
		preload: ['ja', 'en'],
		fallbackLng: langSession
			? langSession?.lang?.code?.toLowerCase()
			: userLang?.split('-')?.shift()?.toLowerCase() === 'en' ||
			  userLang?.split('-')?.shift()?.toLowerCase() === 'ja'
			? userLang?.split('-')?.shift()?.toLowerCase()
			: 'en',
		lng: langSession
			? langSession?.lang?.code?.toLowerCase()
			: userLang?.split('-')?.shift()?.toLowerCase() === 'en' ||
			  userLang?.split('-')?.shift()?.toLowerCase() === 'ja'
			? userLang?.split('-')?.shift()?.toLowerCase()
			: 'en',
		debug: false,
		ns: ['common'],
		defaultNS: 'common',
		interpolation: {
			escapeValue: false // not needed for react as it escapes by default
		},
		saveMissing: true,
		appendNamespaceToMissingKey: true,
		partialBundledLanguages: true
	})

export default i18n
