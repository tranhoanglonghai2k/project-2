/*
 * Created Date: 07-10-2022, 10:41:54 am
 * Author: Peter
 * Email: phantrung696@gmail.com
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import { createBrowserRouter, Navigate } from 'react-router-dom'
import Page404 from './Cms/components/Error/Page404'
import React from 'react'
import AdminCmsLayout from './Cms/components/Layout/AdminCmsLayout'
import { isUserLogin } from '@Core/helper/Session'
const LazyChooseType = React.lazy(() => import('./Cms/pages/Auth/ChooseType'))
const LazyLogin = React.lazy(() => import('./Cms/pages/Auth/Login'))
const LazyResetPassword = React.lazy(() => import('./Cms/pages/Auth/ResetPassword'))
const LazyForgotPassword = React.lazy(() => import('./Cms/pages/Auth/ForgotPassword'))
const LazyConfirmForgotPassword = React.lazy(() => import('./Cms/pages/Auth/ForgotPassword/ConfirmForgotPassword'))
const LazyScheduleCalendar = React.lazy(() => import('./Cms/pages/ScheduleCalendar'))
const LazySetting = React.lazy(() => import('./Cms/pages/Setting'))
const LazyLessonList = React.lazy(() => import('./Cms/pages/Lesson/ListLesson'))
const LazyLessonDetail = React.lazy(() => import('./Cms/pages/Lesson/DetailLesson'))
const LazyLessonEdit = React.lazy(() => import('./Cms/pages/Lesson/EditLesson'))
const LazyStudentList = React.lazy(() => import('./Cms/pages/Student/ListStudent'))
const LazyStudentDetail = React.lazy(() => import('./Cms/pages/Student/DetailStudent'))
const LazyStudentEdit = React.lazy(() => import('./Cms/pages/Student/EditStudent'))
const DetailLessonListStudent = React.lazy(() => import('./Cms/pages/LessonListStudent/DetailLessonListStudent'))
const RequestFormPage = React.lazy(() => import('./Cms/pages/LessonListStudent/RequestFormPage'))
const LazyLessonStudent = React.lazy(() => import('./Cms/pages/LessonListStudent'))
const LazyReserveLessonList = React.lazy(() => import('./Cms/pages/ReserveLesson/ListReserveLesson'))
const LazyReserveLessonDetail = React.lazy(() => import('./Cms/pages/ReserveLesson/DetailReserveLesson'))
const LazyReserveLessonEdit = React.lazy(() => import('./Cms/pages/ReserveLesson/EditReserveLesson'))
const LazyExecutiveAccountList = React.lazy(() => import('./Cms/pages/ExecutiveAccount/ListExecutiveAccount'))
const LazyExecutiveAccountDetail = React.lazy(() => import('./Cms/pages/ExecutiveAccount/DetailExecutiveAccount'))
const LazyExecutiveAccountEdit = React.lazy(() => import('./Cms/pages/ExecutiveAccount/EditExecutiveAccount'))
const LazyClassroomList = React.lazy(() => import('./Cms/pages/Classroom/ListClassroom'))
const LazyClassroomDetail = React.lazy(() => import('./Cms/pages/Classroom/DetailClassroom'))
const LazyTeacherList = React.lazy(() => import('./Cms/pages/Teacher/ListTeacher'))
const LazyTeacherDetail = React.lazy(() => import('./Cms/pages/Teacher/DetailTeacher'))
const LazyTeacherEdit = React.lazy(() => import('./Cms/pages/Teacher/EditTeacher'))
const LazyDetailLessonTeacher = React.lazy(() => import('./Cms/pages/LessonTeacher/DetailLessonTeacher'))
const LazyLessonTeacher = React.lazy(() => import('./Cms/pages/LessonTeacher'))
const LazyRequestFormTeacher = React.lazy(() => import('./Cms/pages/LessonTeacher/RequestFormTeacher'))
const LazyInfoAccount = React.lazy(() => import('./Cms/pages/InfoAccount'))

export const appRouterConfig = createBrowserRouter([
	{
		// path: '/cms',
		// element: <AdminCmsLayout />,
		children: [
			{
				path: '',
				element: <Navigate to="choose-type" />
			},
			{
				path: 'choose-type',
				element: <LazyChooseType />
			},
			{
				path: 'auth',
				children: [
					{
						path: 'login/:type',
						element: <LazyLogin />
					},
					{
						path: 'reset-password/:type',
						element: <LazyResetPassword />
					},
					{
						path: 'forgot-password/:type',
						element: <LazyForgotPassword />
					},
					{
						path: 'forgot-password/confirm/:type',
						element: <LazyConfirmForgotPassword />
					}
				]
			},

			// route for student
			{
				path: 'user',
				element: <AdminCmsLayout hasLeftMenu={false} type="user" />,
				loader: () => isUserLogin(),
				children: [
					{
						path: 'info-account',
						element: <LazyInfoAccount type="user" />
					},
					{
						path: 'schedule-calendar',
						element: <LazyScheduleCalendar type="user" />
					},
					{
						path: 'setting',
						element: <LazySetting type="user" />
					},
					{
						path: 'lesson-list-student',
						children: [
							{
								path: '',
								element: <LazyLessonStudent />
							},
							{
								path: 'detail/:id',
								element: <DetailLessonListStudent />
							}
						]
					},
					{
						path: 'form',
						element: <RequestFormPage />
					}
				]
			},

			// route for teacher
			{
				path: 'teacher',
				element: <AdminCmsLayout hasLeftMenu={false} type="teacher" />,
				loader: () => isUserLogin(),
				children: [
					{
						path: 'info-account',
						element: <LazyInfoAccount type="teacher" />
					},
					{
						path: 'schedule-calendar',
						element: <LazyScheduleCalendar type="teacher" />
					},
					{
						path: 'setting',
						element: <LazySetting type="teacher" />
					},
					{
						path: 'lesson-teacher',
						children: [
							{
								path: '',
								element: <LazyLessonTeacher />
							},
							{
								path: 'detail/:id',
								element: <LazyDetailLessonTeacher />
							}
						]
					},
					{
						path: 'form',
						element: <LazyRequestFormTeacher />
					}
				]
			},

			// route for admin
			{
				path: 'admin',
				element: <AdminCmsLayout />,
				loader: () => isUserLogin(),
				children: [
					{
						path: 'lesson',
						children: [
							{
								path: '',
								element: <LazyLessonList />
							},
							{
								path: 'detail/:id',
								element: <LazyLessonDetail />
							},
							{
								path: 'edit/:id',
								element: <LazyLessonEdit />
							}
						]
					},
					{
						path: 'student',
						children: [
							{
								path: '',
								element: <LazyStudentList />
							},
							{
								path: 'edit/:id',
								element: <LazyStudentEdit />
							},
							{
								path: 'view/:id',
								element: <LazyStudentDetail />
							}
						]
					},
					{
						path: 'reserve-lesson',
						children: [
							{
								path: '',
								element: <LazyReserveLessonList />
							},
							{
								path: 'view/:id',
								element: <LazyReserveLessonDetail />
							},
							{
								path: 'edit/:id',
								element: <LazyReserveLessonEdit />
							}
						]
					},
					{
						path: 'executive-account',
						children: [
							{
								path: '',
								element: <LazyExecutiveAccountList />
							},
							{
								path: 'detail/:id',
								element: <LazyExecutiveAccountDetail />
							},
							{
								path: 'edit/:id',
								element: <LazyExecutiveAccountEdit />
							}
						]
					},
					{
						path: 'classroom',
						children: [
							{
								path: '',
								element: <LazyClassroomList />
							},
							{
								path: 'edit/:id',
								element: <LazyClassroomDetail />
							}
						]
					},
					{
						path: 'teacher',
						children: [
							{
								path: '',
								element: <LazyTeacherList />
							},
							{
								path: 'view/:id',
								element: <LazyTeacherDetail />
							},
							{
								path: 'edit/:id',
								element: <LazyTeacherEdit />
							}
						]
					}
				]
			},
			{
				path: '*',
				element: <Page404 />
			}
		]
	},
	{
		path: '*',
		element: <Page404 />
	}
])
