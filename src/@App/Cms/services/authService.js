import { env } from '@App/env'
import BaseService from '@Core/api/BaseService'

class Auth extends BaseService {
	// BASE_URL = '/'
	BASE_URL = env.CMS_BASE_URL

	BASE_ENDPOINT = '/api/admin/login'

	constructor(params) {
		super(params)
		this.setRequest()
		// this.createFactory(eventFactory)
		// this.setMockAdapter()
	}

	csrf = params => {
		const endpoint = '/api/resource/sanctum/csrf-cookie'
		return this.request.get(endpoint, { params })
	}

	login = (data, type) => {
		const endpoint = `/api/${type}/login`
		return this.request.post(endpoint, data)
	}

	logout = type => {
		const endpoint = `/api/${type}/v1/logout`
		return this.request.get(endpoint)
	}

	logoutAdmin = () => {
		const endpoint = `/api/v1/logout`
		return this.request.get(endpoint)
	}
}

export const authService = new Auth()
