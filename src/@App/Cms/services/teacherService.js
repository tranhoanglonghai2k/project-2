/*
 * Created Date: 16-12-2022, 10:46:04 am
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import { env } from '@App/env'
import BaseService from '@Core/api/BaseService'

class Teacher extends BaseService {
	BASE_URL = env.CMS_BASE_URL

	BASE_ENDPOINT = '/api/v1/teacher'

	constructor(props) {
		super(props)
		this.setRequest()
	}

	selectByOptions = params => {
		const endpoint = '/api/v1/select-teachers'

		return this.request.get(endpoint, { params })
	}
}

export const teacherService = new Teacher()
