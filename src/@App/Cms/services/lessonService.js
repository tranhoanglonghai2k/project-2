/*
 * Created Date: 14-12-2022, 9:37:40 am
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import { env } from '@App/env'
import BaseService from '@Core/api/BaseService'
import { lessonFactory } from './factory/lessonFactory'

class Lesson extends BaseService {
	BASE_URL = env.CMS_BASE_URL

	BASE_ENDPOINT = '/api/v1/lesson'

	constructor(props) {
		super(props)
		this.setRequest()
		// this.createFactory(lessonFactory)
		// this.setMockAdapter()
	}

	submitLesson = data => {
		const endpoint = `/api/user/v1/reservation-lesson`
		return this.request.post(endpoint, data)
	}

	updateLesson = (data, id) => {
		const endpoint = `/api/teacher/v1/lesson/${id}/status`
		return this.request.put(endpoint, data)
	}
}

export const lessonService = new Lesson()
