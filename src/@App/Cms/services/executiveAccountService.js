/*
 * Created Date: 15-12-2022, 4:10:58 pm
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import { env } from '@App/env'
import BaseService from '@Core/api/BaseService'

class ExecutiveAccount extends BaseService {
	BASE_URL = env.CMS_BASE_URL

	BASE_ENDPOINT = '/api/v1/admin'

	constructor(props) {
		super(props)
		this.setRequest()
	}
}

export const executiveAccountService = new ExecutiveAccount()
