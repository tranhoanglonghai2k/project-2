/*
 * Created Date: 15-12-2022, 11:03:55 am
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import { env } from '@App/env'
import BaseService from '@Core/api/BaseService'

class ReserveLesson extends BaseService {
	BASE_URL = env.CMS_BASE_URL

	BASE_ENDPOINT = '/api/v1/reservation-lesson'

	constructor(props) {
		super(props)
		this.setRequest()
	}
}

export const reserveLessonService = new ReserveLesson()
