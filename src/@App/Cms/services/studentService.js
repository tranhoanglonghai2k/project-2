/*
 * Created Date: 14-12-2022, 4:37:50 pm
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import { env } from '@App/env'
import BaseService from '@Core/api/BaseService'

class Student extends BaseService {
	BASE_URL = env.CMS_BASE_URL

	BASE_ENDPOINT = '/api/v1/user'

	constructor(props) {
		super(props)
		this.setRequest()
		// this.setMockAdapter()
	}

	getAttendingTime = userId => {
		const endpoint = `/api/user/v1/attending-time-user/${userId}`
		return this.request.get(endpoint)
	}
}

export const studentService = new Student()
