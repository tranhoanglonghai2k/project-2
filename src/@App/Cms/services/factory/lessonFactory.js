/*
 * Created Date: 14-12-2022, 9:40:02 am
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import BaseFactory from '@Core/api/Factory'

export const lessonFactory = new BaseFactory()
	.setFactoryModel({
		id: i => {
			return i
		},
		name: i => {
			return `名前 ${i}`
		},
		start_date: i => {
			return new Date()
		},
		from_time: i => {
			return new Date()
		},
		to_time: i => {
			return new Date()
		},
		teacher: i => {
			return '担当講師 ' + i
		},
		status: i => {
			return Math.round(Math.random() * 3)
		}
	})
	.makeData(20)
