/*
 * Created Date: 12-12-2022, 10:25:55 pm
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import BaseFactory from '@Core/api/Factory'

export const scheduleCalendarFactory = new BaseFactory()
	.setData({
		id: i => {
			return i
		},
		title: i => {
			return `All Day Event very long title ${i}`
		},
		allDay: i => {
			return i % 15
		},
		start: i => {
			return new Date(2022, Math.floor(Math.random() * 12) + 1, Math.floor(Math.random() * 30) + 1)
		},
		end: i => {
			return new Date(2022, Math.floor(Math.random() * 12) + 1, Math.floor(Math.random() * 30) + 1)
		}
	})
	.makeData(20)
