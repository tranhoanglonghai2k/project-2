/*
 * Created Date: 16-12-2022, 11:14:37 pm
 * Author: DESKTOP-SRKKG3M\Hello Keys
 * Email: you@you.you
 * -----
 * Last Modified: Fri Dec 16 2022
 * Modified By: DESKTOP-SRKKG3M\Hello Keys
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import {env} from '@App/env'
import BaseService from '@Core/api/BaseService'

class LessonListStudent extends BaseService {
    BASE_URL ='/'

    BASE_ENDPOINT = '/api/v1/lesson-list-students'

    constructor(props) {
        super(props)
        this.setRequest()
    }
}

export const lessonListStudent = new LessonListStudent()