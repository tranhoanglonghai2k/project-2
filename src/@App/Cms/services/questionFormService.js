/*
 * Created Date: 22-12-2022, 11:15:12 pm
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import { env } from '@App/env'
import BaseService from '@Core/api/BaseService'

class QuestionForm extends BaseService {
	BASE_URL = env.CMS_BASE_URL

	constructor(props) {
		super(props)
		this.setRequest()
	}

	sendQuestion = (data, type) => {
		const endpoint = `/api/${type}/v1/question`

		return this.request.post(endpoint, data)
	}
}

export const questionFormService = new QuestionForm()
