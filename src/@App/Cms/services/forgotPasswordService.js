/*
 * Created Date: 21-12-2022, 2:45:05 pm
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import { env } from '@App/env'
import BaseService from '@Core/api/BaseService'

class ForgotPassword extends BaseService {
	BASE_URL = env.CMS_BASE_URL

	// BASE_ENDPOINT = '/api/reset-password'

	constructor(params) {
		super(params)
		this.setRequest()
	}

	sendEmail = (data, type) => {
		const endpoint = `/api/${type}/reset-password`
		return this.request.post(endpoint, data)
	}

	saveResetPassword = (data, type) => {
		const endpoint = `/api/${type}/edit-password`
		return this.request.post(endpoint, data)
	}
}

export const forgotPasswordService = new ForgotPassword()
