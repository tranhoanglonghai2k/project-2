/*
 * Created Date: 16-12-2022, 9:36:54 am
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import { env } from '@App/env'
import BaseService from '@Core/api/BaseService'

class Classroom extends BaseService {
	BASE_URL = env.CMS_BASE_URL

	BASE_ENDPOINT = '/api/v1/attending-time'

	constructor(props) {
		super(props)
		this.setRequest()
	}
}

export const classroomService = new Classroom()
