/*
 * Created Date: 12-12-2022, 9:36:05 pm
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import { env } from '@App/env'
import BaseService from '@Core/api/BaseService'

class ResetPassword extends BaseService {
	BASE_URL = env.CMS_BASE_URL

	constructor(params) {
		super(params)
		this.setRequest()
	}

	sendPasswordReset = (data, type) => {
		const endpoint = `/api/${type}/set-password`

		return this.request.post(endpoint, data)
	}
}

export const resetPasswordService = new ResetPassword()
