/*
 * Created Date: 13-12-2022, 9:14:44 am
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import { env } from '@App/env'
import BaseService from '@Core/api/BaseService'

class Setting extends BaseService {
	BASE_URL = env.CMS_BASE_URL
	// BASE_URL = '/'

	// BASE_ENDPOINT = '/api/v1/settings'

	constructor(props) {
		super(props)
		this.setRequest()
	}

	getSetting = (type, params) => {
		const endpoint = `/api/${type}/v1/setting`
		return this.request.get(endpoint, { params })
	}

	saveSetting = (data, type) => {
		const endpoint = `/api/${type}/v1/setting`
		return this.request.post(endpoint, data)
	}

	deleteSetting = (id, type) => {
		const endpoint = `/api/${type}/v1/setting/${id}`
		return this.request.delete(endpoint)
	}

	getTimezone = type => {
		const endpoint = `/api/${type}/v1/time-zone`
		return this.request.get(endpoint)
	}

	getLanguage = type => {
		const endpoint = `/api/${type}/v1/lang`
		return this.request.get(endpoint)
	}

	resetPassword = (data, type) => {
		const endpoint = `/api/${type}/v1/setting-${type}`
		return this.request.post(endpoint, data)
	}

	saveTimezone = (data, type) => {
		const endpoint = `/api/${type}/v1/setting-${type}-timezone`
		return this.request.post(endpoint, data)
	}

	saveLang = (data, type) => {
		const endpoint = `/api/${type}/v1/setting-${type}-lang`
		return this.request.post(endpoint, data)
	}

	getDetailAccount = type => {
		const endpoint = `/api/${type}/v1/detail-${type}`
		return this.request.get(endpoint)
	}

	saveZoomUrl = data => {
		const endpoint = '/api/teacher/v1/update-info-zoom'
		return this.request.post(endpoint, data)
	}
}

export const settingService = new Setting()
