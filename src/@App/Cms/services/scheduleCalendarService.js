/*
 * Created Date: 12-12-2022, 10:24:13 pm
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import { env } from '@App/env'
import BaseService from '@Core/api/BaseService'
import { scheduleCalendarFactory } from './factory/scheduleCalendarFactory'

class ScheduleCalendar extends BaseService {
	// BASE_URL = env.CMS_BASE_URL
	BASE_URL = '/'

	BASE_ENDPOINT = '/api/v1/schedule-calendars'

	constructor(props) {
		super(props)
		this.setRequest()
		this.createFactory(scheduleCalendarFactory)
	}
}

export const scheduleCalendarService = new ScheduleCalendar()
