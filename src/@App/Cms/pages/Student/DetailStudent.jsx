/*
 * Created Date: 14-12-2022, 4:28:36 pm
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import AdminContentPage from '@App/Cms/components/Layout/AdminContentPage'
import { TRANSLATE_CMS } from '@App/Cms/configs/constants'
import { Box, CircularProgress } from '@mui/material'
import React from 'react'
import { useTranslation } from 'react-i18next'
import { useLocation } from 'react-router-dom'
import StudentDetailContent from './components/StudentDetailContent'
import { useDetailStudent } from './hooks/useDetailStudent'
import StudentProvider from './StudentProvider'
// import PropTypes from 'prop-types'

const DetailStudent = props => {
	const { t } = useTranslation(TRANSLATE_CMS.student)
	const { student, loadingStudent } = useDetailStudent()

	return (
		<StudentProvider>
			<AdminContentPage
				pageTitle={t('title.detail_page')}
				content={
					loadingStudent ? (
						<Box className="mt-40 text-center">
							<CircularProgress />
						</Box>
					) : (
						<StudentDetailContent studentData={student} />
					)
				}
			/>
		</StudentProvider>
	)
}

// DetailStudent.defaultProps = {}

// DetailStudent.propTypes = {}

export default React.memo(DetailStudent)
