/*
 * Created Date: 14-12-2022, 4:26:37 pm
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import AdminPageProvider from '@App/Cms/components/Provider/AdminPageProvider'
import { TRANSLATE_CMS } from '@App/Cms/configs/constants'
import { studentService } from '@App/Cms/services/studentService'
import useCoreTable from '@Core/components/Table/hooks/useCoreTable'
import { errorMsg, successMsg } from '@Core/helper/Message'
import { useRequest } from 'ahooks'
import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
// import PropTypes from 'prop-types'

const StudentProvider = props => {
	const { t } = useTranslation(TRANSLATE_CMS.student)
	const requestStudents = useRequest(studentService.list, {
		manual: true,
		onError: error => errorMsg(error?.response?.data?.message)
	})

	const { runAsync: handleDeleteStudent } = useRequest(studentService.delete, {
		manual: true,
		onSuccess: res => {
			lessonTableHandler.handleFetchData()
			successMsg(t('common:message.delete_success'))
		},
		onError: error => errorMsg(error?.response?.data?.message)
	})

	const studentTableHandler = useCoreTable(requestStudents)

	useEffect(() => {
		studentTableHandler.handleFetchData()
	}, [])

	const data = {
		...props,
		studentTableHandler,
		handleDeleteStudent
	}

	return <AdminPageProvider {...data}>{props.children}</AdminPageProvider>
}

// StudentProvider.defaultProps = {}

// StudentProvider.propTypes = {}

export default React.memo(StudentProvider)
