/*
 * Created Date: 20-12-2022, 10:49:02 pm
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import { studentService } from '@App/Cms/services/studentService'
import { errorMsg } from '@Core/helper/Message'
import { useRequest } from 'ahooks'
import { useEffect } from 'react'
import { useParams } from 'react-router-dom'

export const useDetailStudent = props => {
	const { id } = useParams()

	const {
		data: student,
		run: fetchStudent,
		loading: loadingStudent
	} = useRequest(studentService.find, {
		manual: true,
		onError: error => errorMsg(error?.response?.data?.message)
	})

	useEffect(() => {
		if (id) {
			fetchStudent(id)
		}
	}, [])

	return { student, loadingStudent }
}
