/*
 * Created Date: 14-12-2022, 4:29:16 pm
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import { useAdminPageContext } from '@App/Cms/components/Provider/AdminPageProvider'
import { ROUTER_EXE, TRANSLATE_CMS } from '@App/Cms/configs/constants'
import { useOptions } from '@App/Cms/hooks/useOptions'
import { studentService } from '@App/Cms/services/studentService'
import CoreAutocomplete from '@Core/components/Input/CoreAutocomplete'
import CoreInput from '@Core/components/Input/CoreInput'
import { errorMsg, handleErrorFieldBackend, successMsg } from '@Core/helper/Message'
import Yup from '@Core/helper/Yup'
import { yupResolver } from '@hookform/resolvers/yup'
import { LoadingButton } from '@mui/lab'
import { Box, Typography } from '@mui/material'
import React from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { useNavigate } from 'react-router-dom'
// import PropTypes from 'prop-types'

const StudentForm = props => {
	const { studentTableHandler } = useAdminPageContext()
	const { studentData } = props
	const navigate = useNavigate()
	const { levelIdOptions } = useOptions()
	const { t } = useTranslation(TRANSLATE_CMS.student)

	const {
		control,
		handleSubmit,
		formState: { isSubmitting },
		watch,
		setError
	} = useForm({
		mode: 'onTouched',
		defaultValues: {
			id: studentData?.id ?? null,
			level_id: studentData?.level_id ?? null
		},
		resolver: yupResolver(
			Yup.object({
				level_id: Yup.mixed().nullable().required(t('common:validation.required'))
			})
		)
	})

	const onSubmit = handleSubmit(async data => {
		try {
			await studentService.save(data)
			navigate(ROUTER_EXE.student.list + `/view/${data.id}`)
			successMsg(t('message.edit_success'))
		} catch (error) {
			if (typeof error?.response?.data?.message === 'string') {
				errorMsg(error?.response?.data?.message)
			} else {
				handleErrorFieldBackend(error, setError)
			}
		}
	})

	return (
		<form onSubmit={onSubmit}>
			<Box className="max-w-lg mt-40 mx-auto lg:p-12">
				<Box className="w-full flex mb-20">
					<Typography className="w-full text-xl mr-20 lg:w-1/2 lg:mr-40 text-right">
						{t('label.login_id')}
					</Typography>
					<Typography className="w-full text-xl ml-20 lg:w-1/2 lg:ml-40 text-left">
						{studentData?.login_id}
					</Typography>
				</Box>
				<Box className="w-full flex mb-20">
					<Typography className="w-full text-xl mr-20 lg:w-1/2 lg:mr-40 text-right">
						{t('label.mail_address')}
					</Typography>
					<Typography className="w-full text-xl ml-20 lg:w-1/2 lg:ml-40 text-left">
						{studentData?.mail_address}
					</Typography>
				</Box>
				<Box className="w-full flex mb-20">
					<Typography className="w-full text-xl mr-20 lg:w-1/2 lg:mr-40 text-right">
						{t('label.name')}
					</Typography>
					<Typography className="w-full text-xl ml-20 lg:w-1/2 lg:ml-40 text-left">
						{studentData?.name}
					</Typography>
				</Box>
				<Box className="w-full flex mb-20">
					<Typography className="w-full text-xl mr-20 lg:w-1/2 lg:mr-40 my-auto text-right">
						{t('label.level_class')}
					</Typography>
					<CoreAutocomplete
						control={control}
						name="level_id"
						placeholder={t('placeholder.level_class')}
						className="w-full text-xl lg:w-1/2 lg:ml-40 mx-10"
						size="small"
						variant="outlined"
						valuePath="value"
						labelPath="label"
						returnValueType="enum"
						options={levelIdOptions}
					/>
				</Box>

				<Box className="flex flex-wrap lg:flex-nowrap mt-40">
					<Box className="w-full lg:w-1/3 sm:w-2/5 m-12 mx-auto px-20 sm:p-0">
						<LoadingButton
							type="submit"
							loading={isSubmitting}
							variant="contained"
							color="primary"
							size="small"
							className="w-full text-xl py-10"
						>
							{t('common:btn.update')}
						</LoadingButton>
					</Box>
				</Box>
			</Box>
		</form>
	)
}

// StudentForm.defaultProps = {}

// StudentForm.propTypes = {}

export default React.memo(StudentForm)
