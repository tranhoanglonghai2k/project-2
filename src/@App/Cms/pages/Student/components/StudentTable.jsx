/*
 * Created Date: 14-12-2022, 4:29:49 pm
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import { useAdminPageContext } from '@App/Cms/components/Provider/AdminPageProvider'
import { ROUTER_EXE, TRANSLATE_CMS } from '@App/Cms/configs/constants'
import { CoreActionDelete, CoreActionEdit, CoreActionView } from '@Core/components/Table/components/CoreTableAction'
import CoreTable, { columnHelper } from '@Core/components/Table/CoreTable'
import { Box } from '@mui/material'
import React, { useMemo } from 'react'
import { useTranslation } from 'react-i18next'
import { useNavigate } from 'react-router-dom'
import { useDataFake } from '../../Lesson/hooks/useDataFake'
import StudentTableFilter from './StudentTableFilter'
// import PropTypes from 'prop-types'

const StudentTable = props => {
	const { t } = useTranslation(TRANSLATE_CMS.student)
	const navigate = useNavigate()
	const { studentTableHandler, handleDeleteStudent } = useAdminPageContext()

	const columns = useMemo(() => {
		return [
			columnHelper.accessor('id', {
				cell: info => info.getValue(),
				header: 'ID',
				className: 'w-[10%]'
			}),
			columnHelper.accessor('login_id', {
				header: t('label.login_id')
			}),
			columnHelper.accessor('mail_address', {
				header: t('label.mail_address')
			}),
			columnHelper.accessor('name', {
				header: t('label.name')
			}),
			columnHelper.accessor('level_name', {
				header: t('label.level_class')
			}),
			columnHelper.accessor('number_remaining_lessons', {
				header: t('label.attending_time')
			}),
			columnHelper.accessor('action', {
				header: t('label.action'),
				className: 'w-[15%]',
				cell: ({ row }) => {
					const data = row.original

					return (
						<div className="flex">
							<CoreActionView onClick={() => navigate(ROUTER_EXE.student.list + `/view/${data?.id}`)} />
							{/* <CoreActionDelete onConfirmDelete={() => handleDeleteStudent(data?.id)} /> */}
						</div>
					)
				}
			})
		]
	}, [t])

	return (
		<Box className="m-12">
			<StudentTableFilter className="mb-20" />
			<CoreTable isShowPagination columns={columns} {...studentTableHandler} />
		</Box>
	)
}

// StudentTable.defaultProps = {}

// StudentTable.propTypes = {}

export default React.memo(StudentTable)
