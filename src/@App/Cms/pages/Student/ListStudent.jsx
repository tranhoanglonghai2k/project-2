/*
 * Created Date: 14-12-2022, 4:26:56 pm
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import AdminContentPage from '@App/Cms/components/Layout/AdminContentPage'
import { ROUTER_EXE, TRANSLATE_CMS } from '@App/Cms/configs/constants'
import { Box, Button } from '@mui/material'
import React from 'react'
import { useTranslation } from 'react-i18next'
import { useNavigate } from 'react-router-dom'
import StudentTable from './components/StudentTable'
import StudentProvider from './StudentProvider'
// import PropTypes from 'prop-types'

const ListStudent = props => {
	const { t } = useTranslation(TRANSLATE_CMS.student)
	const navigate = useNavigate()

	return (
		<StudentProvider>
			<AdminContentPage
				pageTitle={t('title.title_page')}
				content={
					<>
						<StudentTable />
						{/* <Box className="text-right my-40 mr-40">
							<Button
								variant="contained"
								color="primary"
								onClick={() => navigate(ROUTER_EXE.student.list + '/create/new')}
								className="px-20 w-160 text-18"
							>
								{t('common:btn.add')}
							</Button>
						</Box> */}
					</>
				}
			/>
		</StudentProvider>
	)
}

// ListStudent.defaultProps = {}

// ListStudent.propTypes = {}

export default React.memo(ListStudent)
