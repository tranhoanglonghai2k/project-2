/*
 * Created Date: 15-12-2022, 9:45:48 am
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import AdminContentPage from '@App/Cms/components/Layout/AdminContentPage'
import { TRANSLATE_CMS } from '@App/Cms/configs/constants'
import React from 'react'
import { useTranslation } from 'react-i18next'
import { useLocation } from 'react-router-dom'
import StudentForm from './components/StudentForm'
import StudentProvider from './StudentProvider'
// import PropTypes from 'prop-types'

const EditStudent = props => {
	const { t } = useTranslation(TRANSLATE_CMS.student)
	const location = useLocation()

	return (
		<StudentProvider>
			<AdminContentPage
				pageTitle={t('title.edit_page')}
				content={<StudentForm studentData={location?.state} />}
			/>
		</StudentProvider>
	)
}

// EditStudent.defaultProps = {}

// EditStudent.propTypes = {}

export default React.memo(EditStudent)
