/*
 * Created Date: 25-12-2022, 2:07:37 pm
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import { TRANSLATE_CMS } from '@App/Cms/configs/constants'
import { getAuthTokenSession } from '@Core/helper/Session'
import { Box, CircularProgress, Typography } from '@mui/material'
import React, { useEffect } from 'react'
import { useTranslation } from 'react-i18next'
import OperatingTime from '../../Setting/components/OperatingTime/OperatingTime'
import ResetPassword from '../../Setting/components/ResetPassword/ResetPassword'
import useLevelLabel from '@App/Cms/hooks/useLevelLabel'
import { settingService } from '@App/Cms/services/settingService'
import { useRequest } from 'ahooks'
import ZoomUrl from '../../Setting/components/ZoomUrl/ZoomUrl'
// import PropTypes from 'prop-types'

const InfoAccountContent = props => {
	const { t } = useTranslation(TRANSLATE_CMS.info_account)
	const { type } = props
	const infoAccount = getAuthTokenSession()
	const {
		data: detailAccount,
		run: fetchDetailAccount,
		loading: loadingDetailAccount
	} = useRequest(settingService.getDetailAccount, {
		manual: true
	})

	useEffect(() => {
		fetchDetailAccount(type)
	}, [])

	return (
		<Box className="w-full mt-40 flex">
			<Box className="w-full lg:w-1/3 mx-auto">
				<div className="w-full border mb-40 flex flex-wrap border-grey-300 rounded p-20">
					<Typography className="w-full text-24 mb-10 font-bold text-center">{t('label.info')}</Typography>

					{loadingDetailAccount ? (
						<div className="w-full flex">
							<div className="mx-auto">
								<CircularProgress />
							</div>
						</div>
					) : (
						<div className="mb-20 mx-auto text-right">
							<Typography className="w-full text-20 text-center font-bold">{`${t('label.name')}: ${
								detailAccount?.name
							}`}</Typography>
							<Typography className="w-full text-20 text-center font-bold">{`${t('label.login_id')}: ${
								detailAccount?.login_id
							}`}</Typography>
							<Typography className="w-full text-20 text-center font-bold">
								{`${t('label.mail_address')}: ${detailAccount?.mail_address}`}
							</Typography>
							{type === 'user' ? (
								<Typography className="w-full text-20 text-center font-bold">
									{`${t('label.level_class')}: ${detailAccount?.levels?.name}`}
								</Typography>
							) : type === 'teacher' ? (
								<Typography className="w-full text-20 text-center font-bold">
									{t('label.level_class')} : {detailAccount?.levels?.name}
								</Typography>
							) : null}
						</div>
					)}
				</div>

				{type === 'teacher' && (
					<>
						<OperatingTime type={type} />

						<ZoomUrl />

						<div className="w-full border mb-40 border-grey-300 rounded p-20">
							<Typography className="w-full text-24 mb-10 font-bold text-center">
								{t('label.password_update')}
							</Typography>

							<ResetPassword type={type} />
						</div>
					</>
				)}
			</Box>
		</Box>
	)
}

// InfoAccountContent.defaultProps = {}

// InfoAccountContent.propTypes = {}

export default React.memo(InfoAccountContent)
