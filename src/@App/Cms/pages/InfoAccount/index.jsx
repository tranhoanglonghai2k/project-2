/*
 * Created Date: 25-12-2022, 2:07:04 pm
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import AdminContentPage from '@App/Cms/components/Layout/AdminContentPage'
import { TRANSLATE_CMS } from '@App/Cms/configs/constants'
import React from 'react'
import { useTranslation } from 'react-i18next'
import InfoAccountContent from './components/InfoAccountContent'
// import PropTypes from 'prop-types'

const InfoAccount = props => {
	const { t } = useTranslation(TRANSLATE_CMS.info_account)

	return <AdminContentPage pageTitle={t('title.info_account')} content={<InfoAccountContent {...props} />} />
}

// InfoAccount.defaultProps = {}

// InfoAccount.propTypes = {}

export default React.memo(InfoAccount)
