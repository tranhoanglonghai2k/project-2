/*
 * Created Date: 15-12-2022, 5:09:16 pm
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import AdminContentPage from '@App/Cms/components/Layout/AdminContentPage'
import { TRANSLATE_CMS } from '@App/Cms/configs/constants'
import React from 'react'
import { Helmet } from 'react-helmet'
import { useTranslation } from 'react-i18next'
import { useLocation, useParams } from 'react-router-dom'
import LessonForm from './components/LessonForm'
import LessonProvider from './LessonProvider'
// import PropTypes from 'prop-types'

const EditLesson = props => {
	const { t } = useTranslation(TRANSLATE_CMS.lesson)
	const location = useLocation()
	const { id } = useParams()
	const isEdit = id !== 'new'

	return (
		<LessonProvider isEdit={isEdit}>
			
			<AdminContentPage
				pageTitle={isEdit ? t('title.edit_page') : t('title.add_page')}
				content={<LessonForm lessonData={location?.state} />}
			/>
		</LessonProvider>
	)
}

// EditLesson.defaultProps = {}

// EditLesson.propTypes = {}

export default React.memo(EditLesson)
