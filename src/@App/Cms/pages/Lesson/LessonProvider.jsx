/*
 * Created Date: 14-12-2022, 9:33:10 am
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import AdminPageProvider from '@App/Cms/components/Provider/AdminPageProvider'
import { TRANSLATE_CMS } from '@App/Cms/configs/constants'
import { lessonService } from '@App/Cms/services/lessonService'
import { teacherService } from '@App/Cms/services/teacherService'
import useCoreTable from '@Core/components/Table/hooks/useCoreTable'
import { errorMsg, successMsg } from '@Core/helper/Message'
import { useRequest } from 'ahooks'
import React, { useState } from 'react'
import { useEffect } from 'react'
import { useTranslation } from 'react-i18next'
// import PropTypes from 'prop-types'

const LessonProvider = props => {
	const { t } = useTranslation(TRANSLATE_CMS.lesson)

	const requestLessons = useRequest(lessonService.list, {
		manual: true,
		onError: error => errorMsg(error?.response?.data?.message)
	})

	const { runAsync: handleDeleteLesson } = useRequest(lessonService.delete, {
		manual: true,
		onSuccess: res => {
			lessonTableHandler.handleFetchData()
			successMsg(t('common:message.delete_success'))
		},
		onError: error => {
			errorMsg(error?.response?.data?.message)
		}
	})

	const { data: teacher, run: fetchTeacher } = useRequest(teacherService.list, {
		manual: true,
		onError: error => errorMsg(error)
	})
	// const { data: level, run: fetchLevel } = useRequest(teacherService.list, {
	// 	manual: true,
	// 	onError: error => errorMsg(error)
	// })

	const lessonTableHandler = useCoreTable(requestLessons)

	useEffect(() => {
		lessonTableHandler.handleFetchData()
		fetchTeacher({ per_page: 1000 })
	}, [])

	const data = {
		...props,
		lessonTableHandler,
		handleDeleteLesson,
		teacher
	}

	return <AdminPageProvider {...data}>{props.children}</AdminPageProvider>
}

// LessonProvider.defaultProps = {}

// LessonProvider.propTypes = {}

export default React.memo(LessonProvider)
