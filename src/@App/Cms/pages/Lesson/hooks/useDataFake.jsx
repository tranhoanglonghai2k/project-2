/*
 * Created Date: 14-12-2022, 10:59:51 am
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

export const useDataFake = props => {
	const lessonFakeData = [
		{
			id: 1,
			lesson_name: '日本語レッスン1',
			start_date: new Date(),
			from_time: new Date(),
			to_time: new Date(),
			teacher: '担当講師 1',
			status: '予約中',
			level_class: '日本語レベル1'
		},
		{
			id: 2,
			lesson_name: '日本語レッスン2',
			start_date: new Date(),
			from_time: new Date(),
			to_time: new Date(),
			teacher: '担当講師 2',
			status: '実施済み',
			level_class: '日本語レベル2'
		},
		{
			id: 3,
			lesson_name: '日本語レッスン3',
			start_date: new Date(),
			from_time: new Date(),
			to_time: new Date(),
			teacher: '担当講師 3',
			status: '未実施',
			level_class: '日本語レベル3'
		},
		{
			id: 4,
			lesson_name: '日本語レッスン4',
			start_date: new Date(),
			from_time: new Date(),
			to_time: new Date(),
			teacher: '担当講師 4',
			status: '予約中',
			level_class: '日本語レベル4'
		},
		{
			id: 5,
			lesson_name: '日本語レッスン5',
			start_date: new Date(),
			from_time: new Date(),
			to_time: new Date(),
			teacher: '担当講師 5',
			status: '実施済み',
			level_class: '日本語レベル5'
		},
		{
			id: 6,
			lesson_name: '日本語レッスン6',
			start_date: new Date(),
			from_time: new Date(),
			to_time: new Date(),
			teacher: '担当講師 6',
			status: '実施済み',
			level_class: '日本語レベル6'
		},
		{
			id: 7,
			lesson_name: '日本語レッスン7',
			start_date: new Date(),
			from_time: new Date(),
			to_time: new Date(),
			teacher: '担当講師 7',
			status: '予約中',
			level_class: '日本語レベル7'
		},
		{
			id: 8,
			lesson_name: '日本語レッスン8',
			start_date: new Date(),
			from_time: new Date(),
			to_time: new Date(),
			teacher: '担当講師 8',
			status: '未実施',
			level_class: '日本語レベル8'
		},
		{
			id: 9,
			lesson_name: '日本語レッスン9',
			start_date: new Date(),
			from_time: new Date(),
			to_time: new Date(),
			teacher: '担当講師 9',
			status: '未実施',
			level_class: '日本語レベル9'
		},
		{
			id: 10,
			lesson_name: '日本語レッスン10',
			start_date: new Date(),
			from_time: new Date(),
			to_time: new Date(),
			teacher: '担当講師 10',
			status: '予約中',
			level_class: '日本語レベル10'
		}
	]

	const studentFakeData = [
		{
			id: 1,
			login_id: 'Student-1',
			mail_address: 'test@test.com',
			name: 'Student 1',
			level_class: 'レベル1'
		},
		{
			id: 2,
			login_id: 'Student-2',
			mail_address: 'test@test.com',
			name: 'Student 2',
			level_class: 'レベル2'
		},
		{
			id: 3,
			login_id: 'Student-3',
			mail_address: 'test@test.com',
			name: 'Student 3',
			level_class: 'レベル3'
		},
		{
			id: 4,
			login_id: 'Student-4',
			mail_address: 'test@test.com',
			name: 'Student 4',
			level_class: 'レベル4'
		},
		{
			id: 5,
			login_id: 'Student-5',
			mail_address: 'test@test.com',
			name: 'Student 5',
			level_class: 'レベル5'
		},
		{
			id: 6,
			login_id: 'Student-6',
			mail_address: 'test@test.com',
			name: 'Student 6',
			level_class: 'レベル6'
		},
		{
			id: 1,
			login_id: 'Student-1',
			mail_address: 'test@test.com',
			name: 'Student 1',
			level_class: 'レベル1'
		},
		{
			id: 7,
			login_id: 'Student-7',
			mail_address: 'test@test.com',
			name: 'Student 7',
			level_class: 'レベル7'
		},
		{
			id: 8,
			login_id: 'Student-8',
			mail_address: 'test@test.com',
			name: 'Student 8',
			level_class: 'レベル8'
		},
		{
			id: 9,
			login_id: 'Student-9',
			mail_address: 'test@test.com',
			name: 'Student 9',
			level_class: 'レベル9'
		},
		{
			id: 10,
			login_id: 'Student-10',
			mail_address: 'test@test.com',
			name: 'Student 10',
			level_class: 'レベル10'
		}
	]

	const reverseLessonFakeData = [
		{
			id: 1,
			lesson_name: '日本語レッスン1',
			start_date: new Date(),
			from_time: new Date(),
			to_time: new Date(),
			teacher: '講師 講師',
			student: 'Student Student',
			level_class: 'レベル1',
			status: '未実施'
		},
		{
			id: 2,
			lesson_name: '日本語レッスン2',
			start_date: new Date(),
			from_time: new Date(),
			to_time: new Date(),
			teacher: '講師 講師',
			student: 'Student Student',
			level_class: 'レベル2',
			status: '予約中'
		},
		{
			id: 3,
			lesson_name: '日本語レッスン3',
			start_date: new Date(),
			from_time: new Date(),
			to_time: new Date(),
			teacher: '講師 講師',
			student: 'Student Student',
			level_class: 'レベル3',
			status: '未実施'
		},
		{
			id: 4,
			lesson_name: '日本語レッスン4',
			start_date: new Date(),
			from_time: new Date(),
			to_time: new Date(),
			teacher: '講師 講師',
			student: 'Student Student',
			level_class: 'レベル4',
			status: '予約中'
		},
		{
			id: 5,
			lesson_name: '日本語レッスン5',
			start_date: new Date(),
			from_time: new Date(),
			to_time: new Date(),
			teacher: '講師 講師',
			student: 'Student Student',
			level_class: 'レベル5',
			status: '予約中'
		},
		{
			id: 6,
			lesson_name: '日本語レッスン6',
			start_date: new Date(),
			from_time: new Date(),
			to_time: new Date(),
			teacher: '講師 講師',
			student: 'Student Student',
			level_class: 'レベル6',
			status: '実施済み'
		},
		{
			id: 7,
			lesson_name: '日本語レッスン7',
			start_date: new Date(),
			from_time: new Date(),
			to_time: new Date(),
			teacher: '講師 講師',
			student: 'Student Student',
			level_class: 'レベル7',
			status: '実施済み'
		},
		{
			id: 8,
			lesson_name: '日本語レッスン8',
			start_date: new Date(),
			from_time: new Date(),
			to_time: new Date(),
			teacher: '講師 講師',
			student: 'Student Student',
			level_class: 'レベル8',
			status: '予約中'
		},
		{
			id: 9,
			lesson_name: '日本語レッスン9',
			start_date: new Date(),
			from_time: new Date(),
			to_time: new Date(),
			teacher: '講師 講師',
			student: 'Student Student',
			level_class: 'レベル9',
			status: '実施済み'
		},
		{
			id: 10,
			lesson_name: '日本語レッスン10',
			start_date: new Date(),
			from_time: new Date(),
			to_time: new Date(),
			teacher: '講師 講師',
			student: 'Student Student',
			level_class: 'レベル10',
			status: '予約中'
		}
	]

	const executiveAccountFakeData = [
		{
			id: 1,
			login_id: 'Admin-1',
			mail_address: 'test@test.com',
			name: '事務局 事務局 1'
		},
		{
			id: 2,
			login_id: 'Admin-2',
			mail_address: 'test@test.com',
			name: '事務局 事務局 2'
		},
		{
			id: 3,
			login_id: 'Admin-3',
			mail_address: 'test@test.com',
			name: '事務局 事務局 3'
		},
		{
			id: 4,
			login_id: 'Admin-4',
			mail_address: 'test@test.com',
			name: '事務局 事務局 4'
		},
		{
			id: 5,
			login_id: 'Admin-5',
			mail_address: 'test@test.com',
			name: '事務局 事務局 5'
		},
		{
			id: 6,
			login_id: 'Admin-6',
			mail_address: 'test@test.com',
			name: '事務局 事務局 6'
		},
		{
			id: 7,
			login_id: 'Admin-7',
			mail_address: 'test@test.com',
			name: '事務局 事務局 7'
		},
		{
			id: 8,
			login_id: 'Admin-8',
			mail_address: 'test@test.com',
			name: '事務局 事務局 8'
		},
		{
			id: 9,
			login_id: 'Admin-9',
			mail_address: 'test@test.com',
			name: '事務局 事務局 9'
		},
		{
			id: 10,
			login_id: 'Admin-10',
			mail_address: 'test@test.com',
			name: '事務局 事務局 10'
		}
	]

	const classroomFakeData = [
		{
			id: 1,
			login_id: 'Student-1',
			updater_name: '',
			kind: '生徒利用',
			attendance: -1,
			student: 'Student Student',
			note: '備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考備考 備考 備考 備考備考v備考備考備考備考備考備考 備考 備考 備考 備考 備考 備考'
		},
		{
			id: 2,
			login_id: 'Student-2',
			updater_name: '',
			kind: '生徒利用',
			attendance: -1,
			student: 'Student Student',
			note: '備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考備考 備考 備考 備考備考v備考備考備考備考備考備考 備考 備考 備考 備考 備考 備考'
		},
		{
			id: 3,
			login_id: 'Student-3',
			updater_name: '事務局 事務局',
			kind: '事務局登録',
			attendance: +1,
			student: 'Student Student',
			note: '備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考備考 備考 備考 備考備考v備考備考備考備考備考備考 備考 備考 備考 備考 備考 備考'
		},
		{
			id: 4,
			login_id: 'Student-4',
			updater_name: '',
			kind: '生徒利用',
			attendance: -1,
			student: 'Student Student',
			note: '備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考備考 備考 備考 備考備考v備考備考備考備考備考備考 備考 備考 備考 備考 備考 備考'
		},
		{
			id: 5,
			login_id: 'Student-5',
			updater_name: '事務局 事務局',
			kind: '事務局登録',
			attendance: +1,
			student: 'Student Student',
			note: '備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考備考 備考 備考 備考備考v備考備考備考備考備考備考 備考 備考 備考 備考 備考 備考'
		},
		{
			id: 6,
			login_id: 'Student-6',
			updater_name: '',
			kind: '生徒利用',
			attendance: -1,
			student: 'Student Student',
			note: '備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考備考 備考 備考 備考備考v備考備考備考備考備考備考 備考 備考 備考 備考 備考 備考'
		},
		{
			id: 7,
			login_id: 'Student-7',
			updater_name: '事務局 事務局',
			kind: '事務局登録',
			attendance: +1,
			student: 'Student Student',
			note: '備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考備考 備考 備考 備考備考v備考備考備考備考備考備考 備考 備考 備考 備考 備考 備考'
		},
		{
			id: 8,
			login_id: 'Student-8',
			updater_name: '',
			kind: '生徒利用',
			attendance: -1,
			student: 'Student Student',
			note: '備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考備考 備考 備考 備考備考v備考備考備考備考備考備考 備考 備考 備考 備考 備考 備考'
		},
		{
			id: 9,
			login_id: 'Student-9',
			updater_name: '',
			kind: '生徒利用',
			attendance: -1,
			student: 'Student Student',
			note: '備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考備考 備考 備考 備考備考v備考備考備考備考備考備考 備考 備考 備考 備考 備考 備考'
		},
		{
			id: 10,
			login_id: 'Student-10',
			updater_name: '事務局 事務局',
			kind: '事務局登録',
			attendance: +1,
			student: 'Student Student',
			note: '備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考 備考備考 備考 備考 備考備考v備考備考備考備考備考備考 備考 備考 備考 備考 備考 備考'
		}
	]

	const teacherFakeData = [
		{
			id: 1,
			login_id: 'Teacher-1',
			mail_address: 'test@test.com',
			name: '講師講師',
			operation_time: ['水曜日: 12:00 ~ 18:00', '木曜日: 12:00 ~ 18:00', '祝日: 12:00 ~ 18:00']
		},
		{
			id: 2,
			login_id: 'Teacher-2',
			mail_address: 'test@test.com',
			name: '講師講師',
			operation_time: ['水曜日: 12:00 ~ 18:00', '木曜日: 12:00 ~ 18:00', '祝日: 12:00 ~ 18:00']
		},
		{
			id: 3,
			login_id: 'Teacher-3',
			mail_address: 'test@test.com',
			name: '講師講師',
			operation_time: ['水曜日: 12:00 ~ 18:00', '木曜日: 12:00 ~ 18:00', '祝日: 12:00 ~ 18:00']
		},
		{
			id: 4,
			login_id: 'Teacher-4',
			mail_address: 'test@test.com',
			name: '講師講師',
			operation_time: ['水曜日: 12:00 ~ 18:00', '木曜日: 12:00 ~ 18:00', '祝日: 12:00 ~ 18:00']
		},
		{
			id: 5,
			login_id: 'Teacher-5',
			mail_address: 'test@test.com',
			name: '講師講師',
			operation_time: ['水曜日: 12:00 ~ 18:00', '木曜日: 12:00 ~ 18:00', '祝日: 12:00 ~ 18:00']
		},
		{
			id: 6,
			login_id: 'Teacher-6',
			mail_address: 'test@test.com',
			name: '講師講師',
			operation_time: ['水曜日: 12:00 ~ 18:00', '木曜日: 12:00 ~ 18:00', '祝日: 12:00 ~ 18:00']
		},
		{
			id: 7,
			login_id: 'Teacher-7',
			mail_address: 'test@test.com',
			name: '講師講師',
			operation_time: ['水曜日: 12:00 ~ 18:00', '木曜日: 12:00 ~ 18:00', '祝日: 12:00 ~ 18:00']
		},
		{
			id: 8,
			login_id: 'Teacher-8',
			mail_address: 'test@test.com',
			name: '講師講師',
			operation_time: ['水曜日: 12:00 ~ 18:00', '木曜日: 12:00 ~ 18:00', '祝日: 12:00 ~ 18:00']
		},
		{
			id: 9,
			login_id: 'Teacher-9',
			mail_address: 'test@test.com',
			name: '講師講師',
			operation_time: ['水曜日: 12:00 ~ 18:00', '木曜日: 12:00 ~ 18:00', '祝日: 12:00 ~ 18:00']
		},
		{
			id: 10,
			login_id: 'Teacher-10',
			mail_address: 'test@test.com',
			name: '講師講師',
			operation_time: ['水曜日: 12:00 ~ 18:00', '木曜日: 12:00 ~ 18:00', '祝日: 12:00 ~ 18:00']
		}
	]

	return {
		lessonFakeData,
		studentFakeData,
		reverseLessonFakeData,
		executiveAccountFakeData,
		classroomFakeData,
		teacherFakeData
	}
}
