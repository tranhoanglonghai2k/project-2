/*
 * Created Date: 14-12-2022, 9:34:11 am
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import AdminContentPage from '@App/Cms/components/Layout/AdminContentPage'
import { TRANSLATE_CMS } from '@App/Cms/configs/constants'
import { CircularProgress } from '@mui/material'
import { Box } from '@mui/system'
import React from 'react'
import { Helmet } from 'react-helmet'
import { useTranslation } from 'react-i18next'
import { useLocation, useNavigate, useParams } from 'react-router-dom'
import LessonDetailContent from './components/LessonDetailContent'
import { useDetailLesson } from './hooks/useDetailLesson'
import LessonProvider from './LessonProvider'
// import PropTypes from 'prop-types'

const DetailLesson = props => {
	const { t } = useTranslation(TRANSLATE_CMS.lesson)
	const { lessonData, loadingLesson } = useDetailLesson()

	return (
		<LessonProvider>
			
			<AdminContentPage
				pageTitle={t('title.detail_page')}
				content={
					loadingLesson ? (
						<Box className="mt-40 text-center">
							<CircularProgress />
						</Box>
					) : (
						<LessonDetailContent lessonData={lessonData} />
					)
				}
			/>
		</LessonProvider>
	)
}

// DetailLesson.defaultProps = {}

// DetailLesson.propTypes = {}

export default React.memo(DetailLesson)
