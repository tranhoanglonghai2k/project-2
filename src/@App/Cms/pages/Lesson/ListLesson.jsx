/*
 * Created Date: 14-12-2022, 9:34:00 am
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import AdminContentPage from '@App/Cms/components/Layout/AdminContentPage'
import { ROUTER_EXE, TRANSLATE_CMS } from '@App/Cms/configs/constants'
import { Button } from '@mui/material'
import { Box } from '@mui/system'
import React from 'react'
import { useTranslation } from 'react-i18next'
import { useNavigate } from 'react-router-dom'
import LessonTable from './components/LessonTable'
import LessonProvider from './LessonProvider'
import {Helmet} from "react-helmet";
// import PropTypes from 'prop-types'

const ListLesson = props => {
	const { t } = useTranslation(TRANSLATE_CMS.lesson)
	const navigate = useNavigate()

	return (
		<LessonProvider>
			
			<AdminContentPage
				pageTitle={t('title.title_page')}
				content={
					<>
						<LessonTable />
						<Box className="text-right my-40 mr-40">
							<Button
								variant="contained"
								color="primary"
								onClick={() => navigate(ROUTER_EXE.lesson.list + '/edit/new')}
								className="px-20 w-160 text-18"
							>
								{t('common:btn.add')}
							</Button>
						</Box>
					</>
				}
			/>
		</LessonProvider>
	)
}

// ListLesson.defaultProps = {}

// ListLesson.propTypes = {}

export default React.memo(ListLesson)
