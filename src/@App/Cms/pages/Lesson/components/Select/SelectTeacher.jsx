/*
 * Created Date: 28-12-2022, 3:13:11 pm
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import { useAdminPageContext } from '@App/Cms/components/Provider/AdminPageProvider'
import { teacherService } from '@App/Cms/services/teacherService'
import CoreAutocomplete from '@Core/components/Input/CoreAutocomplete'
import { useRequest } from 'ahooks'
import moment from 'moment'
import React, { useCallback, useEffect } from 'react'
// import PropTypes from 'prop-types'

const SelectTeacher = props => {
	const { control, name, className, watchEventDate, watchStartTime, watchEndTime,watchLevelId, lessonId, ...restProps } = props

	const { data: teacher, run: getTeachers } = useRequest(teacherService.selectByOptions, {
		manual: true
	})

	console.log('============= teacher', teacher)

	useEffect(() => {
		if (watchEventDate && watchStartTime && watchEndTime && watchLevelId) {
			getTeachers({
				event_date: moment(watchEventDate).format('YYYY-MM-DD'),
				start_time: moment(watchStartTime).format('HH:mm'),
				end_time: moment(watchEndTime).format('HH:mm'),
				lesson_id: lessonId,
				level_id:watchLevelId
			})
		}
	}, [watchEventDate, watchStartTime, watchEndTime,watchLevelId])

	return (
		<CoreAutocomplete
			control={control}
			name={name}
			className={className}
			placeholder=""
			size="small"
			valuePath="id"
			labelPath="name"
			returnValueType="enum"
			disabled={!watchStartTime || !watchEndTime || !watchEventDate || !watchLevelId}
			options={teacher}
		/>
	)
}
// SelectTeacher.defaultProps = {}

// SelectTeacher.propTypes = {}

export default React.memo(SelectTeacher)
