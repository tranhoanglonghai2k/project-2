/*
 * Created Date: 14-12-2022, 10:21:28 am
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import { useAdminPageContext } from '@App/Cms/components/Provider/AdminPageProvider'
import { ROUTER_EXE, TRANSLATE_CMS } from '@App/Cms/configs/constants'
import { CoreActionDelete, CoreActionEdit, CoreActionView } from '@Core/components/Table/components/CoreTableAction'
import CoreTable, { columnHelper } from '@Core/components/Table/CoreTable'
import { IconButton, Tooltip } from '@mui/material'
import { Box } from '@mui/system'
import moment from 'moment'
import React, { useMemo } from 'react'
import { useTranslation } from 'react-i18next'
import { BiTrash } from 'react-icons/bi'
import { useNavigate } from 'react-router-dom'
import { toast } from 'react-toastify'
import { useDataFake } from '../hooks/useDataFake'
import LessonTableFilter from './LessonTableFilter'
// import PropTypes from 'prop-types'

const LessonTable = props => {
	const { t } = useTranslation(TRANSLATE_CMS.lesson)
	const navigate = useNavigate()
	const { lessonTableHandler, handleDeleteLesson } = useAdminPageContext()
	const rejectNotify = () => {
		toast.error(t('error.delete_error'));
	}
	
	const columns = useMemo(() => {
		return [
			columnHelper.accessor('id', {
				cell: info => info.getValue(),
				header: 'ID',
				className: 'w-[10%]'
			}),
			columnHelper.accessor('name', {
				header: t('label.lesson_name')
			}),
			columnHelper.accessor('event_date', {
				header: t('label.start_date'),
				cell: ({ row }) => {
					return <>{moment(row?.original?.event_date).format('YYYY年MM月DD日')}</>
				}
			}),
			columnHelper.accessor('start_time', {
				header: t('label.from_time')
				// cell: ({ row }) => {
				// 	return <>{moment(row?.original?.start_time).format('HH時mm分')}</>
				// }
			}),
			columnHelper.accessor('end_time', {
				header: t('label.to_time')
				// cell: ({ row }) => {
				// 	return <>{moment(row?.original?.end_time).format('HH時mm分')}</>
				// }
			}),
			columnHelper.accessor('teacher_name', {
				header: t('label.teacher')
			}),
			columnHelper.accessor('status', {
				header: t('label.status'),
				cell: ({ row }) => {
					return row?.original?.status === 1
						? '予約中'
						: row?.original?.status === 2
							? '予約確定'
							: row?.original?.status === 3
								? '実施済み'
								: row?.original?.status === 4
									? '未実施'
									: '未予約'
				}
			}),
			columnHelper.accessor('action', {
				header: t('label.action'),
				className: 'w-[15%]',
				cell: ({ row }) => {
					const data = row.original
					if (data?.status === 3 || data?.status === 4) {
						return (
							<div className="flex">
								<CoreActionView onClick={() => navigate(ROUTER_EXE.lesson.list + `/detail/${data?.id}`)} />
								<Tooltip title={t('error.icon_delete')}>
									<IconButton onClick={rejectNotify} color="error">
										<BiTrash />
									</IconButton>
								</Tooltip>
							</div>
						);
					}
					return (
						<div className="flex">
							<CoreActionView onClick={() => navigate(ROUTER_EXE.lesson.list + `/detail/${data?.id}`)} />
							<CoreActionDelete onConfirmDelete={() => handleDeleteLesson(data?.id)} />
						</div>
					)
				}
			})
		]
	}, [t])

	return (
		<Box className="m-12">
			<LessonTableFilter className="mb-20" />
			<CoreTable isShowPagination columns={columns} {...lessonTableHandler} />
		</Box>
	)
}

// LessonTable.defaultProps = {}

// LessonTable.propTypes = {}

export default React.memo(LessonTable)
