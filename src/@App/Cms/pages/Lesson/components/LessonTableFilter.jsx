/*
 * Created Date: 14-12-2022, 5:36:44 pm
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import { useAdminPageContext } from '@App/Cms/components/Provider/AdminPageProvider'
import { TRANSLATE_CMS } from '@App/Cms/configs/constants'
import { useOptions } from '@App/Cms/hooks/useOptions'
import CoreAutocomplete from '@Core/components/Input/CoreAutocomplete'
import CoreDatePicker from '@Core/components/Input/CoreDatePicker'
import CoreInput from '@Core/components/Input/CoreInput'
import CoreTimePicker from '@Core/components/Input/CoreTimePicker'
import { Button, Typography } from '@mui/material'
import { Box } from '@mui/system'
import clsx from 'clsx'
import moment from 'moment'
import React from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
// import PropTypes from 'prop-types'

const LessonTableFilter = props => {
	const { className } = props
	const { lessonTableHandler, teacher } = useAdminPageContext()
	const { statusOptions, teacherOptions } = useOptions()
	const { t } = useTranslation(TRANSLATE_CMS.lesson)
	const handleFilter = () => {
		const data = getValues()

		const params = {
			...data,
			event_date: data?.event_date !== null ? moment(data?.event_date).format('YYYY-MM-DD') : null,
			start_time: data?.start_time !== null ? moment(data?.start_time).format('HH:mm') : null,
			end_time: data?.end_time !== null ? moment(data?.end_time).format('HH:mm') : null,
			page: 1
		}

		lessonTableHandler.handleFetchData(params)
	}

	const { control, getValues, watch } = useForm({
		mode: 'onTouched',
		defaultValues: {
			name: '',
			event_date: null,
			start_time: null,
			end_time: null,
			teacher_id: '',
			status: null
		}
	})

	return (
		<Box className={clsx('border-1 rounded-4 border-grey-300', className)}>
			<Box className="p-8 w-full lg:flex lg:flex-wrap">
				<Box className="flex w-full lg:w-1/2 mb-20 lg:mb-10 items-start">
					<Box className="w-2/5 px-20 text-base h-full my-auto text-left lg:text-right">
						{t('label.lesson_name')}
					</Box>
					<CoreInput
						control={control}
						name="name"
						size="small"
						className="lg:w-2/3 w-3/5"
						fullWidth
						variant="outlined"
					/>
				</Box>
				<Box className="flex w-full lg:w-1/2 mb-20 lg:mb-10 items-start">
					<Box className="w-2/5 px-20 text-base h-full my-auto text-left lg:text-right">
						{t('label.start_date')}
					</Box>
					<CoreDatePicker
						control={control}
						name="event_date"
						size="small"
						className="lg:w-2/3 w-3/5"
						fullWidth
						variant="outlined"
					/>
				</Box>
				<Box className="flex w-full lg:w-1/2 mb-20 lg:mb-10 items-start">
					<Box className="w-2/5 px-20 text-base h-full my-auto text-left lg:text-right">
						{t('label.teacher')}
					</Box>
					<CoreAutocomplete
						control={control}
						name="teacher_id"
						size="small"
						className="lg:w-2/3 w-3/5"
						fullWidth
						variant="outlined"
						valuePath="id"
						labelPath="name"
						returnValueType="enum"
						options={teacher?.data}
					/>
				</Box>
				<Box className="flex w-full lg:w-1/2 mb-20 lg:mb-10 items-start">
					<Box className="w-2/5 px-20 text-base h-full my-auto text-left lg:text-right">{`${t(
						'label.from_time'
					)} ${t('label.to')} ${t('label.to_time')}`}</Box>
					<Box className="flex lg:w-2/3 w-3/5">
						<CoreTimePicker
							control={control}
							name="start_time"
							size="small"
							className="w-1/2"
							variant="outlined"
							placeholder=""
						/>

						<Box className="text-center mx-8 pt-10">
							<Typography variant="h3">{t('label.to')}</Typography>
						</Box>

						<CoreTimePicker
							control={control}
							name="end_time"
							size="small"
							className="w-1/2"
							variant="outlined"
							placeholder=""
						/>
					</Box>
				</Box>
				<Box className="flex w-full lg:w-1/2 mb-20 lg:mb-10 items-start">
					<Box className="w-2/5 px-20 text-base h-full my-auto text-left lg:text-right">
						{t('label.status')}
					</Box>
					<CoreAutocomplete
						control={control}
						name="status"
						size="small"
						className="lg:w-2/3 w-3/5"
						fullWidth
						variant="outlined"
						valuePath="value"
						labelPath="label"
						returnValueType="enum"
						options={statusOptions}
					/>
				</Box>
			</Box>
			<Box className="flex p-8 w-full">
				<Button
					variant="contained"
					color="primary"
					className="ml-auto w-2/5 p-12 sm:p-8 lg:p-5 lg:w-160 text-18"
					onClick={handleFilter}
				>
					{t('common:btn.search')}
				</Button>
			</Box>
		</Box>
	)
}

// LessonTableFilter.defaultProps = {}

// LessonTableFilter.propTypes = {}

export default React.memo(LessonTableFilter)
