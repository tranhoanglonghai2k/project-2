/*
 * Created Date: 14-12-2022, 11:13:50 am
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import AdminInput from '@App/Cms/components/Input/AdminInput'
import { useAdminPageContext } from '@App/Cms/components/Provider/AdminPageProvider'
import { ROUTER_CMS, ROUTER_EXE, TRANSLATE_CMS } from '@App/Cms/configs/constants'
import { useOptions } from '@App/Cms/hooks/useOptions'
import { lessonService } from '@App/Cms/services/lessonService'
import CoreAutocomplete from '@Core/components/Input/CoreAutocomplete'
import CoreCheckbox from '@Core/components/Input/CoreCheckbox'
import CoreDatePicker from '@Core/components/Input/CoreDatePicker'
import CoreInput from '@Core/components/Input/CoreInput'
import CoreTimePicker from '@Core/components/Input/CoreTimePicker'
import { errorMsg, handleErrorFieldBackend, successMsg } from '@Core/helper/Message'
import Yup from '@Core/helper/Yup'
import { yupResolver } from '@hookform/resolvers/yup'
import { LoadingButton } from '@mui/lab'
import { Button, Typography } from '@mui/material'
import { Box } from '@mui/system'
import moment from 'moment'
import React from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { useNavigate } from 'react-router-dom'
import SelectTeacher from './Select/SelectTeacher'
// import PropTypes from 'prop-types'

const LessonForm = props => {
	const { lessonTableHandler, isEdit, teacher } = useAdminPageContext()
	const navigate = useNavigate()
	const { t } = useTranslation(TRANSLATE_CMS.lesson)
	const { eventWeekOptions, statusOptions, levelIdOptions } = useOptions()
	const { lessonData } = props

	const {
		control,
		handleSubmit,
		watch,
		formState: { isSubmitting },
		setError
	} = useForm({
		mode: 'onTouched',
		defaultValues: {
			id: lessonData?.id ?? null,
			name: lessonData?.name ?? '',
			// content: lessonData?.content ?? '',
			event_date: lessonData?.event_date ? new Date(lessonData.event_date) : null,
			// event_week: lessonData?.event_week ? `${lessonData?.event_week}` : '1',
			start_time: lessonData?.start_time ? new Date(`${lessonData?.event_date} ${lessonData?.start_time}`) : null,
			end_time: lessonData?.end_time ? new Date(`${lessonData?.event_date} ${lessonData?.end_time}`) : null,
			teacher_id: lessonData?.teacher_id ?? null,
			level_id: lessonData?.level_id ?? null
			// zoom_url: lessonData?.zoom_url ? `${lessonData?.zoom_url}` : ''
			// status: lessonData?.status ? `${lessonData?.status}` : '0'
		},
		resolver: yupResolver(
			Yup.object({
				name: Yup.string().trim().required(t('common:validation.required')),
				// content: Yup.string().required(t('common:validation.required')),
				event_date: Yup.mixed().nullable().required(t('common:validation.required')),
				// event_week: Yup.mixed().nullable().required(t('common:validation.required')),
				start_time: Yup.mixed().nullable().required(t('common:validation.required')),
				end_time: Yup.mixed().nullable().required(t('common:validation.required')),
				teacher_id: Yup.mixed().nullable().required(t('common:validation.required')),
				level_id: Yup.mixed().nullable().required(t('common:validation.required'))
				// zoom_url: Yup.string().required(t('common:validation.required'))
				// status: Yup.mixed().nullable().required(t('common:validation.required'))
			})
		)
	})

	const onSubmit = handleSubmit(async data => {
		try {
			console.log(data)
			const newData = {
				...data,
				event_date: moment(data?.event_date).format('YYYY-MM-DD'),
				start_time: moment(data?.start_time).format('HH:mm'),
				end_time: moment(data?.end_time).format('HH:mm')
			}

			await lessonService.save(newData)
			if (isEdit) {
				navigate(ROUTER_EXE.lesson.list + `/detail/${data?.id}`)
			} else {
				navigate(ROUTER_EXE.lesson.list)
			}
			successMsg(isEdit ? t('message.edit_success') : t('message.create_success'))
		} catch (error) {
			if (typeof error?.response?.data?.message === 'string') {
				errorMsg(error?.response?.data?.message)
			} else {
				handleErrorFieldBackend(error, setError)
			}
		}
	})

	return (
		<form onSubmit={onSubmit}>
			<Box className="max-w-lg sm:mt-40 mt-20 mx-auto sm:p-12">
				<AdminInput
					label={t('label.lesson_name')}
					control={control}
					name="name"
					placeholder=""
					size="small"
					className="mb-16 sm:mb-20 p-12"
					// required
				/>

				{/* <AdminInput
					label={t('label.content')}
					control={control}
					name="content"
					placeholder=""
					size="small"
					className="mb-16 sm:mb-20 p-12"
					multiline
					minRows={5}
					// required
				/> */}

				<Box className="flex flex-wrap sm:flex-nowrap mb-16 sm:mb-20 p-12">
					<Box className="w-full sm:w-1/3 mt-12 mb-8 sm:mb-0">
						<Typography variant="h3" className="flex items-center whitespace-normal mb-4">
							{t('label.start_date')}
						</Typography>
					</Box>

					<Box className="rounded-md flex w-full sm:w-2/3">
						<CoreDatePicker
							control={control}
							name="event_date"
							className="w-full sm:w-1/2"
							placeholder=""
							size="small"
							minDate={new Date()}
						/>
					</Box>
				</Box>

				{/* <Box className="flex flex-wrap sm:flex-nowrap mb-16 sm:mb-20 p-12">
					<Box className="w-full sm:w-1/3 mt-12 mb-8 sm:mb-0">
						<Typography variant="h3" className="flex items-center whitespace-normal mb-4">
							{t('label.event_week')}
						</Typography>
					</Box>

					<Box className="rounded-md flex w-full sm:w-2/3">
						<CoreAutocomplete
							control={control}
							name="event_week"
							className="w-full sm:w-1/2"
							placeholder=""
							size="small"
							valuePath="value"
							labelPath="label"
							returnValueType="enum"
							options={eventWeekOptions}
						/>
					</Box>
				</Box> */}

				<Box className="flex flex-wrap sm:flex-nowrap mb-16 sm:mb-20 p-12">
					<Box className="w-full sm:w-1/3 mt-12 mb-8 sm:mb-0">
						<Typography variant="h3" className="flex items-center whitespace-normal mb-4">
							{`${t('label.from_time')} ${t('label.to')} ${t('label.to_time')}`}
						</Typography>
					</Box>

					<Box className="rounded-md sm:flex w-full sm:w-2/3">
						<CoreTimePicker
							control={control}
							name="start_time"
							size="small"
							className="w-full sm:w-1/3"
							variant="outlined"
							placeholder=""
						/>

						<Box className="text-center w-1/3 mx-auto my-10 sm:my-0 pt-10">
							<Typography variant="h3">{t('label.to')}</Typography>
						</Box>

						<CoreTimePicker
							control={control}
							name="end_time"
							size="small"
							className="w-full sm:w-1/3"
							variant="outlined"
							placeholder=""
						/>
					</Box>
				</Box>

				{/* <Box className="flex flex-wrap sm:flex-nowrap mb-16 sm:mb-20 p-12">
					<Box className="w-full sm:w-1/3 mt-12 mb-8 sm:mb-0">
						<Typography variant="h3" className="flex items-center whitespace-normal mb-4">
							{t('label.status_form')}
						</Typography>
					</Box>

					<Box className="rounded-md flex w-full sm:w-2/3">
						<CoreAutocomplete
							control={control}
							name="status"
							className="w-full sm:w-1/2"
							placeholder=""
							size="small"
							valuePath="value"
							labelPath="label"
							returnValueType="enum"
							options={statusOptions}
						/>
					</Box>
				</Box> */}

				<Box className="flex flex-wrap sm:flex-nowrap mb-16 sm:mb-20 p-12">
					<Box className="w-full sm:w-1/3 mt-12 mb-8 sm:mb-0">
						<Typography variant="h3" className="flex items-center whitespace-normal mb-4">
							{t('label.level_class')}
						</Typography>
					</Box>

					<Box className="rounded-md flex w-full sm:w-2/3">
						<CoreAutocomplete
							control={control}
							name="level_id"
							className="w-full sm:w-1/2"
							placeholder=""
							size="small"
							valuePath="value"
							labelPath="label"
							returnValueType="enum"
							options={levelIdOptions}
						/>
					</Box>
				</Box>

				<Box className="flex flex-wrap sm:flex-nowrap mb-16 sm:mb-20 p-12">
					<Box className="w-full sm:w-1/3 mt-12 mb-8 sm:mb-0">
						<Typography variant="h3" className="flex items-center whitespace-normal mb-4">
							{t('label.teacher')}
						</Typography>
					</Box>

					<Box className="rounded-md flex w-full sm:w-2/3">
						<SelectTeacher
							control={control}
							name="teacher_id"
							className="w-full sm:w-1/2"
							placeholder=""
							size="small"
							watchEventDate={watch('event_date')}
							watchStartTime={watch('start_time')}
							watchEndTime={watch('end_time')}
							watchLevelId={watch('level_id')}
							lessonId={lessonData?.id ?? null}
						/>
					</Box>
				</Box>

				{/* <AdminInput
					label={t('label.zoom_url')}
					control={control}
					name="zoom_url"
					placeholder=""
					size="small"
					className="mb-16 sm:mb-20 p-12"
					// required
				/> */}

				<Box className="flex flex-wrap sm:flex-nowrap mb-16 sm:mb-20">
					<Box className="w-full sm:w-1/3 m-12 sm:mx-auto">
						<LoadingButton
							type="submit"
							loading={isSubmitting}
							variant="contained"
							color="primary"
							size="small"
							className="w-full text-16"
						>
							{isEdit ? t('common:btn.update') : t('common:btn.sign_up')}
						</LoadingButton>
					</Box>
				</Box>
			</Box>
		</form>
	)
}

// LessonForm.defaultProps = {}

// LessonForm.propTypes = {}

export default React.memo(LessonForm)
