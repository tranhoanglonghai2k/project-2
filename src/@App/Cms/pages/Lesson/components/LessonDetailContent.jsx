/*
 * Created Date: 15-12-2022, 5:09:38 pm
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import { ROUTER_EXE, TRANSLATE_CMS } from '@App/Cms/configs/constants'
import { Box, Button, Typography } from '@mui/material'
import moment from 'moment'
import React from 'react'
import { useTranslation } from 'react-i18next'
import { toast } from 'react-toastify'
import { useNavigate } from 'react-router-dom'
import { errorMsg } from '@Core/helper/Message'
import CoreInput from '@Core/components/Input/CoreInput'
import { useForm } from 'react-hook-form'
// import PropTypes from 'prop-types'

const LessonDetailContent = props => {
	const { t } = useTranslation(TRANSLATE_CMS.lesson)
	const { lessonData } = props
	const navigate = useNavigate()

	console.log('============= lessonData', lessonData)

	const handleEditLessonClick = () => {
		if (lessonData?.status == 3 || lessonData?.status == 4) {
			errorMsg(t('error.edit_error'))
		} else {
			navigate(ROUTER_EXE.lesson.list + `/edit/${lessonData?.id}`, {
				state: lessonData
			})
		}
	}

	const { control } = useForm({
		mode: 'onTouched',
		defaultValues: {
			zoom_url: lessonData?.zoom_url ?? ''
		}
	})

	return (
		<Box className="w-full mt-40 flex">
			<Box className="w-full lg:w-1/2 mx-auto">
				<Box className="w-full flex mb-20">
					<Typography className="w-full text-xl mr-20 lg:w-1/2 lg:mr-40 text-right">
						{t('label.lesson_name')}
					</Typography>
					<Typography className="w-full text-xl ml-20 lg:w-1/2 lg:ml-40 break-all text-left">
						{lessonData?.name}
					</Typography>
				</Box>
				<Box className="w-full flex mb-20">
					<Typography className="w-full text-xl mr-20 lg:w-1/2 lg:mr-40 text-right">
						{t('label.start_date')}
					</Typography>
					<Typography className="w-full text-xl ml-20 lg:w-1/2 lg:ml-40 text-left">
						{moment(lessonData?.event_date).format('YYYY年MM月DD日')}
					</Typography>
				</Box>
				<Box className="w-full flex mb-20">
					<Typography className="w-full text-xl mr-20 lg:w-1/2 lg:mr-40 text-right">
						{`${t('label.from_time')} ${t('label.to')} ${t('label.to_time')}`}
					</Typography>
					<Typography className="w-full text-xl ml-20 lg:w-1/2 lg:ml-40 text-left">
						{/* {`${moment(lessonData?.from_time).format('HH時mm分')} ${t('label.to')} ${moment(
							lessonData?.to_time
						).format('HH時mm分')}`} */}
						{`${lessonData?.start_time} ${t('label.to')} ${lessonData?.end_time}`}
					</Typography>
				</Box>
				<Box className="w-full flex mb-20">
					<Typography className="w-full text-xl mr-20 lg:w-1/2 lg:mr-40 my-auto text-right">
						{t('label.teacher')}
					</Typography>
					<Typography className="w-full text-xl ml-20 lg:w-1/2 lg:ml-40 break-all text-left">
						{lessonData?.teacher?.name}
					</Typography>
				</Box>
				<Box className="w-full flex mb-20">
					<Typography className="w-full text-xl mr-20 lg:w-1/2 lg:mr-40 my-auto text-right">
						{t('label.level_class')}
					</Typography>
					<Typography className="w-full text-xl ml-20 lg:w-1/2 lg:ml-40 text-left">
						{lessonData?.level?.name}
					</Typography>
				</Box>
				<Box className="w-full flex mb-20">
					<Typography className="w-full text-xl mr-20 lg:w-1/2 lg:mr-40 my-auto text-right">
						{t('label.status')}
					</Typography>
					<Typography className="w-full text-xl ml-20 lg:w-1/2 lg:ml-40 text-left">
						{lessonData?.status_string}
					</Typography>
				</Box>

				<Box className="w-full flex mb-20">
					<Typography className="w-full text-xl mr-20 lg:w-1/2 lg:mr-40 my-auto text-right">
						{t('label.zoom_url')}
					</Typography>
					<Typography className="w-full text-xl ml-20 lg:w-1/2 lg:ml-40 text-left">
						<CoreInput
							control={control}
							name="zoom_url"
							size="small"
							inputProps={{
								readOnly: true
							}}
						/>
					</Typography>
				</Box>

				{lessonData?.status !== 0 ? (
					<Box className="w-full flex my-40">
						<Button
							onClick={() =>
								navigate(
									ROUTER_EXE.reserve_lesson.list + `/view/${lessonData?.reservation_lessons[0]?.id}`
								)
							}
							variant="contained"
							color="primary"
							className="sm:w-1/3 w-3/5 mx-auto text-center text-16"
						>
							{t('btn.detail_reserve_lesson')}
						</Button>
					</Box>
				) : null}

				<Box className="w-full mt-40 flex">
					<Button
						variant="contained"
						color="primary"
						className="w-1/2 mx-10 text-center text-16"
						onClick={() => navigate(ROUTER_EXE.lesson.list)}
					>
						{t('common:btn.return_list')}
					</Button>

					<Button
						variant="contained"
						color="primary"
						className={`w-1/2 mx-10 text-center text-16`}
						onClick={() => handleEditLessonClick()}
					>
						{t('common:btn.edit')}
					</Button>
				</Box>
			</Box>
		</Box>
	)
}

// LessonDetailContent.defaultProps = {}

// LessonDetailContent.propTypes = {}

export default React.memo(LessonDetailContent)
