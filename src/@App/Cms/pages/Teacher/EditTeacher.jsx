/*
 * Created Date: 16-12-2022, 10:44:16 am
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import AdminContentPage from '@App/Cms/components/Layout/AdminContentPage'
import { TRANSLATE_CMS } from '@App/Cms/configs/constants'
import React from 'react'
import { useTranslation } from 'react-i18next'
import { useLocation, useParams } from 'react-router-dom'
import TeacherForm from './components/TeacherForm'
import TeacherProvider from './TeacherProvider'
// import PropTypes from 'prop-types'

const EditTeacher = props => {
	const { t } = useTranslation(TRANSLATE_CMS.teacher)
	const location = useLocation()
	const { id } = useParams()
	const isEdit = id !== 'new'

	return (
		<TeacherProvider isEdit={isEdit}>
			<AdminContentPage
				pageTitle={isEdit ? t('title.edit_page') : t('title.add_page')}
				content={<TeacherForm teacherData={location?.state} />}
			/>
		</TeacherProvider>
	)
}

// EditTeacher.defaultProps = {}

// EditTeacher.propTypes = {}

export default React.memo(EditTeacher)
