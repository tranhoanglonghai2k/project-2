/*
 * Created Date: 16-12-2022, 10:44:04 am
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import AdminContentPage from '@App/Cms/components/Layout/AdminContentPage'
import { TRANSLATE_CMS } from '@App/Cms/configs/constants'
import { Box, CircularProgress } from '@mui/material'
import React from 'react'
import { useTranslation } from 'react-i18next'
import { useLocation } from 'react-router-dom'
import TeacherDetailContent from './components/TeacherDetailContent'
import { useDetailTeacher } from './hooks/useDetailTeacher'
import TeacherProvider from './TeacherProvider'
// import PropTypes from 'prop-types'

const DetailTeacher = props => {
	const { t } = useTranslation(TRANSLATE_CMS.teacher)
	const { teacher, loadingTeacher } = useDetailTeacher()

	return (
		<TeacherProvider>
			<AdminContentPage
				pageTitle={t('title.detail_page')}
				content={
					loadingTeacher ? (
						<Box className="mt-40 text-center">
							<CircularProgress />
						</Box>
					) : (
						<TeacherDetailContent teacherData={teacher} />
					)
				}
			/>
		</TeacherProvider>
	)
}

// DetailTeacher.defaultProps = {}

// DetailTeacher.propTypes = {}

export default React.memo(DetailTeacher)
