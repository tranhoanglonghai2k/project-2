/*
 * Created Date: 16-12-2022, 10:43:29 am
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import AdminPageProvider from '@App/Cms/components/Provider/AdminPageProvider'
import { TRANSLATE_CMS } from '@App/Cms/configs/constants'
import { teacherService } from '@App/Cms/services/teacherService'
import useCoreTable from '@Core/components/Table/hooks/useCoreTable'
import { errorMsg, successMsg } from '@Core/helper/Message'
import { useRequest } from 'ahooks'
import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
// import PropTypes from 'prop-types'

const TeacherProvider = props => {
	const { t } = useTranslation(TRANSLATE_CMS.teacher)
	const requestTeachers = useRequest(teacherService.list, {
		manual: true,
		onError: error => errorMsg(error?.response?.data?.message)
	})

	const { runAsync: handleDeleteTeacher } = useRequest(teacherService.delete, {
		manual: true,
		onSuccess: res => {
			teacherTableHandler.handleFetchData()
			successMsg(t('common:message.delete_success'))
		},
		onError: error => errorMsg(error?.response?.data?.message)
	})

	const teacherTableHandler = useCoreTable(requestTeachers)

	useEffect(() => {
		teacherTableHandler.handleFetchData()
	}, [])

	const data = {
		...props,
		teacherTableHandler,
		handleDeleteTeacher
	}

	return <AdminPageProvider {...data}>{props.children}</AdminPageProvider>
}

// TeacherProvider.defaultProps = {}

// TeacherProvider.propTypes = {}

export default React.memo(TeacherProvider)
