/*
 * Created Date: 16-12-2022, 10:44:58 am
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import { useAdminPageContext } from '@App/Cms/components/Provider/AdminPageProvider'
import { ROUTER_EXE, TRANSLATE_CMS } from '@App/Cms/configs/constants'
import { CoreActionDelete, CoreActionView } from '@Core/components/Table/components/CoreTableAction'
import CoreTable, { columnHelper } from '@Core/components/Table/CoreTable'
import { Box } from '@mui/material'
import React, { useMemo } from 'react'
import { useTranslation } from 'react-i18next'
import { useNavigate } from 'react-router-dom'
import { useDataFake } from '../../Lesson/hooks/useDataFake'
import TeacherTableFilter from './TeacherTableFilter'
// import PropTypes from 'prop-types'

const TeacherTable = props => {
	const { t } = useTranslation(TRANSLATE_CMS.teacher)
	const navigate = useNavigate()
	const { teacherTableHandler, handleDeleteTeacher } = useAdminPageContext()

	const columns = useMemo(() => {
		return [
			columnHelper.accessor('id', {
				cell: info => info.getValue(),
				header: 'ID',
				className: 'w-[10%]'
			}),
			columnHelper.accessor('login_id', {
				header: t('label.login_id')
			}),
			columnHelper.accessor('mail_address', {
				header: t('label.mail_address')
			}),
			columnHelper.accessor('name', {
				header: t('label.name')
			}),
			columnHelper.accessor('action', {
				header: t('label.action'),
				className: 'w-[15%]',
				cell: ({ row }) => {
					const data = row.original

					return (
						<div className="flex">
							<CoreActionView
								onClick={() => navigate(ROUTER_EXE.teacher.list + `/view/${data?.id}`, { state: data })}
							/>
							<CoreActionDelete onConfirmDelete={() => handleDeleteTeacher(data?.id)} />
						</div>
					)
				}
			})
		]
	}, [t])

	return (
		<Box className="m-12">
			<TeacherTableFilter className="mb-20" />
			<CoreTable isShowPagination columns={columns} {...teacherTableHandler} />
		</Box>
	)
}

// TeacherTable.defaultProps = {}

// TeacherTable.propTypes = {}

export default React.memo(TeacherTable)
