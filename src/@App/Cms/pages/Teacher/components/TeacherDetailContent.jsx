/*
 * Created Date: 16-12-2022, 10:45:35 am
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import { ROUTER_EXE, TRANSLATE_CMS } from '@App/Cms/configs/constants'
import { Box, Button, Typography } from '@mui/material'
import React from 'react'
import { useTranslation } from 'react-i18next'
import { useNavigate } from 'react-router-dom'
import useLevelLabel from '@App/Cms/hooks/useLevelLabel'
// import PropTypes from 'prop-types'

const TeacherDetailContent = props => {
	const { t } = useTranslation(TRANSLATE_CMS.teacher)
	const { teacherData } = props
	const navigate = useNavigate()

	return (
		<Box className="w-full mt-40 flex">
			<Box className="w-full lg:w-1/2 mx-auto">
				<Box className="w-full flex mb-20">
					<Typography className="w-full text-xl mr-20 lg:w-1/2 lg:mr-40 text-right">
						{t('label.login_id')}
					</Typography>
					<Typography className="w-full text-xl ml-20 lg:w-1/2 lg:ml-40 break-all text-left">
						{teacherData?.login_id}
					</Typography>
				</Box>
				<Box className="w-full flex mb-20">
					<Typography className="w-full text-xl mr-20 lg:w-1/2 lg:mr-40 text-right">
						{t('label.mail_address')}
					</Typography>
					<Typography className="w-full text-xl ml-20 lg:w-1/2 lg:ml-40 break-all text-left">
						{teacherData?.mail_address}
					</Typography>
				</Box>
				<Box className="w-full flex mb-20">
					<Typography className="w-full text-xl mr-20 lg:w-1/2 lg:mr-40 text-right">
						{t('label.name')}
					</Typography>
					<Typography className="w-full text-xl ml-20 lg:w-1/2 lg:ml-40 break-all text-left">
						{teacherData?.name}
					</Typography>
				</Box>
				<Box className="w-full flex mb-20">
					<Typography className="w-full text-xl mr-20 lg:w-1/2 lg:mr-40 text-right">
						{t('label.operation_time')}
					</Typography>
					<Box className="w-full ml-20 lg:w-1/2 lg:ml-40">
						{teacherData?.operating_time?.length > 0
							? teacherData?.operating_time?.map(item => (
									<Typography key={item?.id} className="text-xl text-left">
										{item?.working_day}: {item?.start_time} ~ {item?.end_time}
									</Typography>
							  ))
							: null}
					</Box>
				</Box>

				<Box className="w-full flex mb-20">
					<Typography className="w-full text-xl mr-20 lg:w-1/2 lg:mr-40 text-right">
						{t('label.level')}
					</Typography>
					<Typography className="w-full text-xl ml-20 lg:w-1/2 lg:ml-40 break-all text-left">
						{teacherData?.level_id ? useLevelLabel(teacherData?.level_id) : null}
					</Typography>
				</Box>

				<Box className="w-full mt-40 flex">
					<Button
						onClick={() => navigate(ROUTER_EXE.teacher.list)}
						variant="contained"
						color="primary"
						className="w-1/2 mx-10 text-center text-16"
					>
						{t('common:btn.return_list')}
					</Button>
					<Button
						onClick={() =>
							navigate(ROUTER_EXE.teacher.list + `/edit/${teacherData?.id}`, {
								state: teacherData
							})
						}
						variant="contained"
						color="primary"
						className="w-1/2 mx-10 text-center text-16"
					>
						{t('common:btn.edit')}
					</Button>
				</Box>
			</Box>
		</Box>
	)
}

// TeacherDetailContent.defaultProps = {}

// TeacherDetailContent.propTypes = {}

export default React.memo(TeacherDetailContent)
