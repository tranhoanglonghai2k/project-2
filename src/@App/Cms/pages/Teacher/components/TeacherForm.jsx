/*
 * Created Date: 16-12-2022, 10:45:49 am
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import AdminInput from '@App/Cms/components/Input/AdminInput'
import { useAdminPageContext } from '@App/Cms/components/Provider/AdminPageProvider'
import { ROUTER_EXE, TRANSLATE_CMS } from '@App/Cms/configs/constants'
import { teacherService } from '@App/Cms/services/teacherService'
import { errorMsg, handleErrorFieldBackend, successMsg } from '@Core/helper/Message'
import CoreAutocomplete from '@Core/components/Input/CoreAutocomplete'
import Yup from '@Core/helper/Yup'
import { yupResolver } from '@hookform/resolvers/yup'
import { LoadingButton } from '@mui/lab'
import { Box, Typography } from '@mui/material'
import React from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { useNavigate } from 'react-router-dom'
import { useOptions } from '@App/Cms/hooks/useOptions'
import useLevelLabel from '@App/Cms/hooks/useLevelLabel'
// import PropTypes from 'prop-types'

const TeacherForm = props => {
	const { teacherTableHandler, isEdit } = useAdminPageContext()
	const navigate = useNavigate()
	const { t } = useTranslation(TRANSLATE_CMS.teacher)
	const { teacherData } = props
	const { levelIdOptions } = useOptions()

	const {
		control,
		handleSubmit,
		formState: { isSubmitting, isDirty },
		setError
	} = useForm({
		mode: 'onTouched',
		defaultValues: {
			id: teacherData?.id ?? null,
			login_id: teacherData?.login_id ?? '',
			mail_address: teacherData?.mail_address ?? '',
			name: teacherData?.name ?? '',
			// password: teacherData?.password ?? '',
			login_first: teacherData?.login_first ?? '0',
			level_id: +teacherData?.level_id ?? null
		},
		resolver: yupResolver(
			Yup.object({
				login_id: Yup.string()
					.trim()
					.required(t('common:validation.required'))
					.min(6, t('common:validation.string.min_login_id', { value: 6 }))
					.max(16, t('common:validation.string.max_login_id', { value: 16 })),
				mail_address: Yup.string()
					.trim()
					.required(t('common:validation.required'))
					.email(t('common:validation.string.valid_email'))
					.max(255, t('common:validation.string.max_length_email', { value: 255 })),
				name: Yup.string().trim().required(t('common:validation.required')),
				level_id: Yup.mixed().nullable().required(t('common:validation.required'))
				// password: Yup.string().required(t('common:validation.required'))
			})
		)
	})

	const onSubmit = handleSubmit(async data => {
		try {
			await teacherService.save(data)
			navigate(ROUTER_EXE.teacher.list)
			successMsg(isEdit ? t('message.edit_success') : t('message.create_success'))
		} catch (error) {
			console.log('============= error', error)
			if (typeof error?.response?.data?.message === 'string') {
				errorMsg(error?.response?.data?.message)
			} else {
				handleErrorFieldBackend(error, setError)
			}
		}
	})

	return (
		<form onSubmit={onSubmit}>
			<Box className="max-w-lg sm:mt-4'0' mt-20 mx-auto sm:p-12">
				<AdminInput
					label={t('label.login_id')}
					control={control}
					name="login_id"
					placeholder=""
					size="small"
					className="mb-16 sm:mb-20 p-12"
					// required
				/>

				<AdminInput
					label={t('label.mail_address')}
					control={control}
					name="mail_address"
					className="mb-16 sm:mb-20 p-12"
					placeholder=""
					size="small"
					// required
				/>

				{/* <AdminInput
					label={t('label.password')}
					control={control}
					type="password"
					name="password"
					className="mb-16 sm:mb-20 p-12"
					placeholder=""
					size="small"
					// required
				/> */}

				<AdminInput
					label={t('label.name')}
					control={control}
					name="name"
					className="mb-16 sm:mb-20 p-12"
					placeholder=""
					size="small"
				/>

				<Box className="flex flex-wrap sm:flex-nowrap mb-16 sm:mb-20 p-12">
					<Box className="w-full sm:w-1/3 mt-12 mb-8 sm:mb-0">
						<Typography variant="h3" className="flex items-center whitespace-normal mb-4">
							{t('label.level')}
						</Typography>
					</Box>

					<Box className="rounded-md flex w-full sm:w-2/3">
						<CoreAutocomplete
							control={control}
							name="level_id"
							className="w-full"
							placeholder=""
							size="small"
							valuePath="value"
							labelPath="label"
							returnValueType="enum"
							options={levelIdOptions}
						/>
					</Box>
				</Box>

				<Box className="flex flex-wrap sm:flex-nowrap mb-16 sm:mb-20">
					<Box className="w-full lg:w-1/3 sm:w-2/5 m-12 mx-auto px-20 sm:p-0">
						<LoadingButton
							type="submit"
							loading={isSubmitting}
							disabled={!isEdit && !isDirty}
							variant="contained"
							color="primary"
							size="small"
							className="w-full text-16"
						>
							{isEdit ? t('common:btn.update') : t('common:btn.sign_up')}
						</LoadingButton>
					</Box>
				</Box>
			</Box>
		</form>
	)
}

// TeacherForm.defaultProps = {}

// TeacherForm.propTypes = {}

export default React.memo(TeacherForm)
