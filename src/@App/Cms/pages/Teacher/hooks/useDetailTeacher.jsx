/*
 * Created Date: 21-12-2022, 10:55:04 am
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import { teacherService } from '@App/Cms/services/teacherService'
import { errorMsg } from '@Core/helper/Message'
import { useRequest } from 'ahooks'
import { useEffect } from 'react'
import { useParams } from 'react-router-dom'

export const useDetailTeacher = props => {
	const { id } = useParams()

	const {
		data: teacher,
		run: fetchTeacher,
		loading: loadingTeacher
	} = useRequest(teacherService.find, {
		manual: true,
		onError: error => errorMsg(error?.response?.data?.message)
	})

	useEffect(() => {
		if (id) {
			fetchTeacher(id)
		}
	}, [])

	return { teacher, loadingTeacher }
}
