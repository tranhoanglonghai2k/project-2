/*
 * Created Date: 16-12-2022, 10:43:54 am
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import AdminContentPage from '@App/Cms/components/Layout/AdminContentPage'
import { ROUTER_EXE, TRANSLATE_CMS } from '@App/Cms/configs/constants'
import { Box, Button } from '@mui/material'
import React from 'react'
import { useTranslation } from 'react-i18next'
import { useNavigate } from 'react-router-dom'
import TeacherTable from './components/TeacherTable'
import TeacherProvider from './TeacherProvider'
// import PropTypes from 'prop-types'

const ListTeacher = props => {
	const { t } = useTranslation(TRANSLATE_CMS.teacher)
	const navigate = useNavigate()

	return (
		<TeacherProvider>
			<AdminContentPage
				pageTitle={t('title.title_page')}
				content={
					<>
						<TeacherTable />
						<Box className="text-right my-40 mr-40">
							<Button
								variant="contained"
								color="primary"
								onClick={() => navigate(ROUTER_EXE.teacher.list + '/edit/new')}
								className="px-20 w-160 text-18"
							>
								{t('common:btn.add')}
							</Button>
						</Box>
					</>
				}
			/>
		</TeacherProvider>
	)
}

// ListTeacher.defaultProps = {}

// ListTeacher.propTypes = {}

export default React.memo(ListTeacher)
