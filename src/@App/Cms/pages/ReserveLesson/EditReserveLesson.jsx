/*
 * Created Date: 15-12-2022, 11:02:58 am
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import AdminContentPage from '@App/Cms/components/Layout/AdminContentPage'
import { TRANSLATE_CMS } from '@App/Cms/configs/constants'
import React from 'react'
import { useTranslation } from 'react-i18next'
import { useLocation } from 'react-router-dom'
import ReserveLessonForm from './components/ReserveLessonForm'
import ReserveLessonProvider from './ReserveLessonProvider'
// import PropTypes from 'prop-types'

const EditReserveLesson = props => {
	const { t } = useTranslation(TRANSLATE_CMS.reserve_lesson)
	const location = useLocation()

	return (
		<ReserveLessonProvider>
			<AdminContentPage
				pageTitle={t('title.edit_page')}
				content={<ReserveLessonForm reserveLessonData={location?.state} />}
			/>
		</ReserveLessonProvider>
	)
}

// EditReserveLesson.defaultProps = {}

// EditReserveLesson.propTypes = {}

export default React.memo(EditReserveLesson)
