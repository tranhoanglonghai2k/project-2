/*
 * Created Date: 15-12-2022, 11:08:11 am
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import { ROUTER_EXE, TRANSLATE_CMS } from '@App/Cms/configs/constants'
import { Box, Button, Typography } from '@mui/material'
import moment from 'moment'
import React from 'react'
import { useTranslation } from 'react-i18next'
import { useNavigate } from 'react-router-dom'
// import PropTypes from 'prop-types'

const ReserveLessonDetailContent = props => {
	const { t } = useTranslation(TRANSLATE_CMS.reserve_lesson)
	const { reserveLessonData } = props
	const navigate = useNavigate()

	return (
		<Box className="w-full mt-40 flex">
			<Box className="w-full lg:w-1/2 mx-auto">
				<Box className="w-full flex mb-20">
					<Typography className="w-full text-xl mr-20 lg:w-1/2 lg:mr-40 text-right">
						{t('label.lesson_name')}
					</Typography>
					<Typography className="w-full text-xl ml-20 lg:w-1/2 lg:ml-40 break-all text-left">
						{reserveLessonData?.lessons?.name}
					</Typography>
				</Box>
				<Box className="w-full flex mb-20">
					<Typography className="w-full text-xl mr-20 lg:w-1/2 lg:mr-40 text-right">
						{t('label.start_date')}
					</Typography>
					<Typography className="w-full text-xl ml-20 lg:w-1/2 lg:ml-40 text-left">
						{moment(reserveLessonData?.lessons?.event_date).format('YYYY年MM月DD日')}
					</Typography>
				</Box>
				<Box className="w-full flex mb-20">
					<Typography className="w-full text-xl mr-20 lg:w-1/2 lg:mr-40 text-right">
						{`${t('label.from_time')} ${t('label.to')} ${t('label.to_time')}`}
					</Typography>
					<Typography className="w-full text-xl ml-20 lg:w-1/2 lg:ml-40 text-left">
						{`${reserveLessonData?.lessons?.start_time} ${t('label.to')} ${
							reserveLessonData?.lessons?.end_time
						}`}
					</Typography>
				</Box>
				<Box className="w-full flex mb-20">
					<Typography className="w-full text-xl mr-20 lg:w-1/2 lg:mr-40 my-auto text-right">
						{t('label.teacher')}
					</Typography>
					<Typography className="w-full text-xl ml-20 lg:w-1/2 lg:ml-40 break-all text-left">
						{reserveLessonData?.lessons?.teachers?.name}
					</Typography>
				</Box>
				<Box className="w-full flex mb-20">
					<Typography className="w-full text-xl mr-20 lg:w-1/2 lg:mr-40 my-auto text-right">
						{t('label.student')}
					</Typography>
					<Typography className="w-full text-xl ml-20 lg:w-1/2 lg:ml-40 break-all text-left">
						{reserveLessonData?.users?.name}
					</Typography>
				</Box>
				<Box className="w-full flex mb-20">
					<Typography className="w-full text-xl mr-20 lg:w-1/2 lg:mr-40 my-auto text-right">
						{t('label.level_class')}
					</Typography>
					<Typography className="w-full text-xl ml-20 lg:w-1/2 lg:ml-40 text-left">
						{reserveLessonData?.lessons?.level?.name}
					</Typography>
				</Box>
				<Box className="w-full flex mb-20">
					<Typography className="w-full text-xl mr-20 lg:w-1/2 lg:mr-40 my-auto text-right">
						{t('label.zoom_url')}
					</Typography>
					<Typography className="w-full text-xl ml-20 lg:w-1/2 lg:ml-40 text-left">
						{reserveLessonData?.lessons?.zoom_url}
					</Typography>
				</Box>
				<Box className="w-full flex mb-20">
					<Typography className="w-full text-xl mr-20 lg:w-1/2 lg:mr-40 my-auto text-right">
						{t('label.status')}
					</Typography>
					<Typography className="w-full text-xl ml-20 lg:w-1/2 lg:ml-40 text-left">
						{reserveLessonData?.lessons?.status === 1
							? '予約中'
							: reserveLessonData?.lessons?.status === 2
							? '予約確定'
							: reserveLessonData?.lessons?.status === 3
							? '実施済み'
							: reserveLessonData?.lessons?.status === 4
							? '未実施'
							: reserveLessonData?.lessons?.status === 0
							? '未予約'
							: null}
					</Typography>
				</Box>

				<Box className="w-full mt-40 flex">
					<Button
						variant="contained"
						color="primary"
						className="w-1/2 mx-10 text-center text-16"
						onClick={() => navigate(ROUTER_EXE.reserve_lesson.list)}
					>
						{t('common:btn.return_list')}
					</Button>
					<Button
						variant="contained"
						color="primary"
						className="w-1/2 mx-10 text-center text-16"
						onClick={() =>
							navigate(ROUTER_EXE.reserve_lesson.list + `/edit/${reserveLessonData?.id}`, {
								state: reserveLessonData
							})
						}
					>
						{t('common:btn.edit')}
					</Button>
				</Box>
			</Box>
		</Box>
	)
}

// ReserveLessonDetailContent.defaultProps = {}

// ReserveLessonDetailContent.propTypes = {}

export default React.memo(ReserveLessonDetailContent)
