/*
 * Created Date: 15-12-2022, 11:07:26 am
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import { useAdminPageContext } from '@App/Cms/components/Provider/AdminPageProvider'
import { TRANSLATE_CMS } from '@App/Cms/configs/constants'
import { useOptions } from '@App/Cms/hooks/useOptions'
import CoreAutocomplete from '@Core/components/Input/CoreAutocomplete'
import CoreDatePicker from '@Core/components/Input/CoreDatePicker'
import CoreInput from '@Core/components/Input/CoreInput'
import { Box, Button, Typography } from '@mui/material'
import clsx from 'clsx'
import moment from 'moment'
import React from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
// import PropTypes from 'prop-types'

const ReserveLessonTableFilter = props => {
	const { className } = props
	const { reserveLessonTableHandler, teacherData, studentData } = useAdminPageContext()
	const { t } = useTranslation(TRANSLATE_CMS.reserve_lesson)
	const { statusFilterOptions } = useOptions()
	const handleFilter = () => {
		const data = getValues()
		console.log('============= data', data)

		const params = {
			...data,
			start_time: data?.start_time ? moment(data?.start_time)?.format('DD/MM') : null,
			end_time: data?.end_time ? moment(data?.end_time)?.format('DD/MM') : null,
			page: 1
		}

		reserveLessonTableHandler.handleFetchData(params)
	}

	const { control, getValues, watch } = useForm({
		mode: 'onTouched',
		defaultValues: {
			name_lesson: '',
			name_teacher: '',
			name_user: '',
			status: 1,
			start_time: '',
			end_time: ''
		}
	})

	return (
		<Box className={clsx('border-1 rounded-4 border-grey-300', className)}>
			<Box className="p-8 w-full lg:flex lg:flex-wrap">
				<Box className="flex w-full lg:w-1/2 mb-20 lg:mb-10 items-start">
					<Box className="w-2/5 lg:w-1/4 px-20 text-base h-full my-auto text-left lg:text-right">
						{t('label.lesson_name')}
					</Box>
					<CoreInput
						control={control}
						name="name_lesson"
						size="small"
						className="lg:w-3/4 w-3/5"
						fullWidth
						variant="outlined"
					/>
				</Box>
				<Box className="flex w-full lg:w-1/2 mb-20 lg:mb-10 items-start">
					<Box className="w-2/5 lg:w-1/4 px-20 text-base h-full my-auto text-left lg:text-right">
						{t('label.teacher')}
					</Box>
					<CoreAutocomplete
						control={control}
						name="name_teacher"
						size="small"
						className="lg:w-3/4 w-3/5"
						fullWidth
						variant="outlined"
						valuePath="name"
						labelPath="name"
						returnValueType="enum"
						options={teacherData?.data}
					/>
				</Box>
				<Box className="flex w-full lg:w-1/2 mb-20 lg:mb-10 items-start">
					<Box className="w-2/5 lg:w-1/4 px-20 text-base h-full my-auto text-left lg:text-right">
						{t('label.student')}
					</Box>
					<CoreAutocomplete
						control={control}
						name="name_user"
						size="small"
						className="lg:w-3/4 w-3/5"
						fullWidth
						variant="outlined"
						valuePath="name"
						labelPath="name"
						returnValueType="enum"
						options={studentData?.data}
					/>
				</Box>
				<Box className="flex w-full lg:w-1/2 mb-20 lg:mb-10 items-start">
					<Box className="w-2/5 lg:w-1/4 px-20 text-base h-full my-auto text-left lg:text-right">
						{t('label.status')}
					</Box>
					<CoreAutocomplete
						control={control}
						name="status"
						size="small"
						className="lg:w-3/4 w-3/5"
						fullWidth
						variant="outlined"
						valuePath="value"
						labelPath="label"
						returnValueType="enum"
						options={statusFilterOptions}
					/>
				</Box>

				<Box className="flex w-full lg:w-1/2 mb-20 lg:mb-10 items-start">
					<Box className="w-2/5 lg:w-1/4 px-20 text-base h-full my-auto text-left lg:text-right">
						{`${t('label.event_date')}`}
					</Box>
					<Box className="flex lg:w-3/4 w-3/5">
						<CoreDatePicker
							control={control}
							name="start_time"
							size="small"
							className="sm:w-1/2 w-full"
							variant="outlined"
							placeholder=""
							// showMonthYearPicker
							showDateMonth
						/>

						<Box className="text-center mx-8 pt-10">
							<Typography variant="h3">{t('label.to')}</Typography>
						</Box>

						<CoreDatePicker
							control={control}
							name="end_time"
							size="small"
							className="sm:w-1/2 w-full"
							variant="outlined"
							placeholder=""
							// showMonthYearPicker
							showDateMonth
						/>
					</Box>
				</Box>
			</Box>
			<Box className="flex p-8 w-full">
				<Button
					variant="contained"
					color="primary"
					className="ml-auto w-2/5 p-12 sm:p-8 lg:p-5 lg:w-160 text-18"
					onClick={handleFilter}
				>
					{t('common:btn.search')}
				</Button>
			</Box>
		</Box>
	)
}

// ReserveLessonTableFilter.defaultProps = {}

// ReserveLessonTableFilter.propTypes = {}

export default React.memo(ReserveLessonTableFilter)
