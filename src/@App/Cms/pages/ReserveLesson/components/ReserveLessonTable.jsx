/*
 * Created Date: 15-12-2022, 11:03:21 am
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import { useAdminPageContext } from '@App/Cms/components/Provider/AdminPageProvider'
import { ROUTER_EXE, TRANSLATE_CMS } from '@App/Cms/configs/constants'
import { CoreActionDelete, CoreActionView } from '@Core/components/Table/components/CoreTableAction'
import CoreTable, { columnHelper } from '@Core/components/Table/CoreTable'
import { Box } from '@mui/material'
import moment from 'moment'
import React, { useMemo } from 'react'
import { useTranslation } from 'react-i18next'
import { useNavigate } from 'react-router-dom'
import { useDataFake } from '../../Lesson/hooks/useDataFake'
import ReserveLessonTableFilter from './ReserveLessonTableFilter'
// import PropTypes from 'prop-types'

const ReserveLessonTable = props => {
	const { t } = useTranslation(TRANSLATE_CMS.reserve_lesson)
	const navigate = useNavigate()
	const { reserveLessonTableHandler, handleDeleteReserveLesson } = useAdminPageContext()

	const columns = useMemo(() => {
		return [
			columnHelper.accessor('lesson_name', {
				header: t('label.lesson_name')
			}),
			columnHelper.accessor('lesson_event_date', {
				header: t('label.start_date'),
				cell: ({ row }) => {
					return <>{moment(row?.original?.lesson_event_date).format('YYYY年MM月DD日')}</>
				}
			}),
			columnHelper.accessor('lesson_start_time', {
				header: t('label.from_time')
			}),
			columnHelper.accessor('lesson_end_time', {
				header: t('label.to_time')
			}),
			columnHelper.accessor('teacher_name', {
				header: t('label.teacher')
			}),
			columnHelper.accessor('user_name', {
				header: t('label.student')
			}),
			columnHelper.accessor('action', {
				header: t('label.action'),
				className: 'w-[15%]',
				cell: ({ row }) => {
					const data = row.original

					return (
						<div className="flex">
							<CoreActionView
								onClick={() => navigate(ROUTER_EXE.reserve_lesson.list + `/view/${data?.id}`)}
							/>
							{/* <CoreActionDelete onConfirmDelete={() => handleDeleteReserveLesson(data?.id)} /> */}
						</div>
					)
				}
			})
		]
	}, [t])

	return (
		<Box className="m-12">
			<ReserveLessonTableFilter className="mb-20" />
			<CoreTable isShowPagination columns={columns} {...reserveLessonTableHandler} />
		</Box>
	)
}

// ReserveLessonTable.defaultProps = {}

// ReserveLessonTable.propTypes = {}

export default React.memo(ReserveLessonTable)
