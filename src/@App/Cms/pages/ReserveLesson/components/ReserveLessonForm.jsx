/*
 * Created Date: 15-12-2022, 11:07:51 am
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import { useAdminPageContext } from '@App/Cms/components/Provider/AdminPageProvider'
import { ROUTER_EXE, TRANSLATE_CMS } from '@App/Cms/configs/constants'
import { reserveLessonService } from '@App/Cms/services/reserveLessonService'
import { errorMsg, handleErrorFieldBackend, successMsg } from '@Core/helper/Message'
import Yup from '@Core/helper/Yup'
import moment from 'moment'
import { yupResolver } from '@hookform/resolvers/yup'
import { Box, Typography } from '@mui/material'
import React from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { useNavigate } from 'react-router-dom'
import CoreInput from '@Core/components/Input/CoreInput'
import { LoadingButton } from '@mui/lab'
import { useOptions } from '@App/Cms/hooks/useOptions'
import CoreAutocomplete from '@Core/components/Input/CoreAutocomplete'
// import PropTypes from 'prop-types'

const ReserveLessonForm = props => {
	const { reserveLessonTableHandler } = useAdminPageContext()
	const { reserveLessonData } = props
	const { statusOptions } = useOptions()
	const navigate = useNavigate()
	const { t } = useTranslation(TRANSLATE_CMS.reserve_lesson)

	const {
		control,
		handleSubmit,
		formState: { isSubmitting },
		setError
	} = useForm({
		mode: 'onTouched',
		defaultValues: {
			id: reserveLessonData?.id ?? null,
			status: reserveLessonData?.lessons?.status ?? 1
		},
		resolver: yupResolver(
			Yup.object({
				status: Yup.mixed().nullable().required(t('common:validation.required'))
			})
		)
	})

	const onSubmit = handleSubmit(async data => {
		try {
			await reserveLessonService.save(data)
			navigate(ROUTER_EXE.reserve_lesson.list)
			successMsg(t('message.edit_success'))
		} catch (error) {
			if (typeof error?.response?.data?.message === 'string') {
				errorMsg(error?.response?.data?.message)
			} else {
				handleErrorFieldBackend(error, setError)
			}
		}
	})

	return (
		<form onSubmit={onSubmit}>
			<Box className="max-w-lg mt-40 mx-auto lg:p-12">
				<Box className="w-full flex mb-20">
					<Typography className="w-full text-xl mr-20 lg:w-1/2 lg:mr-40 text-right">
						{t('label.lesson_name')}
					</Typography>
					<Typography className="w-full text-xl ml-20 lg:w-1/2 lg:ml-40 text-left">
						{reserveLessonData?.lessons?.name}
					</Typography>
				</Box>
				<Box className="w-full flex mb-20">
					<Typography className="w-full text-xl mr-20 lg:w-1/2 lg:mr-40 text-right">
						{t('label.start_date')}
					</Typography>
					<Typography className="w-full text-xl ml-20 lg:w-1/2 lg:ml-40 text-left">
						{moment(reserveLessonData?.lessons?.event_date).format('YYYY年MM月DD日')}
					</Typography>
				</Box>
				<Box className="w-full flex mb-20">
					<Typography className="w-full text-xl mr-20 lg:w-1/2 lg:mr-40 text-right">
						{`${t('label.from_time')} ${t('label.to')} ${t('label.to_time')}`}
					</Typography>
					<Typography className="w-full text-xl ml-20 lg:w-1/2 lg:ml-40 text-left">
						{`${reserveLessonData?.lessons?.start_time} ${t('label.to')} ${
							reserveLessonData?.lessons?.end_time
						}`}
					</Typography>
				</Box>
				<Box className="w-full flex mb-20">
					<Typography className="w-full text-xl mr-20 lg:w-1/2 lg:mr-40 my-auto text-right">
						{t('label.teacher')}
					</Typography>
					<Typography className="w-full text-xl ml-20 lg:w-1/2 lg:ml-40 text-left">
						{reserveLessonData?.lessons?.teachers?.name}
					</Typography>
				</Box>
				<Box className="w-full flex mb-20">
					<Typography className="w-full text-xl mr-20 lg:w-1/2 lg:mr-40 my-auto text-right">
						{t('label.student')}
					</Typography>
					<Typography className="w-full text-xl ml-20 lg:w-1/2 lg:ml-40 text-left">
						{reserveLessonData?.users?.name}
					</Typography>
				</Box>
				<Box className="w-full flex mb-20">
					<Typography className="w-full text-xl mr-20 lg:w-1/2 lg:mr-40 my-auto text-right">
						{t('label.level_class')}
					</Typography>
					<Typography className="w-full text-xl ml-20 lg:w-1/2 lg:ml-40 text-left">
						{reserveLessonData?.lessons?.level?.name}
					</Typography>
				</Box>
				<Box className="w-full flex mb-20">
					<Typography className="w-full text-xl mr-20 lg:w-1/2 lg:mr-40 my-auto text-right">
						{t('label.status')}
					</Typography>
					<Box className="w-full ml-20 lg:w-1/2 lg:ml-40 pr-10">
						<CoreAutocomplete
							control={control}
							name="status"
							placeholder=""
							size="small"
							valuePath="value"
							labelPath="label"
							returnValueType="enum"
							options={statusOptions}
						/>
					</Box>
				</Box>

				<Box className="flex flex-wrap lg:flex-nowrap mt-40">
					<Box className="w-full lg:w-1/3 sm:w-2/5 m-12 mx-auto px-20 sm:p-0">
						<LoadingButton
							type="submit"
							loading={isSubmitting}
							variant="contained"
							color="primary"
							size="small"
							className="w-full text-xl py-10"
						>
							{t('common:btn.update')}
						</LoadingButton>
					</Box>
				</Box>
			</Box>
		</form>
	)
}

// ReserveLessonForm.defaultProps = {}

// ReserveLessonForm.propTypes = {}

export default React.memo(ReserveLessonForm)
