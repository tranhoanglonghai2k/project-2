/*
 * Created Date: 15-12-2022, 11:02:21 am
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import AdminContentPage from '@App/Cms/components/Layout/AdminContentPage'
import { TRANSLATE_CMS } from '@App/Cms/configs/constants'
import { Box, CircularProgress } from '@mui/material'
import React from 'react'
import { useTranslation } from 'react-i18next'
import { useLocation } from 'react-router-dom'
import ReserveLessonDetailContent from './components/ReserveLessonDetailContent'
import { useDetailReserveLesson } from './hooks/useDetailReserveLesson'
import ReserveLessonProvider from './ReserveLessonProvider'
// import PropTypes from 'prop-types'

const DetailReserveLesson = props => {
	const { t } = useTranslation(TRANSLATE_CMS.reserve_lesson)
	const { reserveLesson, loadingReserveLesson } = useDetailReserveLesson()
	const location = useLocation()

	return (
		<ReserveLessonProvider>
			<AdminContentPage
				pageTitle={t('title.detail_page')}
				content={
					loadingReserveLesson ? (
						<Box className="mt-40 text-center">
							<CircularProgress />
						</Box>
					) : (
						<ReserveLessonDetailContent reserveLessonData={reserveLesson} />
					)
				}
			/>
		</ReserveLessonProvider>
	)
}

// DetailReserveLesson.defaultProps = {}

// DetailReserveLesson.propTypes = {}

export default React.memo(DetailReserveLesson)
