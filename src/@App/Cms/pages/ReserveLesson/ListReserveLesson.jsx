/*
 * Created Date: 15-12-2022, 11:02:01 am
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import AdminContentPage from '@App/Cms/components/Layout/AdminContentPage'
import { TRANSLATE_CMS } from '@App/Cms/configs/constants'
import React from 'react'
import { useTranslation } from 'react-i18next'
import ReserveLessonTable from './components/ReserveLessonTable'
import ReserveLessonProvider from './ReserveLessonProvider'
// import PropTypes from 'prop-types'

const ListReserveLesson = props => {
	const { t } = useTranslation(TRANSLATE_CMS.reserve_lesson)

	return (
		<ReserveLessonProvider>
			<AdminContentPage pageTitle={t('title.title_page')} content={<ReserveLessonTable />} />
		</ReserveLessonProvider>
	)
}

// ListReserveLesson.defaultProps = {}

// ListReserveLesson.propTypes = {}

export default React.memo(ListReserveLesson)
