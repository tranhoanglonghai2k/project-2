/*
 * Created Date: 20-12-2022, 9:26:45 pm
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import { reserveLessonService } from '@App/Cms/services/reserveLessonService'
import { errorMsg } from '@Core/helper/Message'
import { useRequest } from 'ahooks'
import { useEffect } from 'react'
import { useParams } from 'react-router-dom'

export const useDetailReserveLesson = props => {
	const { id } = useParams()

	const {
		data: reserveLesson,
		run: fetchReserveLesson,
		loading: loadingReserveLesson
	} = useRequest(reserveLessonService.find, {
		manual: true,
		onError: error => errorMsg(error?.response?.data?.message)
	})

	useEffect(() => {
		if (id) {
			fetchReserveLesson(id)
		}
	}, [])

	return { reserveLesson, loadingReserveLesson }
}
