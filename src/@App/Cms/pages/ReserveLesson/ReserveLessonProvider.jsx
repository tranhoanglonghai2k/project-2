/*
 * Created Date: 15-12-2022, 11:01:19 am
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import AdminPageProvider from '@App/Cms/components/Provider/AdminPageProvider'
import { TRANSLATE_CMS } from '@App/Cms/configs/constants'
import { reserveLessonService } from '@App/Cms/services/reserveLessonService'
import { studentService } from '@App/Cms/services/studentService'
import { teacherService } from '@App/Cms/services/teacherService'
import useCoreTable from '@Core/components/Table/hooks/useCoreTable'
import { errorMsg, successMsg } from '@Core/helper/Message'
import { useRequest } from 'ahooks'
import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { useParams } from 'react-router-dom'
// import PropTypes from 'prop-types'

const ReserveLessonProvider = props => {
	const { t } = useTranslation(TRANSLATE_CMS.reserve_lesson)
	const { id } = useParams()
	const requestReserveLessons = useRequest(reserveLessonService.list, {
		manual: true,
		onError: error => errorMsg(error?.response?.data?.message)
	})

	const { runAsync: handleDeleteReserveLesson } = useRequest(reserveLessonService.delete, {
		manual: true,
		onSuccess: res => {
			reserveLessonTableHandler.handleFetchData()
			successMsg(t('common:message.delete_success'))
		},
		onError: error => {
			errorMsg(error?.response?.data?.message)
		}
	})

	const { data: studentData, run: fetchStudent } = useRequest(studentService.list, {
		manual: true,
		onError: error => errorMsg(error?.response?.data?.message)
	})
	const { data: teacherData, run: fetchTeacher } = useRequest(teacherService.list, {
		manual: true,
		onError: error => errorMsg(error?.response?.data?.message)
	})

	const reserveLessonTableHandler = useCoreTable(requestReserveLessons)

	useEffect(() => {
		if (typeof id === 'undefined') {
			fetchTeacher({ per_page: 1000 })
			fetchStudent({ per_page: 1000 })
		}
		reserveLessonTableHandler.handleFetchData()
	}, [])

	const data = {
		...props,
		reserveLessonTableHandler,
		handleDeleteReserveLesson,
		teacherData,
		studentData
	}

	return <AdminPageProvider {...data}>{props.children}</AdminPageProvider>
}

// ReserveLessonProvider.defaultProps = {}

// ReserveLessonProvider.propTypes = {}

export default React.memo(ReserveLessonProvider)
