/*
 * Created Date: 12-12-2022, 10:35:23 pm
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import { useAdminPageContext } from '@App/Cms/components/Provider/AdminPageProvider'
import { ROUTER_STUDENT, ROUTER_TEACHER, TRANSLATE_CMS } from '@App/Cms/configs/constants'
import { CircularProgress } from '@mui/material'
import moment from 'moment'
import 'moment-timezone'
import React, { useCallback, useEffect, useMemo, useRef, useState } from 'react'
import { useTranslation } from 'react-i18next'
import { Calendar, Views, DateLocalizer, momentLocalizer, dateFnsLocalizer } from 'react-big-calendar'
import * as dates from './date'
import 'react-big-calendar/lib/css/react-big-calendar.css'
import { useNavigate } from 'react-router-dom'
import CoreCheckbox from '@Core/components/Input/CoreCheckbox'
import { useForm } from 'react-hook-form'
import { useUpdateEffect } from 'ahooks'
import { addDays } from 'date-fns'
import { getAuthTokenSession, getLangSession } from '@Core/helper/Session'
import format from 'date-fns/format'
import parse from 'date-fns/parse'
import startOfWeek from 'date-fns/startOfWeek'
import getDay from 'date-fns/getDay'
import ja from 'date-fns/locale/ja'
import enUS from 'date-fns/locale/en-US'
// import PropTypes from 'prop-types'

const ColoredDateCellWrapper = ({ children }) =>
	React.cloneElement(React.Children.only(children), {
		style: {
			backgroundColor: 'lightblue'
		}
	})

const ScheduleCalendarContent = props => {
	const { type } = props
	const navigate = useNavigate()
	const { t } = useTranslation(TRANSLATE_CMS.schedule_calendar)
	const { scheduleCalendar, loadingScheduleCalendar, getScheduleCalendar } = useAdminPageContext()
	const [date, setDate] = useState(new Date())
	const timeZone = getAuthTokenSession()?.account?.time_zone?.name?.split(' ').pop()
	const langSession = getLangSession()?.lang?.code?.toLowerCase()

	const getDate = (str, momentObj) => {
		return momentObj(str, 'YYYY-MM-DD').toDate()
	}

	const locales = {
		ja: ja,
		en: enUS
	}

	console.log('============= scheduleCalendar', scheduleCalendar)

	const onNavigate = useCallback(newDate => setDate(newDate), [setDate])
	const newEvents = scheduleCalendar?.data?.map(item => ({
		id: item?.id,
		title: type === 'user' ? item?.level_name : item?.name,
		start: new Date(`${item?.event_date} ${item?.start_time}`),
		end: new Date(`${item?.event_date} ${item?.end_time}`)
	}))

	const { control, watch } = useForm({
		mode: 'onTouched',
		defaultValues: {
			is_different: false
		}
	})

	const dayPropGetter = useCallback(date => {
		return {
			...(moment(date).day() === 0 && {
				style: {
					backgroundColor: '#e79594',
					color: 'white'
				}
			}),
			...(moment(date).day() === 6 && {
				style: {
					backgroundColor: '#e79594',
					color: 'white'
				}
			})
		}
	}, [])

	const onRangeChange = useCallback(range => {
		if (!range?.length)
			getScheduleCalendar({
				per_page: 1000,
				is_different: watch('is_different') === true ? 0 : 1,
				year_month: moment(addDays(range?.start, 8)).format('YYYY-MM')
			})
	}, [])

	const { components, defaultDate, max, views, getNow, localizer, scrollToTime } = useMemo(() => {
		moment.tz.setDefault(timeZone)
		return {
			components: {
				timeSlotWrapper: ColoredDateCellWrapper
			},
			defaultDate: getDate(date, moment),
			getNow: () => moment().toDate(),
			localizer: dateFnsLocalizer({
				format,
				parse,
				startOfWeek,
				getDay,
				locales
			}),
			max: dates.add(dates.endOf(new Date(), 'day'), -1, 'hours'),
			scrollToTime: moment().toDate(),
			views: ['week', 'month']
		}
	}, [timeZone])

	const onSelectEvent = useCallback(calEvent => {
		if (type === 'user') {
			navigate(ROUTER_STUDENT.lesson_list_student + '/detail/' + calEvent?.id)
		} else if (type === 'teacher') {
			navigate(ROUTER_TEACHER.lesson_teacher + '/detail/' + calEvent?.id)
		}
	}, [])

	useUpdateEffect(() => {
		if (type === 'teacher') {
			getScheduleCalendar({
				per_page: 1000,
				is_different: watch('is_different') === true ? 0 : 1,
				year_month: moment(date).format('YYYY-MM')
			})
		}
	}, [watch('is_different'), date])

	return (
		<div className="w-full flex flex-wrap">
			{type === 'teacher' && (
				<CoreCheckbox
					control={control}
					name="is_different"
					label={t('label.is_different')}
					className="mx-auto"
				/>
			)}
			{loadingScheduleCalendar ? (
				<div className="w-full">
					<div className="mt-40 text-center">
						<CircularProgress />
					</div>
				</div>
			) : (
				<div className="w-full flex h-md">
					<Calendar
						date={date}
						culture={langSession}
						onNavigate={onNavigate}
						components={components}
						defaultDate={defaultDate}
						events={newEvents}
						localizer={localizer}
						getNow={getNow}
						scrollToTime={scrollToTime}
						onSelectEvent={onSelectEvent}
						dayPropGetter={dayPropGetter}
						onRangeChange={onRangeChange}
						max={max}
						popup
						showMultiDayTimes
						step={60}
						views={views}
						className="w-full m-20"
						messages={{
							next: t('calendar.next'),
							previous: t('calendar.previous'),
							today: t('calendar.today'),
							month: t('calendar.month'),
							week: t('calendar.week'),
							showMore: total => `+${total} ${t('calendar.more')}`
						}}
						rtl={false}
					/>
				</div>
			)}
		</div>
	)
}

// ScheduleCalendarContent.defaultProps = {}

// ScheduleCalendarContent.propTypes = {}

export default React.memo(ScheduleCalendarContent)
