/*
 * Created Date: 12-12-2022, 10:09:07 pm
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import AdminCmsLayout from '@App/Cms/components/Layout/AdminCmsLayout'
import AdminContentPage from '@App/Cms/components/Layout/AdminContentPage'
import { TRANSLATE_CMS } from '@App/Cms/configs/constants'
import { getAuthTokenSession, getLangSession, refreshPage } from '@Core/helper/Session'
import { useUpdateEffect } from 'ahooks'
import React, { useEffect } from 'react'
import { useTranslation } from 'react-i18next'
import { useLocation } from 'react-router-dom'
import ScheduleCalendarContent from './components/ScheduleCalendarContent'
import ScheduleCalendarProvider from './ScheduleCalendarProvider'
// import PropTypes from 'prop-types'

const ScheduleCalendar = props => {
	const { t } = useTranslation(TRANSLATE_CMS.schedule_calendar)
	const { type } = props

	return (
		<ScheduleCalendarProvider>
			<AdminContentPage
				pageTitle={type === 'user' ? t('title.title_page_student') : t('title.title_page_teacher')}
				content={<ScheduleCalendarContent {...props} />}
			/>
		</ScheduleCalendarProvider>
	)
}

// ScheduleCalendar.defaultProps = {}

// ScheduleCalendar.propTypes = {}

export default React.memo(ScheduleCalendar)
