/*
 * Created Date: 12-12-2022, 10:23:35 pm
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import AdminPageProvider from '@App/Cms/components/Provider/AdminPageProvider'
import { TRANSLATE_CMS } from '@App/Cms/configs/constants'
import { lessonService } from '@App/Cms/services/lessonService'
import { scheduleCalendarService } from '@App/Cms/services/scheduleCalendarService'
import { useRequest } from 'ahooks'
import moment from 'moment'
import React, { useEffect } from 'react'
import { useTranslation } from 'react-i18next'
// import PropTypes from 'prop-types'

const ScheduleCalendarProvider = props => {
	const requestScheduleCalendar = useRequest(lessonService.list, {
		manual: true,
		onError: error => errorMsg(error?.response?.data?.message)
	})

	const {
		data: scheduleCalendar,
		run: getScheduleCalendar,
		loading: loadingScheduleCalendar
	} = requestScheduleCalendar

	useEffect(() => {
		// accountTableHandler.handleFetchData()
		getScheduleCalendar({ per_page: 1000, is_different: 1, year_month: moment(new Date()).format('YYYY-MM') })
	}, [])

	const data = {
		...props,
		scheduleCalendar,
		loadingScheduleCalendar,
		getScheduleCalendar
	}

	return <AdminPageProvider {...data}>{props.children}</AdminPageProvider>
}

// ScheduleCalendarProvider.defaultProps = {}

// ScheduleCalendarProvider.propTypes = {}

export default React.memo(ScheduleCalendarProvider)
