/*
 * Created Date: 15-12-2022, 4:06:55 pm
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import AdminContentPage from '@App/Cms/components/Layout/AdminContentPage'
import { ROUTER_EXE, TRANSLATE_CMS } from '@App/Cms/configs/constants'
import { Box, Button } from '@mui/material'
import React from 'react'
import { Helmet } from 'react-helmet'
import { useTranslation } from 'react-i18next'
import { useNavigate } from 'react-router-dom'
import ExecutiveAccountTable from './components/ExecutiveAccountTable'
import ExecutiveAccountProvider from './ExecutiveAccountProvider'
// import PropTypes from 'prop-types'

const ListExecutiveAccount = props => {
	const { t } = useTranslation(TRANSLATE_CMS.executive_account)
	const navigate = useNavigate()

	return (
		<ExecutiveAccountProvider>

			<AdminContentPage
				pageTitle={t('title.title_page')}
				content={
					<>
						<ExecutiveAccountTable />
						<Box className="text-right my-40 mr-40">
							<Button
								variant="contained"
								color="primary"
								onClick={() => navigate(ROUTER_EXE.executive_account.list + '/edit/new')}
								className="px-20 w-160 text-18"
							>
								{t('common:btn.add')}
							</Button>
						</Box>
					</>
				}
			/>
		</ExecutiveAccountProvider>
	)
}

// ListExecutiveAccount.defaultProps = {}

// ListExecutiveAccount.propTypes = {}

export default React.memo(ListExecutiveAccount)
