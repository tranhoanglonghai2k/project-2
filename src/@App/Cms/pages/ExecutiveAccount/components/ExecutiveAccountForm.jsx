/*
 * Created Date: 15-12-2022, 4:10:20 pm
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import AdminInput from '@App/Cms/components/Input/AdminInput'
import { useAdminPageContext } from '@App/Cms/components/Provider/AdminPageProvider'
import { ROUTER_EXE, TRANSLATE_CMS } from '@App/Cms/configs/constants'
import { executiveAccountService } from '@App/Cms/services/executiveAccountService'
import { errorMsg, handleErrorFieldBackend, successMsg } from '@Core/helper/Message'
import Yup from '@Core/helper/Yup'
import { yupResolver } from '@hookform/resolvers/yup'
import { LoadingButton } from '@mui/lab'
import { Box } from '@mui/material'
import React from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { useNavigate } from 'react-router-dom'
// import PropTypes from 'prop-types'

const ExecutiveAccountForm = props => {
	const { executiveAccountTableHandler, isEdit } = useAdminPageContext()
	const { executiveAccountData } = props
	const navigate = useNavigate()
	const { t } = useTranslation(TRANSLATE_CMS.executive_account)

	const {
		control,
		handleSubmit,
		formState: { isSubmitting },
		setError
	} = useForm({
		mode: 'onTouched',
		defaultValues: {
			id: executiveAccountData?.id ?? null,
			login_id: executiveAccountData?.login_id ?? '',
			mail_address: executiveAccountData?.mail_address ?? '',
			name: executiveAccountData?.name ?? ''
		},
		resolver: yupResolver(
			Yup.object({
				login_id: Yup.string()
					.trim()
					.required(t('common:validation.required'))
					.min(6, t('common:validation.string.min_login_id', { value: 6 }))
					.max(16, t('common:validation.string.max_login_id', { value: 16 })),
				mail_address: Yup.string()
					.trim()
					.required(t('common:validation.required'))
					.email(t('common:validation.string.valid_email'))
					.max(255, t('common:validation.string.max_length_email', { value: 255 })),
				name: Yup.string().trim().required(t('common:validation.required'))
			})
		)
	})

	const onSubmit = handleSubmit(async data => {
		try {
			await executiveAccountService.save(data)
			navigate(ROUTER_EXE.executive_account.list)
			successMsg(isEdit ? t('message.edit_success') : t('message.create_success'))
		} catch (error) {
			if (typeof error?.response?.data?.message === 'string') {
				errorMsg(error?.response?.data?.message)
			} else {
				handleErrorFieldBackend(error, setError)
			}
		}
	})

	return (
		<form onSubmit={onSubmit}>
			<Box className="max-w-lg sm:mt-40 mt-20 mx-auto sm:p-12">
				<AdminInput
					label={t('label.login_id')}
					control={control}
					name="login_id"
					placeholder=""
					size="small"
					className="mb-16 sm:mb-20 p-12"
					// required
				/>

				<AdminInput
					label={t('label.mail_address')}
					control={control}
					name="mail_address"
					className="mb-16 sm:mb-20 p-12"
					placeholder=""
					size="small"
					// required
				/>

				<AdminInput
					label={t('label.name')}
					control={control}
					name="name"
					className="mb-16 sm:mb-20 p-12"
					placeholder=""
					size="small"
					// required
				/>

				<Box className="flex flex-wrap sm:flex-nowrap mb-16 sm:mb-20">
					<Box className="w-full lg:w-1/3 sm:w-2/5 m-12 mx-auto px-20 sm:p-0">
						<LoadingButton
							type="submit"
							loading={isSubmitting}
							variant="contained"
							color="primary"
							size="small"
							className="w-full text-16"
						>
							{isEdit ? t('common:btn.update') : t('common:btn.sign_up')}
						</LoadingButton>
					</Box>
				</Box>
			</Box>
		</form>
	)
}

// ExecutiveAccountForm.defaultProps = {}

// ExecutiveAccountForm.propTypes = {}

export default React.memo(ExecutiveAccountForm)
