/*
 * Created Date: 15-12-2022, 4:38:03 pm
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import { ROUTER_EXE, TRANSLATE_CMS } from '@App/Cms/configs/constants'
import { Box, Button, Typography } from '@mui/material'
import React from 'react'
import { useTranslation } from 'react-i18next'
import { useNavigate, useParams } from 'react-router-dom'
// import PropTypes from 'prop-types'

const ExecutiveAccountDetailContent = props => {
	const { t } = useTranslation(TRANSLATE_CMS.executive_account)
	const { id } = useParams()
	const { executiveAccountData } = props
	const navigate = useNavigate()

	return (
		<Box className="w-full mt-40 flex">
			<Box className="w-full lg:w-1/2 mx-auto">
				<Box className="w-full flex mb-20">
					<Typography className="w-full text-xl mr-20 lg:w-1/2 lg:mr-40 text-right">
						{t('label.login_id')}
					</Typography>
					<Typography className="w-full text-xl ml-20 lg:w-1/2 lg:ml-40 break-all text-left">
						{executiveAccountData?.login_id}
					</Typography>
				</Box>
				<Box className="w-full flex mb-20">
					<Typography className="w-full text-xl mr-20 lg:w-1/2 lg:mr-40 text-right">
						{t('label.mail_address')}
					</Typography>
					<Typography className="w-full text-xl ml-20 lg:w-1/2 lg:ml-40 break-all text-left">
						{executiveAccountData?.mail_address}
					</Typography>
				</Box>
				<Box className="w-full flex mb-20">
					<Typography className="w-full text-xl mr-20 lg:w-1/2 lg:mr-40 text-right">
						{t('label.name')}
					</Typography>
					<Typography className="w-full text-xl ml-20 lg:w-1/2 lg:ml-40 break-all text-left">
						{executiveAccountData?.name}
					</Typography>
				</Box>

				<Box className="w-full mt-40 flex">
					<Button
						onClick={() => navigate(ROUTER_EXE.executive_account.list)}
						variant="contained"
						color="primary"
						className="w-1/2 mx-10 text-center text-16"
					>
						{t('common:btn.return_list')}
					</Button>
					<Button
						// onClick={() =>
						// 	navigate(ROUTER_EXE.executive_account.list + `/edit/${executiveAccountData?.id}`, {
						// 		state: executiveAccountData
						// 	})
						// }
						onClick={() =>
							navigate(ROUTER_EXE.executive_account.list + `/edit/${id}`, {
								state: { ...executiveAccountData, id }
							})
						}
						variant="contained"
						color="primary"
						className="w-1/2 mx-10 text-center text-16"
					>
						{t('common:btn.edit')}
					</Button>
				</Box>
			</Box>
		</Box>
	)
}

// ExecutiveAccountDetailContent.defaultProps = {}

// ExecutiveAccountDetailContent.propTypes = {}

export default React.memo(ExecutiveAccountDetailContent)
