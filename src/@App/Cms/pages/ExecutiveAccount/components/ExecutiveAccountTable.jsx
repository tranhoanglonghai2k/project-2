/*
 * Created Date: 15-12-2022, 4:09:20 pm
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import { useAdminPageContext } from '@App/Cms/components/Provider/AdminPageProvider'
import { ROUTER_EXE, TRANSLATE_CMS } from '@App/Cms/configs/constants'
import { CoreActionDelete, CoreActionView } from '@Core/components/Table/components/CoreTableAction'
import CoreTable, { columnHelper } from '@Core/components/Table/CoreTable'
import { Box } from '@mui/material'
import React, { useMemo } from 'react'
import { useTranslation } from 'react-i18next'
import { useNavigate } from 'react-router-dom'
import { useDataFake } from '../../Lesson/hooks/useDataFake'
import ExecutiveAccountTableFilter from './ExecutiveAccountTableFilter'
// import PropTypes from 'prop-types'

const ExecutiveAccountTable = props => {
	const { t } = useTranslation(TRANSLATE_CMS.executive_account)
	const navigate = useNavigate()
	const { executiveAccountTableHandler, handleDeleteExecutiveAccount } = useAdminPageContext()

	const columns = useMemo(() => {
		return [
			columnHelper.accessor('id', {
				cell: info => info.getValue(),
				header: 'ID',
				className: 'w-[10%]'
			}),
			columnHelper.accessor('login_id', {
				header: t('label.login_id')
			}),
			columnHelper.accessor('mail_address', {
				header: t('label.mail_address')
			}),
			columnHelper.accessor('name', {
				header: t('label.name')
			}),
			columnHelper.accessor('action', {
				header: t('label.action'),
				className: 'w-[15%]',
				cell: ({ row }) => {
					const data = row.original

					return (
						<div className="flex">
							<CoreActionView
								onClick={() => navigate(ROUTER_EXE.executive_account.list + `/detail/${data?.id}`)}
							/>
							<CoreActionDelete onConfirmDelete={() => handleDeleteExecutiveAccount(data?.id)} />
						</div>
					)
				}
			})
		]
	}, [t])

	return (
		<Box className="m-12">
			<ExecutiveAccountTableFilter className="mb-20" />
			<CoreTable isShowPagination columns={columns} {...executiveAccountTableHandler} />
		</Box>
	)
}

// ExecutiveAccountTable.defaultProps = {}

// ExecutiveAccountTable.propTypes = {}

export default React.memo(ExecutiveAccountTable)
