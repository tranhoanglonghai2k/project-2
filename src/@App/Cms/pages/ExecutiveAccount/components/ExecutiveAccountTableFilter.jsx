/*
 * Created Date: 15-12-2022, 4:09:40 pm
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import { useAdminPageContext } from '@App/Cms/components/Provider/AdminPageProvider'
import { TRANSLATE_CMS } from '@App/Cms/configs/constants'
import CoreInput from '@Core/components/Input/CoreInput'
import { Box, Button } from '@mui/material'
import clsx from 'clsx'
import React from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
// import PropTypes from 'prop-types'

const ExecutiveAccountTableFilter = props => {
	const { className } = props
	const { executiveAccountTableHandler } = useAdminPageContext()
	const { t } = useTranslation(TRANSLATE_CMS.executive_account)
	const handleFilter = () => {
		const data = getValues()

		const params = {
			...data,
			page: 1
		}

		executiveAccountTableHandler.handleFetchData(params)
	}

	const { control, getValues, watch } = useForm({
		mode: 'onTouched',
		defaultValues: {
			login_id: '',
			mail_address: '',
			name: ''
		}
	})

	return (
		<Box className={clsx('border-1 rounded-4 border-grey-300', className)}>
			<Box className="p-8 w-full lg:flex lg:flex-wrap">
				<Box className="flex w-full lg:w-1/2 mb-20 lg:mb-10 items-start">
					<Box className="w-2/5 px-20 text-base h-full my-auto text-left lg:text-right">
						{t('label.login_id')}
					</Box>
					<CoreInput
						control={control}
						name="login_id"
						size="small"
						className="lg:w-2/3 w-3/5"
						fullWidth
						variant="outlined"
					/>
				</Box>
				<Box className="flex w-full lg:w-1/2 mb-20 lg:mb-10 items-start">
					<Box className="w-2/5 px-20 text-base h-full my-auto text-left lg:text-right">
						{t('label.mail_address')}
					</Box>
					<CoreInput
						control={control}
						name="mail_address"
						size="small"
						className="lg:w-2/3 w-3/5"
						fullWidth
						variant="outlined"
					/>
				</Box>
				<Box className="flex w-full lg:w-1/2 mb-20 lg:mb-10 items-start">
					<Box className="w-2/5 px-20 text-base h-full my-auto text-left lg:text-right">
						{t('label.name')}
					</Box>
					<CoreInput
						control={control}
						name="name"
						size="small"
						className="lg:w-2/3 w-3/5"
						fullWidth
						variant="outlined"
					/>
				</Box>
			</Box>
			<Box className="flex p-8 w-full">
				<Button
					variant="contained"
					color="primary"
					className="ml-auto w-2/5 p-12 sm:p-8 lg:p-5 lg:w-160 text-18"
					onClick={handleFilter}
				>
					{t('common:btn.search')}
				</Button>
			</Box>
		</Box>
	)
}

// ExecutiveAccountTableFilter.defaultProps = {}

// ExecutiveAccountTableFilter.propTypes = {}

export default React.memo(ExecutiveAccountTableFilter)
