/*
 * Created Date: 15-12-2022, 4:35:41 pm
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import AdminContentPage from '@App/Cms/components/Layout/AdminContentPage'
import { TRANSLATE_CMS } from '@App/Cms/configs/constants'
import React from 'react'
import { useTranslation } from 'react-i18next'
import { useLocation, useParams } from 'react-router-dom'
import ExecutiveAccountForm from './components/ExecutiveAccountForm'
import ExecutiveAccountProvider from './ExecutiveAccountProvider'
// import PropTypes from 'prop-types'

const EditExecutiveAccount = props => {
	const { t } = useTranslation(TRANSLATE_CMS.executive_account)
	const { id } = useParams()
	const isEdit = id !== 'new'
	const location = useLocation()

	return (
		<ExecutiveAccountProvider isEdit={isEdit}>
			<AdminContentPage
				pageTitle={isEdit ? t('title.edit_page') : t('title.add_page')}
				content={<ExecutiveAccountForm executiveAccountData={location?.state} />}
			/>
		</ExecutiveAccountProvider>
	)
}

// EditExecutiveAccount.defaultProps = {}

// EditExecutiveAccount.propTypes = {}

export default React.memo(EditExecutiveAccount)
