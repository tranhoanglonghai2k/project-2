/*
 * Created Date: 15-12-2022, 4:08:39 pm
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import AdminContentPage from '@App/Cms/components/Layout/AdminContentPage'
import { TRANSLATE_CMS } from '@App/Cms/configs/constants'
import { Box, CircularProgress } from '@mui/material'
import React from 'react'
import { useTranslation } from 'react-i18next'
import { useLocation } from 'react-router-dom'
import ExecutiveAccountDetailContent from './components/ExecutiveAccountDetailContent'
import ExecutiveAccountProvider from './ExecutiveAccountProvider'
import { useDetailExecutiveAccount } from './hooks/useDetailExecutiveAccount'
// import PropTypes from 'prop-types'

const DetailExecutiveAccount = props => {
	const { t } = useTranslation(TRANSLATE_CMS.executive_account)
	const { adminData, loadingAdmin } = useDetailExecutiveAccount()

	return (
		<ExecutiveAccountProvider>
		
			<AdminContentPage
				pageTitle={t('title.detail_page')}
				content={
					loadingAdmin ? (
						<Box className="mt-40 text-center">
							<CircularProgress />
						</Box>
					) : (
						<ExecutiveAccountDetailContent executiveAccountData={adminData} />
					)
				}
			/>
		</ExecutiveAccountProvider>
	)
}

// DetailExecutiveAccount.defaultProps = {}

// DetailExecutiveAccount.propTypes = {}

export default React.memo(DetailExecutiveAccount)
