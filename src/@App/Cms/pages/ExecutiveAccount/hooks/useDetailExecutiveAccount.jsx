/*
 * Created Date: 20-12-2022, 9:01:58 pm
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import { executiveAccountService } from '@App/Cms/services/executiveAccountService'
import { errorMsg } from '@Core/helper/Message'
import { useRequest } from 'ahooks'
import { useEffect } from 'react'
import { useParams } from 'react-router-dom'

export const useDetailExecutiveAccount = props => {
	const { id } = useParams()

	const {
		data: adminData,
		run: fetchAdmin,
		loading: loadingAdmin
	} = useRequest(executiveAccountService.find, {
		manual: true,
		onError: error => errorMsg(error?.response?.data?.message)
	})

	useEffect(() => {
		if (id) {
			fetchAdmin(id)
		}
	}, [])

	return { adminData, loadingAdmin }
}
