/*
 * Created Date: 13-12-2022, 9:41:59 am
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import AdminPageProvider from '@App/Cms/components/Provider/AdminPageProvider'
import { TRANSLATE_CMS } from '@App/Cms/configs/constants'
import { settingService } from '@App/Cms/services/settingService'
import { useRequest } from 'ahooks'
import React, { useEffect } from 'react'
import { useTranslation } from 'react-i18next'
// import PropTypes from 'prop-types'

const SettingProvider = props => {
	const { type } = props
	const {
		data: detailAccount,
		run: fetchDetailAccount,
		loading: loadingDetailAccount
	} = useRequest(settingService.getDetailAccount, {
		manual: true
	})

	useEffect(() => {
		fetchDetailAccount(type)
	}, [])

	const data = {
		...props,
		detailAccount,
		loadingDetailAccount
	}

	return <AdminPageProvider {...data}>{props.children}</AdminPageProvider>
}

// SettingProvider.defaultProps = {}

// SettingProvider.propTypes = {}

export default React.memo(SettingProvider)
