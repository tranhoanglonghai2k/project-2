/*
 * Created Date: 13-12-2022, 9:44:32 am
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import { TRANSLATE_CMS } from '@App/Cms/configs/constants'
import { errorMsg, handleErrorFieldBackend, successMsg } from '@Core/helper/Message'
import { CircularProgress, Typography } from '@mui/material'
import React, { useEffect, useState } from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { useNavigate } from 'react-router-dom'
import SelectTimezone from './SelectTimezone'
import moment from 'moment'
import SelectLanguage from './SelectLanguage'
import { useAdminPageContext } from '@App/Cms/components/Provider/AdminPageProvider'
import { getAuthTokenSession, refreshPage, setAuthTokenSession, setLangSession } from '@Core/helper/Session'
import OperatingTime from './OperatingTime/OperatingTime'
import ResetPassword from './ResetPassword/ResetPassword'
import { settingService } from '@App/Cms/services/settingService'
import { studentService } from '@App/Cms/services/studentService'
import useLevelLabel from '@App/Cms/hooks/useLevelLabel'
import { useRequest, useUpdateEffect } from 'ahooks'
import ZoomUrl from './ZoomUrl/ZoomUrl'
// import PropTypes from 'prop-types'

const SettingContent = props => {
	const { t } = useTranslation(TRANSLATE_CMS.setting)
	const { type, detailAccount, loadingDetailAccount } = useAdminPageContext()
	const user = getAuthTokenSession()
	const [currentTime, setCurrentTime] = useState('')

	const { data: timezoneData, run: fetchTimezone } = useRequest(settingService.getTimezone, {
		manual: true
	})

	const { data: lang, run: fetchLang } = useRequest(settingService.getLanguage, {
		manual: true
	})

	const {
		data: attendingTime,
		run: fetchAttendingTime,
		loading: loadingAttendingTime
	} = useRequest(studentService.getAttendingTime, {
		manual: true,
		onError: error => errorMsg(error?.response?.data?.message)
	})

	useEffect(() => {
		if (type === 'user') {
			fetchTimezone(type)
			fetchAttendingTime(user.account.id)
			fetchLang(type)
		}
	}, [type])

	const { control, watch } = useForm({
		mode: 'onTouched',
		defaultValues: {
			timezone_id: user?.account?.timezone_id ?? null,
			lang_id: user?.account?.lang_id ?? null
		}
	})

	useUpdateEffect(() => {
		if (watch('timezone_id')) {
			settingService.saveTimezone({ timezone_id: watch('timezone_id') }, type)
			const timezoneItem = timezoneData?.data?.find(item => item.id === watch('timezone_id'))
			if (timezoneItem) setCurrentTime(timezoneItem?.code)
			console.log(timezoneItem)

			setAuthTokenSession({
				...user,
				account: {
					...user?.account,
					timezone_id: watch('timezone_id'),
					time_zone: timezoneItem,
					typeAccount: type
				}
			})
		}
	}, [watch('timezone_id'), timezoneData])

	useUpdateEffect(() => {
		if (watch('lang_id')) {
			settingService.saveLang({ lang_id: watch('lang_id') }, type)
			const langItem = lang?.data?.find(item => item.id === watch('lang_id'))
			setAuthTokenSession({
				...user,
				account: {
					...user?.account,
					lang_id: watch('lang_id')
				}
			})
			setLangSession({ lang: langItem, typeAccount: type })
			successMsg(t('common:message.change_language_success'))
			refreshPage()
		}
	}, [watch('lang_id')])

	function calcTime(timezoneString) {
		const utcString = timezoneString.split(' ')[1] ? timezoneString.split(' ')[0] : timezoneString.split(' ')[0]
		const index = utcString.indexOf('-') || utcString.indexOf('+')
		const utcNumber = utcString.slice(index)

		var d = new Date()
		var utc = d.getTime() + d.getTimezoneOffset() * 60000
		var nd = new Date(utc + 3600000 * utcNumber)

		return nd
	}

	return (
		<div className="w-full flex">
			<div className="w-full sm:w-1/3 m-auto">
				{/* <form onSubmit={onSubmit}> */}
				<div className="w-full mt-40 flex flex-wrap">
					{type === 'user' ? (
						<div className="w-full border mb-40 border-grey-300 rounded p-20">
							<Typography className="w-full text-24 mb-10 font-bold text-center">
								{t('label.time_zone')}
							</Typography>
							<SelectTimezone
								control={control}
								name="timezone_id"
								className="w-full"
								data={timezoneData}
								disableClearable
								// label={t('label.time_zone')}
								// placeholder="Choose timezone..."
							/>
							<Typography className="w-full text-16 mt-10 font-bold text-center">{`${t(
								'label.time_current'
							)} : ${moment(calcTime(currentTime)).format('YYYY/MM/DD HH:mm:ss')}`}</Typography>
						</div>
					) : type === 'teacher' ? (
						<>
							<OperatingTime type={type} />
							<ZoomUrl />
						</>
					) : null}

					{type === 'user' ? (
						<div className="w-full border mb-40 border-grey-300 rounded p-20">
							<Typography className="w-full text-24 mb-10 font-bold text-center">
								{t('label.language')}
							</Typography>
							<SelectLanguage
								control={control}
								name="lang_id"
								className="w-full"
								lang={lang}
								disableClearable
								// label={t('label.time_zone')}
								// placeholder="Choose language..."
							/>
						</div>
					) : null}

					<div className="w-full border mb-40 flex flex-wrap border-grey-300 rounded p-20">
						<Typography className="w-full text-24 mb-10 font-bold text-center">
							{t('label.info')}
						</Typography>

						{loadingDetailAccount || loadingAttendingTime ? (
							<div className="w-full flex">
								<div className="mx-auto">
									<CircularProgress />
								</div>
							</div>
						) : (
							<div className="mb-20 mx-auto text-right">
								<Typography className="w-full text-20 text-center font-bold">{`${t('label.name')}: ${
									detailAccount?.name
								}`}</Typography>
								<Typography className="w-full text-20 text-center font-bold">{`${t(
									'label.login_id'
								)}: ${detailAccount?.login_id}`}</Typography>
								<Typography className="w-full text-20 text-center font-bold">
									{`${t('label.mail_address')}: ${detailAccount?.mail_address}`}
								</Typography>
								{type === 'user' && (
									<>
										<Typography className="w-full text-20 text-center font-bold">
											{`${t('label.level')}: ${detailAccount?.levels?.name}`}
										</Typography>
										<Typography className="w-full text-20 text-center font-bold">
											{`${t('label.attending_time')}: ${
												attendingTime?.attending_time ? attendingTime?.attending_time : ''
											}`}
										</Typography>
									</>
								)}
							</div>
						)}
					</div>

					<div className="w-full border mb-40 border-grey-300 rounded p-20">
						<Typography className="w-full text-24 mb-10 font-bold text-center">
							{t('label.password_update')}
						</Typography>

						<ResetPassword type={type} />
					</div>
				</div>
				{/* </form> */}
			</div>
		</div>
	)
}

// SettingContent.defaultProps = {}

// SettingContent.propTypes = {}

export default React.memo(SettingContent)
