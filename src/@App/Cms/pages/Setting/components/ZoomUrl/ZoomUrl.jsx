/*
 * Created Date: 17-01-2023, 3:50:01 pm
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2023 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import AdminInput from '@App/Cms/components/Input/AdminInput'
import { TRANSLATE_CMS } from '@App/Cms/configs/constants'
import { settingService } from '@App/Cms/services/settingService'
import CoreInput from '@Core/components/Input/CoreInput'
import { errorMsg, handleErrorFieldBackend, successMsg } from '@Core/helper/Message'
import { LoadingButton } from '@mui/lab'
import { Box, Typography } from '@mui/material'
import React from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
// import PropTypes from 'prop-types'

const ZoomUrl = props => {
	const { t } = useTranslation(TRANSLATE_CMS.setting)

	const {
		control,
		handleSubmit,
		watch,
		reset,
		formState: { isSubmitting, isDirty },
		setError
	} = useForm({
		mode: 'onTouched',
		defaultValues: {
			zoom_api_key: '',
			zoom_api_secret: ''
		}
	})

	const onSubmit = handleSubmit(async data => {
		try {
			await settingService.saveZoomUrl(data)
			reset()
			successMsg(t('common:message.send_success'))
		} catch (error) {
			if (typeof error?.response?.data?.message === 'string') {
				errorMsg(error?.response?.data?.message)
			} else {
				handleErrorFieldBackend(error, setError)
			}
		}
	})

	return (
		<Box className="w-full border mb-40 border-grey-300 rounded p-20">
			<form onSubmit={onSubmit}>
				<Box className="w-full mb-10 p-10">
					<Box className="w-full mt-12 text-center">
						<Typography variant="h3" className="whitespace-normal w-full text-24 font-bold text-center">
							{t('label.zoom_url')}
						</Typography>
					</Box>
				</Box>

				<Box className="w-full mb-10 p-10">
					<Box className="w-full mt-12 mb-10">
						<Typography variant="h3" className="whitespace-normal w-full text-20 text-left px-10">
							{t('label.zoom_api_key')}
						</Typography>
					</Box>

					<Box className="rounded-md w-full">
						<CoreInput
							control={control}
							fullWidth
							name="zoom_api_key"
							placeholder=""
							size="small"
							className="p-10"
						/>
					</Box>
				</Box>

				<Box className="w-full mb-10 p-10">
					<Box className="w-full mt-12 mb-10">
						<Typography variant="h3" className="whitespace-normal w-full text-20 text-left px-10">
							{t('label.zoom_api_secret')}
						</Typography>
					</Box>

					<Box className="rounded-md w-full">
						<CoreInput
							control={control}
							fullWidth
							name="zoom_api_secret"
							placeholder=""
							size="small"
							className="p-10"
						/>
					</Box>
				</Box>

				<Box className="flex flex-wrap sm:flex-nowrap mb-16 sm:mb-20">
					<Box className="w-full sm:w-1/3 m-12 sm:mx-auto">
						<LoadingButton
							type="submit"
							loading={isSubmitting}
							variant="contained"
							color="primary"
							size="small"
							className="w-full text-16"
							disabled={!isDirty}
						>
							{t('common:btn.submit')}
						</LoadingButton>
					</Box>
				</Box>
			</form>
		</Box>
	)
}

// ZoomUrl.defaultProps = {}

// ZoomUrl.propTypes = {}

export default React.memo(ZoomUrl)
