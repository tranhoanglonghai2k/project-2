/*
 * Created Date: 28-12-2022, 9:38:14 pm
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import { useAdminPageContext } from '@App/Cms/components/Provider/AdminPageProvider'
import { TRANSLATE_CMS } from '@App/Cms/configs/constants'
import { useOptions } from '@App/Cms/hooks/useOptions'
import { settingService } from '@App/Cms/services/settingService'
import CoreAutocomplete from '@Core/components/Input/CoreAutocomplete'
import CoreTimePicker from '@Core/components/Input/CoreTimePicker'
import { errorMsg, handleErrorFieldBackend, successMsg } from '@Core/helper/Message'
import { LoadingButton } from '@mui/lab'
import { CircularProgress, Typography } from '@mui/material'
import { useRequest } from 'ahooks'
import moment from 'moment'
import React, { useEffect } from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import OperatingTimeItem from './OperatingTimeItem'
// import PropTypes from 'prop-types'

const OperatingTime = props => {
	const { t } = useTranslation(TRANSLATE_CMS.setting)
	const { eventWeekOptions } = useOptions()
	const { type } = props
	const {
		data: setting,
		run: getSetting,
		loading: loadingSetting
	} = useRequest(settingService.getSetting, {
		manual: true,
		onError: error => {
			errorMsg(error?.response?.data?.message)
			console.log('============= error', error)
		}
	})

	useEffect(() => {
		// accountTableHandler.handleFetchData()
		if (type === 'teacher') getSetting(type, { per_page: 1000 })
	}, [])

	const {
		control,
		handleSubmit,
		reset,
		watch,
		formState: { isSubmitting, isDirty },
		setError
	} = useForm({
		mode: 'onTouched',
		defaultValues: {
			operating_week: null,
			start_time: null,
			end_time: null
		}
	})

	const onSubmit = handleSubmit(async data => {
		try {
			const newData = {
				...data,
				start_time: moment(data?.start_time).format('HH:mm'),
				end_time: moment(data?.end_time).format('HH:mm')
			}
			await settingService.saveSetting(newData, type)
			reset()
			getSetting(type, { per_page: 1000 })
			successMsg(t('message.create_operating_time_success'))
		} catch (error) {
			if (typeof error?.response?.data?.message === 'string') {
				errorMsg(error?.response?.data?.message)
			} else {
				handleErrorFieldBackend(error, setError)
			}
		}
	})

	return (
		<div className="w-full mb-40 border border-grey-300 flex flex-wrap rounded p-20">
			<Typography className="w-full text-24 mb-20 font-bold text-center">{t('label.time_setting')}</Typography>

			{loadingSetting ? (
				<div className="mt-100 mx-auto text-center">
					<CircularProgress />
				</div>
			) : (
				<div className="mb-20 mx-auto text-right">
					{setting?.data?.map(item => {
						return <OperatingTimeItem key={item?.id} item={item} type={type} getSetting={getSetting} />
					})}
				</div>
			)}

			<form onSubmit={onSubmit}>
				<div className="w-full mt-20 flex flex-wrap">
					<Typography className="w-full text-24 mb-20 font-bold text-center">
						{t('label.addition')}
					</Typography>
					<div className="w-full flex">
						<CoreAutocomplete
							control={control}
							name="operating_week"
							size="small"
							className="w-full sm:w-1/3 mr-10"
							fullWidth
							variant="outlined"
							valuePath="value"
							labelPath="label"
							returnValueType="enum"
							placeholder="曜日|祝日"
							options={eventWeekOptions}
						/>
						<CoreTimePicker
							control={control}
							name="start_time"
							size="small"
							className="w-full sm:w-1/3 mr-10"
							fullWidth
							variant="outlined"
							placeholder=""
						/>
						<CoreTimePicker
							control={control}
							name="end_time"
							size="small"
							className="w-full sm:w-1/3"
							fullWidth
							variant="outlined"
							placeholder=""
						/>
					</div>

					<div className="w-full flex mt-20">
						<LoadingButton
							type="submit"
							variant="contained"
							loading={isSubmitting}
							disabled={!watch('operating_week') || !watch('start_time') || !watch('end_time')}
							className="px-20 mx-auto py-8"
						>
							{t('common:btn.sign_up')}
						</LoadingButton>
					</div>
				</div>
			</form>
		</div>
	)
}

// OperatingTime.defaultProps = {}

// OperatingTime.propTypes = {}

export default React.memo(OperatingTime)
