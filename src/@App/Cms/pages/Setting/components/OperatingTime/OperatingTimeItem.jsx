/*
 * Created Date: 29-12-2022, 3:36:56 pm
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import { useAdminPageContext } from '@App/Cms/components/Provider/AdminPageProvider'
import { settingService } from '@App/Cms/services/settingService'
import { useConfirm } from '@Core/components/Confirm/CoreConfirm'
import { errorMsg, successMsg } from '@Core/helper/Message'
import { Button, Typography } from '@mui/material'
import moment from 'moment'
import React from 'react'
import { useTranslation } from 'react-i18next'
// import PropTypes from 'prop-types'

const OperatingTimeItem = props => {
	const { t } = useTranslation('common')
	const { item, type, getSetting } = props
	const confirm = useConfirm()

	const handleDelete = async () => {
		try {
			await settingService.deleteSetting(item?.id, type)
			getSetting(type, { per_page: 1000 })
			successMsg(t('message.delete_success'))
		} catch (error) {
			errorMsg(error?.response?.data?.message)
		}
	}

	return (
		<div className="w-full flex">
			<Typography className="w-full sm:text-24 text-20 my-auto text-right font-bold">
				{item?.operating_week_string ?? item?.public_holiday_string}: {item?.start_time} ~ {item?.end_time}
			</Typography>
			<Button
				className="mx-10 sm:text-24 text-20"
				onClick={() =>
					confirm({
						content: t('table.delete_confirm'),
						color: 'error',
						onOk: handleDelete,
						zIndex: 9999
					})
				}
			>
				{t('btn.delete')}
			</Button>
		</div>
	)
}

// OperatingTimeItem.defaultProps = {}

// OperatingTimeItem.propTypes = {}

export default React.memo(OperatingTimeItem)
