/*
 * Created Date: 13-12-2022, 11:26:09 am
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import { useAdminPageContext } from '@App/Cms/components/Provider/AdminPageProvider'
import { TRANSLATE_CMS } from '@App/Cms/configs/constants'
import { settingService } from '@App/Cms/services/settingService'
import CoreAutocomplete from '@Core/components/Input/CoreAutocomplete'
import { useRequest } from 'ahooks'
import React, { useEffect } from 'react'
import { useTranslation } from 'react-i18next'
import { timezoneOptions } from './timezoneData'
// import PropTypes from 'prop-types'

const SelectLanguage = props => {
	const { t } = useTranslation(TRANSLATE_CMS.setting)
	const { type } = useAdminPageContext()
	const { control, name, className, label, placeholder, lang, ...restProps } = props

	return (
		<CoreAutocomplete
			control={control}
			name={name}
			className={className}
			label={label}
			valuePath="id"
			labelPath="name"
			options={lang?.data}
			returnValueType="enum"
			placeholder={placeholder}
			{...restProps}
		/>
	)
}

// SelectLanguage.defaultProps = {}

// SelectLanguage.propTypes = {}

export default React.memo(SelectLanguage)
