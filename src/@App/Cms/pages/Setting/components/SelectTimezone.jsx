/*
 * Created Date: 13-12-2022, 10:16:36 am
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import { useAdminPageContext } from '@App/Cms/components/Provider/AdminPageProvider'
import { TRANSLATE_CMS } from '@App/Cms/configs/constants'
import { settingService } from '@App/Cms/services/settingService'
import CoreAutocomplete from '@Core/components/Input/CoreAutocomplete'
import { errorMsg } from '@Core/helper/Message'
import { useRequest } from 'ahooks'
import React from 'react'
import { useEffect } from 'react'
import { useTranslation } from 'react-i18next'
import { timezoneOptions } from './timezoneData'
// import PropTypes from 'prop-types'

const SelectTimezone = props => {
	const { t } = useTranslation(TRANSLATE_CMS.setting)
	const { type } = useAdminPageContext()
	const { control, name, className, label, placeholder, ...restProps } = props

	return (
		<CoreAutocomplete
			control={control}
			name={name}
			className={className}
			label={label}
			valuePath="id"
			labelPath="name"
			returnValueType="enum"
			options={props.data?.data}
			placeholder={placeholder}
			{...restProps}
		/>
	)
}

// SelectTimezone.defaultProps = {}

// SelectTimezone.propTypes = {}

export default React.memo(SelectTimezone)
