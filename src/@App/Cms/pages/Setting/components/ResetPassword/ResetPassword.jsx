/*
 * Created Date: 28-12-2022, 10:07:32 pm
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import { useAdminPageContext } from '@App/Cms/components/Provider/AdminPageProvider'
import { ROUTER_CMS, TRANSLATE_CMS } from '@App/Cms/configs/constants'
import { settingService } from '@App/Cms/services/settingService'
import CoreInput from '@Core/components/Input/CoreInput'
import { errorMsg, handleErrorFieldBackend, successMsg } from '@Core/helper/Message'
import Yup from '@Core/helper/Yup'
import { yupResolver } from '@hookform/resolvers/yup'
import { LoadingButton } from '@mui/lab'
import { Typography } from '@mui/material'
import React from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { useNavigate } from 'react-router-dom'
// import PropTypes from 'prop-types'

const ResetPassword = props => {
	const { t } = useTranslation(TRANSLATE_CMS.setting)
	const { type } = props
	const navigate = useNavigate()
	const {
		control,
		handleSubmit,
		reset,
		formState: { isSubmitting, isDirty },
		setError
	} = useForm({
		mode: 'onTouched',
		defaultValues: {
			password: '',
			password_new: '',
			re_password_new: ''
		},
		resolver: yupResolver(
			Yup.object({
				password: Yup.string().required(t('common:validation.required')),
				password_new: Yup.string()
					.required(t('common:validation.required'))
					.min(8, t('common:validation.string.min_password', { value: 8 }))
					.password(),
				re_password_new: Yup.string()
					.required(t('common:validation.required'))
					.oneOf([Yup.ref('password_new')], t('common:validation.not_match'))
			})
		)
	})

	const onSubmit = handleSubmit(async data => {
		try {
			await settingService.resetPassword(data, type)
			reset()
			successMsg(t('message.reset_password_success'))
		} catch (error) {
			if (typeof error?.response?.data?.message === 'string') {
				errorMsg(error?.response?.data?.message)
			} else {
				handleErrorFieldBackend(error, setError)
			}
		}
	})

	return (
		<form onSubmit={onSubmit}>
			<CoreInput
				className="py-10 flex-col"
				control={control}
				name="password"
				type="password"
				size="small"
				label={<Typography className="text-20">{t('label.password_current')}</Typography>}
				labelStyle="mb-8"
				required
				showTextRequired={false}
				// placeholder="メールアドレスを入力してください"
				placeholder=""
			/>

			<CoreInput
				className="py-10 flex-col"
				control={control}
				name="password_new"
				type="password"
				size="small"
				label={<Typography className="text-20">{t('label.password')}</Typography>}
				labelStyle="mb-8"
				required
				showTextRequired={false}
				// placeholder="メールアドレスを入力してください"
				placeholder=""
			/>

			<CoreInput
				className="py-10 flex-col"
				control={control}
				name="re_password_new"
				type="password"
				size="small"
				label={<Typography className="text-20">{t('label.password_confirm')}</Typography>}
				labelStyle="mb-8"
				required
				showTextRequired={false}
				// placeholder="メールアドレスを入力してください"
				placeholder=""
			/>

			<LoadingButton
				loading={isSubmitting}
				disabled={!isDirty}
				variant="contained"
				className="text-18 py-8 px-20 w-full rounded-4 font-bold mt-20"
				type="submit"
			>
				{t('common:btn.update')}
			</LoadingButton>
		</form>
	)
}

// ResetPassword.defaultProps = {}

// ResetPassword.propTypes = {}

export default React.memo(ResetPassword)
