/*
 * Created Date: 13-12-2022, 9:07:55 am
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import AdminContentPage from '@App/Cms/components/Layout/AdminContentPage'
import { TRANSLATE_CMS } from '@App/Cms/configs/constants'
import { getAuthTokenSession, refreshPage } from '@Core/helper/Session'
import React, { useEffect } from 'react'
import { useTranslation } from 'react-i18next'
import { useLocation } from 'react-router-dom'
import SettingContent from './components/SettingContent'
import SettingProvider from './SettingProvider'
// import PropTypes from 'prop-types'

const Setting = props => {
	const { t } = useTranslation(TRANSLATE_CMS.setting)
	const { type } = props

	return (
		<SettingProvider {...props}>
			<AdminContentPage
				pageTitle={type === 'user' ? t('title.title_page_student') : t('title.title_page_teacher')}
				content={<SettingContent />}
			/>
		</SettingProvider>
	)
}

// Setting.defaultProps = {}

// Setting.propTypes = {}

export default React.memo(Setting)
