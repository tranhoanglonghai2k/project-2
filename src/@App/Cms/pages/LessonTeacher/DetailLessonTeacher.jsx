/*
 * Created Date: 23-12-2022, 9:24:59 am
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import AdminContentPage from '@App/Cms/components/Layout/AdminContentPage'
import { TRANSLATE_CMS } from '@App/Cms/configs/constants'
import { CircularProgress } from '@mui/material'
import React from 'react'
import { useTranslation } from 'react-i18next'
import DetailLessonTeacherContent from './components/DetailLessonTeacherContent'
import { useDetailLessonTeacher } from './hooks/useDetailLessonTeacher'
// import PropTypes from 'prop-types'

const DetailLessonTeacher = props => {
	const { t } = useTranslation(TRANSLATE_CMS.lesson_teacher)
	const { lessonTeacher, loadingLessonTeacher, fetchLessonTeacher } = useDetailLessonTeacher()

	return (
		<AdminContentPage
			pageTitle={t('title.reservation_lesson')}
			content={
				loadingLessonTeacher ? (
					<div className="mt-40 text-center">
						<CircularProgress />
					</div>
				) : (
					<DetailLessonTeacherContent
						t={t}
						lessonTeacherData={lessonTeacher}
						fetchLessonTeacher={fetchLessonTeacher}
					/>
				)
			}
		/>
	)
}

// DetailLessonTeacher.defaultProps = {}

// DetailLessonTeacher.propTypes = {}

export default React.memo(DetailLessonTeacher)
