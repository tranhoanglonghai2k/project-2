/*
 * Created Date: 23-12-2022, 9:27:24 am
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import { useAdminPageContext } from '@App/Cms/components/Provider/AdminPageProvider'
import { ROUTER_TEACHER } from '@App/Cms/configs/constants'
import { CircularProgress, Typography } from '@mui/material'
import moment from 'moment'
import React from 'react'
import { useNavigate } from 'react-router-dom'
// import PropTypes from 'prop-types'

const LessonTeacherContent = props => {
	const { lessonListTeacher, loadingLessonListTeacher } = useAdminPageContext()
	const { t } = props
	const navigate = useNavigate()

	return loadingLessonListTeacher ? (
		<div className="mt-40 text-center">
			<CircularProgress />
		</div>
	) : (
		<div className="w-full flex">
			<div className="w-full lg:w-1/3 md:w-1/2 sm:w-4/5 p-10 m-auto">
				<div className="w-full flex flex-col justify-center mt-40">
					<Typography className="mx-auto text-24 font-bold mb-20">{t('title.reservation_lesson')}</Typography>
					{lessonListTeacher?.data?.map((item, index) => {
						return (
							<div
								key={item?.id}
								onClick={() => {
									navigate(ROUTER_TEACHER.lesson_teacher + '/detail/' + item?.lesson_id, {
										state: { is_reserved_lesson: true }
									})
								}}
								className="p-48 my-20 border w-full flex border-grey-300 rounded-12 cursor-pointer"
							>
								<div className="w-7/10 mx-auto">
									<Typography className="w-full text-24 mb-10 text-left">
										{t('label.lesson_name')}: {item?.lesson_name}
									</Typography>
									<Typography className="w-full text-24">
										{t('label.date_time')}:{' '}
										{moment(item?.lesson_event_date).format('YYYY年MM月DD日')}{' '}
										{item?.lesson_start_time}
									</Typography>
								</div>
							</div>
						)
					})}
				</div>
			</div>
		</div>
	)
}

// LessonTeacherContent.defaultProps = {}

// LessonTeacherContent.propTypes = {}

export default React.memo(LessonTeacherContent)
