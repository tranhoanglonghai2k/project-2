/*
 * Created Date: 23-12-2022, 9:27:05 am
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import { ROUTER_TEACHER } from '@App/Cms/configs/constants'
import { useOptions } from '@App/Cms/hooks/useOptions'
import { lessonService } from '@App/Cms/services/lessonService'
import CoreAutocomplete from '@Core/components/Input/CoreAutocomplete'
import { errorMsg, handleErrorFieldBackend, successMsg } from '@Core/helper/Message'
import { getAuthTokenSession } from '@Core/helper/Session'
import { Button, Typography } from '@mui/material'
import moment from 'moment'
import React from 'react'
import { useForm } from 'react-hook-form'
import { useLocation, useNavigate } from 'react-router-dom'
// import PropTypes from 'prop-types'

const DetailLessonTeacherContent = props => {
	const { t, lessonTeacherData, fetchLessonTeacher } = props
	const navigate = useNavigate()
	const { statusTeacherOptions } = useOptions()
	const infoAccount = getAuthTokenSession()
	const location = useLocation()

	const { control, handleSubmit, setError } = useForm({
		mode: 'onTouched',
		defaultValues: {
			id: lessonTeacherData?.id ?? null,
			name: lessonTeacherData?.name ?? '',
			content: lessonTeacherData?.content ?? '',
			event_date: lessonTeacherData?.event_date ? new Date(lessonTeacherData.event_date) : null,
			event_week: `${lessonTeacherData?.event_week}` ?? 1,
			start_time: lessonTeacherData?.start_time ?? '',
			end_time: lessonTeacherData?.end_time ?? '',
			teacher_id: `${lessonTeacherData?.teacher_id}` ?? '1',
			level_id: lessonTeacherData?.level_id ?? '',
			status: lessonTeacherData?.status ?? null
		}
	})

	const onSubmit = handleSubmit(async data => {
		try {
			await lessonService.updateLesson(data, data?.id)
			fetchLessonTeacher(data?.id)
			successMsg(t('common:message.send_success'))
		} catch (error) {
			if (typeof error?.response?.data?.message === 'string') {
				errorMsg(error?.response?.data?.message)
			} else {
				handleErrorFieldBackend(error, setError)
			}
		}
	})

	return (
		<div className="w-full flex">
			<div className="w-full sm:w-2/3 sm:p-0 p-8 mx-auto mt-40 text-center">
				<Typography className="w-full text-24 mb-20">
					{t('label.lesson_name')}: {lessonTeacherData?.name}
				</Typography>
				<Typography className="w-full text-24 mb-20">
					{t('label.date_time')}: {moment(lessonTeacherData?.event_date).format('YYYY年MM月DD日')} (
					{lessonTeacherData?.start_time} ~ {lessonTeacherData?.end_time})
				</Typography>
				{lessonTeacherData?.status === 0 && (
					<Typography className="w-full text-24">
						{t('label.zoom_url')}:
						<a href={lessonTeacherData?.zoom_url} target="_blank">
							{lessonTeacherData?.zoom_url}
						</a>
					</Typography>
				)}
				<Typography className="mx-auto text-24 my-40">{t('label.detail')}</Typography>
				<form onSubmit={onSubmit}>
					<Typography className="w-full text-24 mb-40">{lessonTeacherData?.content}</Typography>

					{new Date() > new Date(`${lessonTeacherData?.event_date} ${lessonTeacherData?.end_time}`) && (
						<>
							<CoreAutocomplete
								control={control}
								name="status"
								size="small"
								className="my-20 w-full mx-auto sm:w-1/3"
								fullWidth
								variant="outlined"
								valuePath="value"
								labelPath="label"
								returnValueType="enum"
								placeholder="未実施 | 実施済み"
								options={statusTeacherOptions}
								disabled={lessonTeacherData?.teacher_id !== infoAccount?.account?.id}
							/>

							<div>
								<Button
									type="submit"
									variant="contained"
									className="p-12 sm:p-8 lg:p-5 text-18 w-full lg:w-1/4 sm:w-1/2 my-20"
									disabled={lessonTeacherData?.teacher_id !== infoAccount?.account?.id}
								>
									{t('common:btn.update')}
								</Button>
							</div>
						</>
					)}
					<div>
						<Button
							variant="contained"
							className="p-12 sm:p-8 lg:p-5 w-full lg:w-1/4 sm:w-1/2 text-18 my-20"
							onClick={() => {
								if (location?.state?.is_reserved_lesson) {
									navigate(ROUTER_TEACHER.lesson_teacher)
								} else {
									navigate(ROUTER_TEACHER.schedule_calendar)
								}
							}}
						>
							{t('btn.back')}
						</Button>
					</div>
				</form>
			</div>
		</div>
	)
}

// DetailLessonTeacherContent.defaultProps = {}

// DetailLessonTeacherContent.propTypes = {}

export default React.memo(DetailLessonTeacherContent)
