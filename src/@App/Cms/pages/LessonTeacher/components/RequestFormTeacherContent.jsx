/*
 * Created Date: 23-12-2022, 9:27:47 am
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import { questionFormService } from '@App/Cms/services/questionFormService'
import CoreAutocomplete from '@Core/components/Input/CoreAutocomplete'
import CoreInput from '@Core/components/Input/CoreInput'
import { errorMsg, handleErrorFieldBackend, successMsg } from '@Core/helper/Message'
import { getAuthTokenSession } from '@Core/helper/Session'
import Yup from '@Core/helper/Yup'
import { yupResolver } from '@hookform/resolvers/yup'
import { LoadingButton } from '@mui/lab'
import { Button, Typography } from '@mui/material'
import React from 'react'
import { useForm } from 'react-hook-form'
// import PropTypes from 'prop-types'

const RequestFormTeacherContent = props => {
	const { t } = props
	const user = getAuthTokenSession()
	const {
		control,
		getValues,
		watch,
		handleSubmit,
		reset,
		formState: { isSubmitting, isDirty },
		setError
	} = useForm({
		mode: 'onTouched',
		defaultValues: {
			// subject: 'Test question',
			// mail_address: 'phucstt02@gmail.com',
			content: ''
		},
		resolver: yupResolver(
			Yup.object({
				content: Yup.string()
					.trim()
					.max(100, t('common:validation.string.max_content', { value: 100 }))
			})
		)
	})

	const onSubmit = handleSubmit(async data => {
		try {
			await questionFormService.sendQuestion(data, 'teacher')
			reset()
			successMsg(t('common:message.send_request_success'))
		} catch (error) {
			if (typeof error?.response?.data?.message === 'string') {
				errorMsg(error?.response?.data?.message)
			} else {
				handleErrorFieldBackend(error, setError)
			}
		}
	})
	return (
		<div className="w-full flex">
			<div className="w-full sm:w-2/3 lg:w-1/3 m-auto p-10 text-center">
				<Typography className="w-full text-24 mb-20">
					{t('label.name_form')}: {user?.account?.name}
				</Typography>
				<Typography className="w-full text-24 mb-40">
					{t('label.email')}: {user?.account?.mail_address}
				</Typography>
				<Typography className="w-full text-24 mb-20">{t('label.content')}</Typography>
				<form onSubmit={onSubmit}>
					<CoreInput
						control={control}
						name="content"
						size="small"
						className="my-20 w-full"
						multiline
						minRows={10}
						fullWidth
						variant="outlined"
					/>

					<div className="w-full">
						<LoadingButton
							type="submit"
							variant="contained"
							color="primary"
							loading={isSubmitting}
							disabled={!isDirty}
							className="my-20 ml-auto w-3/5 p-12 sm:p-12 lg:p-5 md:w-1/3 lg:w-1/2 text-18"
						>
							{t('btn.submit')}
						</LoadingButton>
					</div>
				</form>
			</div>
		</div>
	)
}

// RequestFormTeacherContent.defaultProps = {}

// RequestFormTeacherContent.propTypes = {}

export default React.memo(RequestFormTeacherContent)
