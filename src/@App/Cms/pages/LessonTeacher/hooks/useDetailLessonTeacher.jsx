/*
 * Created Date: 23-12-2022, 1:59:34 pm
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import { lessonService } from '@App/Cms/services/lessonService'
import { errorMsg } from '@Core/helper/Message'
import { useRequest } from 'ahooks'
import { useEffect } from 'react'
import { useParams } from 'react-router-dom'

export const useDetailLessonTeacher = props => {
	const { id } = useParams()

	const {
		data: lessonTeacher,
		run: fetchLessonTeacher,
		loading: loadingLessonTeacher
	} = useRequest(lessonService.find, {
		manual: true,
		onError: error => errorMsg(error?.response?.data?.message)
	})

	useEffect(() => {
		if (id) {
			fetchLessonTeacher(id)
		}
	}, [])

	return { lessonTeacher, loadingLessonTeacher, fetchLessonTeacher }
}
