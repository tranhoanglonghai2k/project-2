/*
 * Created Date: 23-12-2022, 9:23:59 am
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import AdminContentPage from '@App/Cms/components/Layout/AdminContentPage'
import { TRANSLATE_CMS } from '@App/Cms/configs/constants'
import React from 'react'
import { useTranslation } from 'react-i18next'
import LessonTeacherContent from './components/LessonTeacherContent'
import LessonTeacherProvider from './LessonTeacherProvider'
// import PropTypes from 'prop-types'

const LessonListTeacher = props => {
	const { t } = useTranslation(TRANSLATE_CMS.lesson_teacher)

	return (
		<LessonTeacherProvider>
			<AdminContentPage pageTitle={t('title.reservation_lesson')} content={<LessonTeacherContent t={t} />} />
		</LessonTeacherProvider>
	)
}

// LessonListTeacher.defaultProps = {}

// LessonListTeacher.propTypes = {}

export default React.memo(LessonListTeacher)
