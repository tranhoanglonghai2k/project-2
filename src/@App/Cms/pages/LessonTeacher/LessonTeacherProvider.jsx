/*
 * Created Date: 23-12-2022, 9:24:35 am
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import AdminPageProvider from '@App/Cms/components/Provider/AdminPageProvider'
import { TRANSLATE_CMS } from '@App/Cms/configs/constants'
import { reserveLessonService } from '@App/Cms/services/reserveLessonService'
import { errorMsg } from '@Core/helper/Message'
import { useRequest } from 'ahooks'
import React, { useEffect } from 'react'
import { useTranslation } from 'react-i18next'
// import PropTypes from 'prop-types'

const LessonTeacherProvider = props => {
	const { t } = useTranslation(TRANSLATE_CMS.lesson_teacher)

	const requestReservationLessonTeacher = useRequest(reserveLessonService.list, {
		manual: true,
		onError: error => {
			errorMsg(error?.response?.data?.message)
		}
	})

	const {
		data: lessonListTeacher,
		run: fetchLessonListTeacher,
		loading: loadingLessonListTeacher
	} = requestReservationLessonTeacher

	useEffect(() => {
		fetchLessonListTeacher()
	}, [])

	const data = {
		...props,
		lessonListTeacher,
		loadingLessonListTeacher
	}

	return <AdminPageProvider {...data}>{props.children}</AdminPageProvider>
}

// LessonTeacherProvider.defaultProps = {}

// LessonTeacherProvider.propTypes = {}

export default React.memo(LessonTeacherProvider)
