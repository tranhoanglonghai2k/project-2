/*
 * Created Date: 23-12-2022, 9:21:49 am
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import AdminContentPage from '@App/Cms/components/Layout/AdminContentPage'
import { TRANSLATE_CMS } from '@App/Cms/configs/constants'
import React from 'react'
import { useTranslation } from 'react-i18next'
import RequestFormTeacherContent from './components/RequestFormTeacherContent'
// import PropTypes from 'prop-types'

const RequestFormTeacher = props => {
	const { t } = useTranslation(TRANSLATE_CMS.lesson_teacher)
	return (
		// <LessonListStudentProvider>
		<AdminContentPage pageTitle={t('title.enquiry')} content={<RequestFormTeacherContent t={t} />} />
		// </LessonListStudentProvider>
	)
}

// RequestFormTeacher.defaultProps = {}

// RequestFormTeacher.propTypes = {}

export default React.memo(RequestFormTeacher)
