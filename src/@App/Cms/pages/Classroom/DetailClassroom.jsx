/*
 * Created Date: 16-12-2022, 9:29:50 am
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import AdminContentPage from '@App/Cms/components/Layout/AdminContentPage'
import { TRANSLATE_CMS } from '@App/Cms/configs/constants'
import { Box, CircularProgress } from '@mui/material'
import React from 'react'
import { Helmet } from 'react-helmet'
import { useTranslation } from 'react-i18next'
import { useLocation, useParams } from 'react-router-dom'
import ClassroomProvider from './ClassroomProvider'
import ClassroomForm from './components/ClassroomForm'
import { useDetailClassroom } from './hooks/useDetailClassroom'
// import PropTypes from 'prop-types'

const DetailClassroom = props => {
	const { t } = useTranslation(TRANSLATE_CMS.classroom)
	const { classroom, loadingClassroom, isEdit, userData } = useDetailClassroom()

	return (
		<ClassroomProvider>
			<AdminContentPage
				pageTitle={isEdit ? t('title.edit_page') : t('title.add_page')}
				content={
					loadingClassroom ? (
						<Box className="mt-40 text-center">
							<CircularProgress />
						</Box>
					) : (
						<ClassroomForm classroomData={classroom} isEdit={isEdit} userData={userData} />
					)
				}
			/>
		</ClassroomProvider>
	)
}

// DetailClassroom.defaultProps = {}

// DetailClassroom.propTypes = {}

export default React.memo(DetailClassroom)
