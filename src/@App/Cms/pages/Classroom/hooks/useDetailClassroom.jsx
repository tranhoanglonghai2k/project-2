/*
 * Created Date: 22-12-2022, 12:02:51 am
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import { classroomService } from '@App/Cms/services/classroomService'
import { studentService } from '@App/Cms/services/studentService'
import { errorMsg } from '@Core/helper/Message'
import { useRequest } from 'ahooks'
import { useEffect } from 'react'
import { useParams } from 'react-router-dom'

export const useDetailClassroom = props => {
	const { id } = useParams()
	const isEdit = id !== 'new'

	const {
		data: classroom,
		run: fetchClassroom,
		loading: loadingClassroom
	} = useRequest(classroomService.find, {
		manual: true,
		onError: error => errorMsg(error?.response?.data?.message)
	})

	useEffect(() => {
		if (isEdit) {
			fetchClassroom(id)
		}
	}, [])

	return { classroom, loadingClassroom, isEdit }
}
