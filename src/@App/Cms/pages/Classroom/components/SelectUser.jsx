/*
 * Created Date: 22-12-2022, 8:59:40 am
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import { studentService } from '@App/Cms/services/studentService'
import CoreAutocomplete from '@Core/components/Input/CoreAutocomplete'
import { errorMsg } from '@Core/helper/Message'
import React, { useCallback } from 'react'
// import PropTypes from 'prop-types'

const SelectUser = props => {
	const { control, name, className, options, valuePath, labelPath, ...restProps } = props

	return (
		<CoreAutocomplete
			control={control}
			name={name}
			className={className}
			valuePath={valuePath}
			labelPath={labelPath}
			options={options}
			{...restProps}
		/>
	)
}

// SelectUser.defaultProps = {}

// SelectUser.propTypes = {}

export default React.memo(SelectUser)
