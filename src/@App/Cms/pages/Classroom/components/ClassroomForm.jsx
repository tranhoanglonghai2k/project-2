/*
 * Created Date: 16-12-2022, 9:31:04 am
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import AdminInput from '@App/Cms/components/Input/AdminInput'
import { useAdminPageContext } from '@App/Cms/components/Provider/AdminPageProvider'
import { ROUTER_EXE, TRANSLATE_CMS } from '@App/Cms/configs/constants'
import { classroomService } from '@App/Cms/services/classroomService'
import CoreAutocomplete from '@Core/components/Input/CoreAutocomplete'
import { errorMsg, handleErrorFieldBackend, successMsg } from '@Core/helper/Message'
import Yup from '@Core/helper/Yup'
import { yupResolver } from '@hookform/resolvers/yup'
import { LoadingButton } from '@mui/lab'
import { Box, Typography } from '@mui/material'
import React from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { useNavigate } from 'react-router-dom'
import SelectUser from './SelectUser'
// import PropTypes from 'prop-types'

const ClassroomForm = props => {
	const navigate = useNavigate()
	const { t } = useTranslation(TRANSLATE_CMS.classroom)
	const { userData } = useAdminPageContext()
	const { classroomData, isEdit } = props

	const {
		control,
		handleSubmit,
		watch,
		formState: { isSubmitting },
		setError
	} = useForm({
		mode: 'onTouched',
		defaultValues: {
			id: classroomData?.id ?? null,
			user_id: classroomData?.user_id ?? '',
			attending_time: classroomData?.attending_time ?? '',
			remarks: classroomData?.remarks ?? '',
			type: classroomData?.type ?? ''
		},
		resolver: yupResolver(
			Yup.object({
				user_id: Yup.mixed().nullable().required(t('common:validation.required')),
				attending_time: Yup.string().required(t('common:validation.required')),
				remarks: Yup.string().trim().required(t('common:validation.required'))
			})
		)
	})

	const onSubmit = handleSubmit(async data => {
		try {
			await classroomService.save(data)
			navigate(ROUTER_EXE.classroom.list)
			successMsg(isEdit ? t('common:message.edit_success') : t('common:message.create_success'))
		} catch (error) {
			if (typeof error?.response?.data?.message === 'string') {
				errorMsg(error?.response?.data?.message)
			} else {
				handleErrorFieldBackend(error, setError)
			}
		}
	})

	return (
		<form onSubmit={onSubmit}>
			<Box className="max-w-lg sm:mt-40 mt-20 mx-auto sm:p-12">
				<Box className="flex flex-wrap sm:flex-nowrap mb-16 sm:mb-20 p-12">
					<Box className="w-full sm:w-1/3 mt-12 mb-8 sm:mb-0">
						<Typography variant="h3" className="flex items-center whitespace-normal mb-4">
							{t('label.student')}
						</Typography>
					</Box>

					<Box className="rounded-md flex w-full sm:w-2/3">
						{isEdit ? (
							<Typography variant="h3" className="flex items-center whitespace-normal my-auto">
								{classroomData?.user?.name}
							</Typography>
						) : (
							<SelectUser
								control={control}
								name="user_id"
								className="w-full"
								placeholder=""
								size="small"
								valuePath="id"
								labelPath="name"
								returnValueType="enum"
								options={userData?.data}
							/>
						)}
					</Box>
				</Box>

				<AdminInput
					label={t('label.attendance')}
					control={control}
					name="attending_time"
					className="mb-16 sm:mb-20 p-12"
					placeholder=""
					size="small"
				/>

				<AdminInput
					label={t('label.note')}
					control={control}
					name="remarks"
					className="mb-16 sm:mb-20 p-12"
					placeholder=""
					size="small"
					// required
					multiline
					minRows={5}
				/>

				<Box className="flex flex-wrap sm:flex-nowrap mb-16 sm:mb-20 p-12">
					<Box className="w-full sm:w-1/3 mt-12 mb-8 sm:mb-0">
						<Typography variant="h3" className="flex items-center whitespace-normal mb-4">
							{t('label.type')}
						</Typography>
					</Box>

					<Box className="rounded-md flex w-full sm:w-2/3">
						<CoreAutocomplete
							control={control}
							name="type"
							className="w-full"
							placeholder=""
							size="small"
							valuePath="value"
							labelPath="label"
							returnValueType="enum"
							options={[
								{ value: 1, label: '生徒利用' },
								{ value: 2, label: '事務局登録' }
							]}
						/>
					</Box>
				</Box>

				<Box className="flex flex-wrap sm:flex-nowrap mb-16 sm:mb-20">
					<Box className="w-full lg:w-1/3 sm:w-2/5 m-12 mx-auto px-20 sm:p-0">
						<LoadingButton
							type="submit"
							loading={isSubmitting}
							variant="contained"
							color="primary"
							size="small"
							className="w-full text-16"
						>
							{isEdit ? t('common:btn.update') : t('common:btn.sign_up')}
						</LoadingButton>
					</Box>
				</Box>
			</Box>
		</form>
	)
}

// ClassroomForm.defaultProps = {}

// ClassroomForm.propTypes = {}

export default React.memo(ClassroomForm)
