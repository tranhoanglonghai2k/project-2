/*
 * Created Date: 16-12-2022, 9:30:19 am
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import { useAdminPageContext } from '@App/Cms/components/Provider/AdminPageProvider'
import { ROUTER_EXE, TRANSLATE_CMS } from '@App/Cms/configs/constants'
import { CoreActionDelete, CoreActionEdit } from '@Core/components/Table/components/CoreTableAction'
import CoreTable, { columnHelper } from '@Core/components/Table/CoreTable'
import { Box } from '@mui/material'
import React, { useMemo } from 'react'
import { useTranslation } from 'react-i18next'
import { useNavigate } from 'react-router-dom'
import { useDataFake } from '../../Lesson/hooks/useDataFake'
import ClassroomTableFilter from './ClassroomTableFilter'
// import PropTypes from 'prop-types'

const ClassroomTable = props => {
	const { t } = useTranslation(TRANSLATE_CMS.classroom)
	const navigate = useNavigate()
	const { classroomTableHandler, handleDeleteClassroom } = useAdminPageContext()

	const columns = useMemo(() => {
		return [
			columnHelper.accessor('id', {
				cell: info => info.getValue(),
				header: 'ID',
				className: 'w-[10%]'
			}),
			columnHelper.accessor('user_name', {
				header: t('label.login_id')
			}),
			columnHelper.accessor('admin_name', {
				header: t('label.updater_name'),
				cell: ({ row }) => {
					return row?.original?.admin_name ? row?.original?.admin_name : '---'
				}
			}),
			columnHelper.accessor('type_string', {
				header: t('label.kind')
			}),
			columnHelper.accessor('attending_time', {
				header: t('label.attendance')
			}),
			columnHelper.accessor('action', {
				header: t('label.action'),
				className: 'w-[15%]',
				cell: ({ row }) => {
					const data = row.original
					return (
						<div className="flex">
							<CoreActionEdit
								onClick={() =>
									navigate(ROUTER_EXE.classroom.list + `/edit/${data?.id}`, { state: data })
								}
							/>
							<CoreActionDelete onConfirmDelete={() => handleDeleteClassroom(data?.id)} />
						</div>
					)
				}
			})
		]
	}, [t])

	return (
		<Box className="m-12">
			<ClassroomTableFilter className="mb-20" />
			<CoreTable isShowPagination columns={columns} {...classroomTableHandler} />
		</Box>
	)
}

// ClassroomTable.defaultProps = {}

// ClassroomTable.propTypes = {}

export default React.memo(ClassroomTable)
