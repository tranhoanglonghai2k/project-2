/*
 * Created Date: 28-12-2022, 3:58:36 pm
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import { executiveAccountService } from '@App/Cms/services/executiveAccountService'
import CoreAutocomplete from '@Core/components/Input/CoreAutocomplete'
import { useRequest } from 'ahooks'
import React, { useEffect } from 'react'
// import PropTypes from 'prop-types'

const SelectAdmin = props => {
	const { control, name, className, ...restProps } = props

	const { data: admin, run: fetchAdmin } = useRequest(executiveAccountService.list, {
		manual: true
	})

	useEffect(() => {
		fetchAdmin({ per_page: 1000 })
	}, [])

	return <CoreAutocomplete control={control} name={name} className={className} options={admin?.data} {...restProps} />
}

// SelectAdmin.defaultProps = {}

// SelectAdmin.propTypes = {}

export default React.memo(SelectAdmin)
