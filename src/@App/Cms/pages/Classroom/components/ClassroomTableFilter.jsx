/*
 * Created Date: 16-12-2022, 9:30:35 am
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import { useAdminPageContext } from '@App/Cms/components/Provider/AdminPageProvider'
import { TRANSLATE_CMS } from '@App/Cms/configs/constants'
import CoreAutocomplete from '@Core/components/Input/CoreAutocomplete'
import CoreInput from '@Core/components/Input/CoreInput'
import { Box, Button } from '@mui/material'
import clsx from 'clsx'
import React from 'react'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import SelectAdmin from './Select/SelectAdmin'
// import PropTypes from 'prop-types'

const ClassroomTableFilter = props => {
	const { className } = props
	const { classroomTableHandler, userData } = useAdminPageContext()
	const { t } = useTranslation(TRANSLATE_CMS.classroom)
	const handleFilter = () => {
		const data = getValues()

		const params = {
			...data,
			page: 1
		}

		classroomTableHandler.handleFetchData(params)
	}

	const { control, getValues, watch } = useForm({
		mode: 'onTouched',
		defaultValues: {
			user_id: null,
			updated_id: '',
			type: null
		}
	})

	return (
		<Box className={clsx('border-1 rounded-4 border-grey-300', className)}>
			<Box className="p-8 w-full lg:flex lg:flex-wrap">
				<Box className="flex w-full lg:w-1/2 mb-20 lg:mb-10 items-start">
					<Box className="w-2/5 lg:w-1/4 px-20 text-base h-full my-auto text-left lg:text-right">
						{t('label.login_id')}
					</Box>
					<CoreAutocomplete
						control={control}
						name="user_id"
						size="small"
						className="lg:w-3/4 w-3/5"
						fullWidth
						valuePath="id"
						labelPath="name"
						returnValueType="enum"
						variant="outlined"
						options={userData?.data}
					/>
				</Box>

				<Box className="flex w-full lg:w-1/2 mb-20 lg:mb-10 items-start">
					<Box className="w-2/5 lg:w-1/4 px-20 text-base h-full my-auto text-left lg:text-right">
						{t('label.updater_name_filter')}
					</Box>
					<SelectAdmin
						control={control}
						name="updated_id"
						size="small"
						className="lg:w-3/4 w-3/5"
						fullWidth
						variant="outlined"
						valuePath="id"
						labelPath="name"
						returnValueType="enum"
					/>
				</Box>

				<Box className="flex w-full lg:w-1/2 mb-20 lg:mb-10 items-start">
					<Box className="w-2/5 lg:w-1/4 px-20 text-base h-full my-auto text-left lg:text-right">
						{t('label.kind')}
					</Box>
					<CoreAutocomplete
						control={control}
						name="type"
						size="small"
						className="lg:w-3/4 w-3/5"
						variant="outlined"
						valuePath="value"
						labelPath="label"
						returnValueType="enum"
						options={[
							{ value: 1, label: '生徒利用' },
							{ value: 2, label: '事務局登録' }
						]}
					/>
				</Box>
			</Box>

			<Box className="flex p-8 w-full">
				<Button
					variant="contained"
					color="primary"
					className="ml-auto w-2/5 p-12 sm:p-8 lg:p-5 lg:w-160 text-18"
					onClick={handleFilter}
				>
					{t('common:btn.search')}
				</Button>
			</Box>
		</Box>
	)
}

// ClassroomTableFilter.defaultProps = {}

// ClassroomTableFilter.propTypes = {}

export default React.memo(ClassroomTableFilter)
