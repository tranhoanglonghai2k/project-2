/*
 * Created Date: 16-12-2022, 9:26:43 am
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import AdminContentPage from '@App/Cms/components/Layout/AdminContentPage'
import { ROUTER_EXE, TRANSLATE_CMS } from '@App/Cms/configs/constants'
import { Box, Button } from '@mui/material'
import React from 'react'
import { Helmet } from 'react-helmet'
import { useTranslation } from 'react-i18next'
import { useNavigate } from 'react-router-dom'
import ClassroomProvider from './ClassroomProvider'
import ClassroomTable from './components/ClassroomTable'
// import PropTypes from 'prop-types'

const ListClassroom = props => {
	const { t } = useTranslation(TRANSLATE_CMS.classroom)
	const navigate = useNavigate()

	return (
		<ClassroomProvider>
			
			<AdminContentPage
				pageTitle={t('title.title_page')}
				content={
					<>
						<ClassroomTable />
						<Box className="text-right my-40 mr-40">
							<Button
								variant="contained"
								color="primary"
								onClick={() => navigate(ROUTER_EXE.classroom.list + '/edit/new')}
								className="px-20 w-160 text-18"
							>
								{t('common:btn.add')}
							</Button>
						</Box>
					</>
				}
			/>
		</ClassroomProvider>
	)
}

// ListClassroom.defaultProps = {}

// ListClassroom.propTypes = {}

export default React.memo(ListClassroom)
