/*
 * Created Date: 16-12-2022, 9:26:19 am
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import AdminPageProvider from '@App/Cms/components/Provider/AdminPageProvider'
import { TRANSLATE_CMS } from '@App/Cms/configs/constants'
import { classroomService } from '@App/Cms/services/classroomService'
import { studentService } from '@App/Cms/services/studentService'
import useCoreTable from '@Core/components/Table/hooks/useCoreTable'
import { errorMsg, successMsg } from '@Core/helper/Message'
import { useRequest } from 'ahooks'
import React, { useEffect, useState } from 'react'
import { useTranslation } from 'react-i18next'
// import PropTypes from 'prop-types'

const ClassroomProvider = props => {
	const { t } = useTranslation(TRANSLATE_CMS.classroom)

	const requestClassrooms = useRequest(classroomService.list, {
		manual: true,
		onError: error => errorMsg(error?.response?.data?.message)
	})

	const { data: userData, run: fetchUsers } = useRequest(studentService.list, {
		manual: true,
		onError: error => errorMsg(error?.response?.data?.message)
	})

	const { runAsync: handleDeleteClassroom } = useRequest(classroomService.delete, {
		manual: true,
		onSuccess: res => {
			classroomTableHandler.handleFetchData()
			successMsg(t('common:message.delete_success'))
		},
		onError: error => {
			errorMsg(error?.response?.data?.message)
		}
	})

	const classroomTableHandler = useCoreTable(requestClassrooms)

	useEffect(() => {
		classroomTableHandler.handleFetchData()
		fetchUsers({ per_page: 1000 })
	}, [])

	const data = {
		...props,
		classroomTableHandler,
		handleDeleteClassroom,
		userData
	}

	return <AdminPageProvider {...data}>{props.children}</AdminPageProvider>
}

// ClassroomProvider.defaultProps = {}

// ClassroomProvider.propTypes = {}

export default React.memo(ClassroomProvider)
