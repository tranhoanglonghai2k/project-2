/*
 * Created Date: 23-12-2022, 11:27:24 am
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import { lessonService } from '@App/Cms/services/lessonService'
import { errorMsg } from '@Core/helper/Message'
import { useRequest } from 'ahooks'
import { useEffect } from 'react'
import { useParams } from 'react-router-dom'

export const useDetailLessonListStudent = props => {
	const { id } = useParams()

	const {
		data: lessonStudent,
		run: fetchLessonStudent,
		loading: loadingLessonStudent
	} = useRequest(lessonService.find, {
		manual: true,
		onError: error => errorMsg(error?.response?.data?.message)
	})

	useEffect(() => {
		if (id) {
			fetchLessonStudent(id)
		}
	}, [])

	return { lessonStudent, loadingLessonStudent, fetchLessonStudent }
}
