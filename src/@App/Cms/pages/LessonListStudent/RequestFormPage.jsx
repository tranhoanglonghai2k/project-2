/*
 * Created Date: 18-12-2022, 12:49:14 am
 * Author: DESKTOP-SRKKG3M\Hello Keys
 * Email: you@you.you
 * -----
 * Last Modified: Wed Jan 04 2023
 * Modified By: haitran
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import AdminContentPage from '@App/Cms/components/Layout/AdminContentPage'
import { TRANSLATE_CMS } from '@App/Cms/configs/constants'
import React from 'react'
import { useTranslation } from 'react-i18next'
import LessonListStudentProvider from './LessonListStudentProvider'
import RequestFormContent from './components/RequestFormContent'
// import PropTypes from 'prop-types'

const RequestFormPage = props => {
	const { t } = useTranslation(TRANSLATE_CMS.lesson_list_student)
	return (
		// <LessonListStudentProvider>
		<AdminContentPage pageTitle={t('title.enquiry')} content={<RequestFormContent t={t} />} />
		// </LessonListStudentProvider>
	)
}

// RequestFormPage.defaultProps = {}

// RequestFormPage.propTypes = {}

export default React.memo(RequestFormPage)
