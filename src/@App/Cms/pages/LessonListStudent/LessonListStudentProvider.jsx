/*
 * Created Date: 16-12-2022, 11:12:15 pm
 * Author: DESKTOP-SRKKG3M\Hello Keys
 * Email: you@you.you
 * -----
 * Last Modified: Thu Jan 05 2023
 * Modified By: haitran
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import AdminPageProvider from '@App/Cms/components/Provider/AdminPageProvider'
import { TRANSLATE_CMS } from '@App/Cms/configs/constants'
import { lessonListStudent } from '@App/Cms/services/lessonListStudentService.js'
import { reserveLessonService } from '@App/Cms/services/reserveLessonService'
import { useRequest } from 'ahooks'
import React, { useEffect } from 'react'
import { useTranslation } from 'react-i18next'
// import PropTypes from 'prop-types'

const SettingProvider = props => {
	const { t } = useTranslation(TRANSLATE_CMS.lesson_list_student)

	const requestSetting = useRequest(reserveLessonService.list, {
		manual: true,
		onError: error => {
			errorMsg(error?.response?.data?.message)
		}
	})

	const { data: lessonList, run: fetchLessonList, loading: loadingLessonList } = requestSetting

	useEffect(() => {
		// accountTableHandler.handleFetchData()
		fetchLessonList({ per_page: 1000 })
	}, [])

	const data = {
		...props,
		lessonList,
		loadingLessonList
	}

	return <AdminPageProvider {...data}>{props.children}</AdminPageProvider>
}

// SettingProvider.defaultProps = {}

// SettingProvider.propTypes = {}

export default React.memo(SettingProvider)
