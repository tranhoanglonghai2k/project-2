/*
 * Created Date: 16-12-2022, 11:31:47 pm
 * Author: DESKTOP-SRKKG3M\Hello Keys
 * Email: you@you.you
 * -----
 * Last Modified: Tue Jan 10 2023
 * Modified By: haitran
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import { ROUTER_STUDENT } from '@App/Cms/configs/constants'
import React from 'react'
import { Typography, Button, CircularProgress } from '@mui/material'
import { useNavigate } from 'react-router-dom'
import { useAdminPageContext } from '@App/Cms/components/Provider/AdminPageProvider'
import moment from 'moment'
import { getAuthTokenSession } from '@Core/helper/Session'

// import PropTypes from 'prop-types'

const LessonListContent = props => {
	const { t } = props
	const navigate = useNavigate()
	const { lessonList, loadingLessonList } = useAdminPageContext()
	const authToken = getAuthTokenSession()

	return loadingLessonList ? (
		<div className="mt-40 text-center">
			<CircularProgress />
		</div>
	) : (
		<div className="w-full flex">
			<div className="w-full lg:w-1/3 md:w-1/2 sm:w-4/5 p-10 m-auto">
				<div className="w-full flex flex-col justify-center mt-40">
					<Typography className="mx-auto text-24 font-bold mb-20">{t('title.title_page')}</Typography>
					{lessonList?.data?.map((item, index) => {
						return (
							<div
								key={item?.id}
								onClick={() => {
									navigate(ROUTER_STUDENT.lesson_list_student + '/detail/' + item?.lesson_id, {
										state: { is_reserved_lesson: true }
									})
								}}
								className="p-48 my-20 border w-full flex border-grey-300 rounded-12 cursor-pointer"
							>
								<div className="w-7/10 mx-auto">
									<Typography className="w-full text-24 mb-10 text-left">
										{t('label.lesson_name')}: {item?.lesson_name}
									</Typography>
									<Typography className="w-full text-24">
										{t('label.date_time')}:{' '}
										{authToken?.account?.lang?.code?.toLowerCase() === 'en'
											? moment(item?.lesson_event_date).format('YYYY/MM/DD')
											: moment(item?.lesson_event_date).format('YYYY年MM月DD日')}{' '}
										{item?.lesson_start_time}
									</Typography>
								</div>
							</div>
						)
					})}
				</div>
			</div>
		</div>
	)
}

//LessonListContent.defaultProps = {}

//LessonListContent.propTypes = {}

export default React.memo(LessonListContent)
