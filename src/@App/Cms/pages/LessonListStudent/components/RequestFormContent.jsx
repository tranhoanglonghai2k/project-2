/*
 * Created Date: 18-12-2022, 12:51:58 am
 * Author: DESKTOP-SRKKG3M\Hello Keys
 * Email: you@you.you
 * -----
 * Last Modified: Thu Jan 05 2023
 * Modified By: haitran
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */
import CoreInput from '@Core/components/Input/CoreInput'
import React from 'react'
import { useForm } from 'react-hook-form'
import { Typography, Button } from '@mui/material'
import { useNavigate } from 'react-router-dom'
import { errorMsg, handleErrorFieldBackend, successMsg } from '@Core/helper/Message'
import { questionFormService } from '@App/Cms/services/questionFormService'
import { getAuthTokenSession } from '@Core/helper/Session'
import { yupResolver } from '@hookform/resolvers/yup'
import { LoadingButton } from '@mui/lab'
import Yup from '@Core/helper/Yup'
// import PropTypes from 'prop-types'

const RequestFormContent = props => {
	const { t } = props
	const user = getAuthTokenSession()
	const {
		control,
		handleSubmit,
		reset,
		setError,
		formState: { isSubmitting, isDirty }
	} = useForm({
		mode: 'onTouched',
		defaultValues: {
			// subject: 'Test question',
			// mail_address: 'phucstt02@gmail.com',
			content: ''
		},
		resolver: yupResolver(
			Yup.object({
				content: Yup.string()
					.trim()
					.max(100, t('common:validation.string.max_content', { value: 100 }))
			})
		)
	})

	const onSubmit = handleSubmit(async data => {
		try {
			await questionFormService.sendQuestion(data, 'user')
			reset()
			successMsg(t('common:message.send_request_success'))
		} catch (error) {
			if (typeof error?.response?.data?.message === 'string') {
				errorMsg(error?.response?.data?.message)
			} else {
				handleErrorFieldBackend(error, setError)
			}
		}
	})
	return (
		<div className="w-full flex">
			<div className="w-full sm:w-2/3 lg:w-1/3 m-auto p-10 text-center">
				<Typography className="w-full text-24 mb-20">
					{t('label.name_form')}: {user?.account?.name}
				</Typography>
				<Typography className="w-full text-24 mb-40">
					{t('label.email')}: {user?.account?.mail_address}
				</Typography>
				<Typography className="w-full text-24 mb-20">{t('label.content')}</Typography>
				<form onSubmit={onSubmit}>
					<CoreInput
						control={control}
						name="content"
						size="small"
						className="my-20"
						multiline
						minRows={10}
						// fullWidth
						variant="outlined"
					/>
					<LoadingButton
						type="submit"
						variant="contained"
						color="primary"
						loading={isSubmitting}
						disabled={!isDirty}
						className="my-20 ml-auto w-3/5 p-12 sm:p-12 lg:p-5 md:w-1/3 lg:w-1/2 text-18"
					>
						{t('btn.submit')}
					</LoadingButton>
				</form>
			</div>
		</div>
	)
}

//RequestFormContent.defaultProps = {}

//RequestFormContent.propTypes = {}

export default React.memo(RequestFormContent)
