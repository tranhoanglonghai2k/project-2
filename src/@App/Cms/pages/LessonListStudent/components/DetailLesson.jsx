/*
 * Created Date: 17-12-2022, 11:35:13 pm
 * Author: DESKTOP-SRKKG3M\Hello Keys
 * Email: you@you.you
 * -----
 * Last Modified: Tue Jan 10 2023
 * Modified By: haitran
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import { ROUTER_STUDENT } from '@App/Cms/configs/constants'
import React from 'react'
import { Typography, Button } from '@mui/material'
import { useLocation, useNavigate } from 'react-router-dom'
import moment from 'moment'
import { errorMsg, successMsg } from '@Core/helper/Message'
import { lessonService } from '@App/Cms/services/lessonService'
import { getAuthTokenSession } from '@Core/helper/Session'
// import PropTypes from 'prop-types'

const DetailLesson = props => {
	const { t, lessonStudentData, fetchLessonStudent } = props
	const navigate = useNavigate()
	const location = useLocation()
	const authToken = getAuthTokenSession()

	const handleSubmit = async lessonId => {
		try {
			await lessonService.submitLesson({ lesson_id: lessonId })
			fetchLessonStudent(lessonId)
			successMsg(t('message.booking_success'))
		} catch (error) {
			errorMsg(error?.response?.data?.message)
		}
	}

	return (
		<div className="w-full flex">
			<div className="w-full sm:w-2/3 sm:p-0 p-8 mx-auto mt-40 text-center">
				<Typography className="w-full text-24 mb-20">
					{t('label.lesson_name')}: {lessonStudentData?.name}
				</Typography>
				<Typography className="w-full text-24 mb-20">
					{t('label.date_time')}:{' '}
					{authToken?.account?.lang?.code?.toLowerCase() === 'en'
						? moment(lessonStudentData?.event_date).format('YYYY/MM/DD')
						: moment(lessonStudentData?.event_date).format('YYYY年MM月DD日')}{' '}
					({lessonStudentData?.start_time} ~ {lessonStudentData?.end_time})
				</Typography>
				<Typography className="w-full text-24">
					{t('label.zoom_url')}:
					<a href={lessonStudentData?.zoom_url} target="_blank">
						{lessonStudentData?.zoom_url}
					</a>
				</Typography>
				<Typography className="mx-auto text-24 my-40">{t('label.detail')}</Typography>
				<div>
					<Typography className="w-full text-24 mb-40">{lessonStudentData?.content}</Typography>

					{lessonStudentData?.is_booking === 1 ? (
						<div>
							<Button
								variant="contained"
								className="p-12 sm:p-8 lg:p-5 text-18 w-full lg:w-1/4 sm:w-1/2 my-20"
								onClick={() => handleSubmit(lessonStudentData?.id)}
							>
								{t('btn.storage')}
							</Button>
						</div>
					) : null}
					<div>
						<Button
							variant="contained"
							className="p-12 sm:p-8 lg:p-5 w-full lg:w-1/4 sm:w-1/2 text-18 my-20"
							onClick={() => {
								if (location?.state?.is_reserved_lesson) {
									navigate(ROUTER_STUDENT.lesson_list_student)
								} else {
									navigate(ROUTER_STUDENT.schedule_calendar)
								}
							}}
						>
							{t('btn.back')}
						</Button>
					</div>
				</div>
			</div>
		</div>
	)
}

//DetailLesson.defaultProps = {}

//DetailLesson.propTypes = {}

export default React.memo(DetailLesson)
