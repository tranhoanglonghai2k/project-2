/*
 * Created Date: 16-12-2022, 10:40:10 pm
 * Author: DESKTOP-SRKKG3M\Hello Keys
 * Email: you@you.you
 * -----
 * Last Modified: Sat Dec 17 2022
 * Modified By: Hello Keys
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import AdminContentPage from '@App/Cms/components/Layout/AdminContentPage'
import { TRANSLATE_CMS } from '@App/Cms/configs/constants'
import React from 'react'
import { useTranslation } from 'react-i18next'
import LessonListStudentProvider from './LessonListStudentProvider'
import LessonListContent from './components/LessonListContent'
// import PropTypes from 'prop-types'

const Setting = props => {
	const { t } = useTranslation(TRANSLATE_CMS.lesson_list_student)

	return (
		<LessonListStudentProvider>
			<AdminContentPage pageTitle={t('title.title_page')} content={<LessonListContent t={t}/>} />
		</LessonListStudentProvider>
	)
}

// Setting.defaultProps = {}

// Setting.propTypes = {}

export default React.memo(Setting)