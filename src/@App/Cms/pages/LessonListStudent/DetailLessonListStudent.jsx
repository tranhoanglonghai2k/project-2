/*
 * Created Date: 17-12-2022, 12:00:49 am
 * Author: DESKTOP-SRKKG3M\Hello Keys
 * Email: you@you.you
 * -----
 * Last Modified: Fri Dec 30 2022
 * Modified By: haitran
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import AdminContentPage from '@App/Cms/components/Layout/AdminContentPage'
import { TRANSLATE_CMS } from '@App/Cms/configs/constants'
import React from 'react'
import { useTranslation } from 'react-i18next'
import LessonListStudentProvider from './LessonListStudentProvider'
import DetailLesson from './components/DetailLesson'
import { useDetailLessonListStudent } from './hooks/useDetailLessonListStudent'
import { CircularProgress } from '@mui/material'
// import PropTypes from 'prop-types'

const DetailLessonListStudent = props => {
	const { t } = useTranslation(TRANSLATE_CMS.lesson_list_student)
	const { lessonStudent, loadingLessonStudent, fetchLessonStudent } = useDetailLessonListStudent()
	return (
		// <LessonListStudentProvider>
		<AdminContentPage
			pageTitle={t('title.title_page')}
			content={
				loadingLessonStudent ? (
					<div className="mt-40 text-center">
						<CircularProgress />
					</div>
				) : (
					<DetailLesson t={t} lessonStudentData={lessonStudent} fetchLessonStudent={fetchLessonStudent} />
				)
			}
		/>
		// </LessonListStudentProvider>
	)
}

//DetailLessonListStudent.defaultProps = {}

//DetailLessonListStudent.propTypes = {}

export default React.memo(DetailLessonListStudent)
