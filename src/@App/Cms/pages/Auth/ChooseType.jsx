/*
 * Created Date: 12-12-2022, 5:27:29 pm
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import { ROUTER_CMS, ROUTER_STUDENT } from '@App/Cms/configs/constants'
import { Button } from '@mui/material'
import React from 'react'
import { Helmet } from 'react-helmet'
import { useTranslation } from 'react-i18next'
import { useNavigate } from 'react-router-dom'
// import PropTypes from 'prop-types'

const ChooseType = props => {
	const navigate = useNavigate()
	const { t } = useTranslation('common')

	return (
		<div className="w-full mt-160 flex">
			<Helmet>
				<title>{t('title.choose_type')}</title>
			</Helmet>
			<div className="m-auto">
				<Button
					variant="outlined"
					className="w-200 p-20  block my-20"
					onClick={() => navigate(ROUTER_CMS.auth.login + '/user')}
				>
					{t('btn.choose_type.student')}
				</Button>
				<Button
					variant="outlined"
					className="w-200 p-20 block my-20"
					onClick={() => navigate(ROUTER_CMS.auth.login + '/teacher')}
				>
					{t('btn.choose_type.teacher')}
				</Button>
				<Button
					variant="outlined"
					className="w-200 p-20 block my-20"
					onClick={() => navigate(ROUTER_CMS.auth.login + '/admin')}
				>
					{t('btn.choose_type.ext')}
				</Button>
			</div>
		</div>
	)
}

// ChooseType.defaultProps = {}

// ChooseType.propTypes = {}

export default React.memo(ChooseType)
