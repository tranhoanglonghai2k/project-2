import Cookies from 'js-cookie'
import Yup from '@Core/helper/Yup'
import { useRequest } from 'ahooks'
import React, { useEffect } from 'react'
import { LoadingButton } from '@mui/lab'
import { useForm } from 'react-hook-form'
import { Box, Paper, Typography } from '@mui/material'
import { yupResolver } from '@hookform/resolvers/yup'
import { useNavigate, useParams } from 'react-router-dom'
import i18n from 'i18next'
import CoreInput from '@Core/components/Input/CoreInput'
import { ROUTER_CMS, TRANSLATE_CMS } from '@App/Cms/configs/constants'
import { authService } from '@App/Cms/services/authService'
import { errorMsg, handleErrorFieldBackend, successMsg } from '@Core/helper/Message'
import { useTranslation } from 'react-i18next'
import { refreshPage, setAuthTokenSession, setLangSession } from '@Core/helper/Session'
import { Helmet } from 'react-helmet'
import { settingService } from '@App/Cms/services/settingService'

const Login = () => {
	const navigate = useNavigate()
	const { type } = useParams()
	const { t } = useTranslation(TRANSLATE_CMS.login)

	// set XSRF-TOKEN to cookies
	// const {
	// 	data: csrfData,
	// 	run: getCSRFData,
	// 	loading: loadingCSRFData
	// } = useRequest(authService.csrf, {
	// 	manual: true
	// })

	// useEffect(() => {
	// 	getCSRFData()
	// }, [])

	const {
		control,
		handleSubmit,
		formState: { isSubmitting },
		setError
	} = useForm({
		mode: 'onTouched',
		defaultValues: {
			login_id: '',
			password: ''
		},
		resolver: yupResolver(
			Yup.object({
				login_id: Yup.string().required(t('common:validation.required')),
				password: Yup.string().required(t('common:validation.required'))
			})
		)
	})

	const onSubmit = handleSubmit(async data => {
		try {
			const res = await authService.login(data, type)
			setAuthTokenSession(res)

			if (type === 'user' && res?.account?.lang) {
				i18n.changeLanguage(res?.account?.lang?.code?.toLowerCase())
			} else {
				i18n.changeLanguage('ja')
			}
			setLangSession({
				lang: res?.account?.lang ?? { code: 'JA' },
				typeAccount: type
			})

			if (res?.is_setting === 0 && type !== 'admin') {
				navigate(`/${type}/setting`, { state: { isLoaded: true } })
			} else {
				navigate(
					type === 'user'
						? '/user/schedule-calendar'
						: type === 'teacher'
						? '/teacher/schedule-calendar'
						: '/admin/lesson',
					{ state: { isLoaded: true } }
				)
				successMsg(t('message.login_success'))
			}
		} catch (error) {
			if (typeof error?.response?.data?.message === 'string') {
				errorMsg(error?.response?.data?.message)
			} else {
				handleErrorFieldBackend(error, setError)
			}
		}
	})

	const renderFormLogin = () => {
		return (
			<form onSubmit={onSubmit} className="text-center block px-10 pt-10">
				<CoreInput
					className="py-10 flex-col"
					control={control}
					name="login_id"
					label={t('label.login_id')}
					labelStyle="mb-8"
					required
					showTextRequired={false}
					// placeholder="メールアドレスを入力してください"
					placeholder=""
				/>
				<CoreInput
					className="py-10 flex-col"
					type="password"
					control={control}
					name="password"
					label={t('label.password')}
					labelStyle="mb-8"
					required
					showTextRequired={false}
					// placeholder="パスワードを入力してください"
					placeholder=""
				/>

				<LoadingButton
					loading={isSubmitting}
					variant="contained"
					className="text-18 py-8 px-20 w-full rounded-4 font-bold mt-20"
					type="submit"
				>
					{t('btn.login')}
				</LoadingButton>
			</form>
		)
	}
	return (
		<div className="text-center w-full pt-40">
			<div className="sm:flex w-full">
				<Helmet>
					<title>
						{type === 'user'
							? t('common:btn.choose_type.student')
							: type === 'teacher'
							? t('common:btn.choose_type.teacher')
							: type === 'admin'
							? t('common:btn.choose_type.ext')
							: null}{' '}
						{t('btn.login')}
					</title>
				</Helmet>
				<Box className="w-full lg:w-1/3 sm:w-1/2 p-24 m-auto shadow-none">
					{renderFormLogin()}
					<Typography
						onClick={() => navigate(ROUTER_CMS.auth.forgot_password.forgot + `/${type}`)}
						className="w-full mt-40 text-base underline cursor-pointer hover:text-primary"
					>
						{t('label.forgot_password')}
					</Typography>
				</Box>
			</div>
		</div>
	)
}

export default Login
