/*
 * Created Date: 12-12-2022, 9:23:26 pm
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import { ROUTER_CMS, TRANSLATE_CMS } from '@App/Cms/configs/constants'
import { forgotPasswordService } from '@App/Cms/services/forgotPasswordService'
import CoreInput from '@Core/components/Input/CoreInput'
import { errorMsg, handleErrorFieldBackend, successMsg } from '@Core/helper/Message'
import Yup from '@Core/helper/Yup'
import { yupResolver } from '@hookform/resolvers/yup'
import { Box, Button, Typography } from '@mui/material'
import React from 'react'
import { Helmet } from 'react-helmet'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { useNavigate, useParams } from 'react-router-dom'
// import PropTypes from 'prop-types'

const ForgotPassword = props => {
	const navigate = useNavigate()
	const { type } = useParams()
	const { t } = useTranslation(TRANSLATE_CMS.login)
	const { control, handleSubmit, setError } = useForm({
		mode: 'onTouched',
		defaultValues: {
			mail_address: ''
		},
		resolver: yupResolver(
			Yup.object({
				mail_address: Yup.string()
					.trim()
					.required(t('common:validation.required'))
					.email(t('common:validation.string.valid_email'))
					.max(255, t('common:validation.string.max_length_email', { value: 255 }))
			})
		)
	})

	const onSubmit = handleSubmit(async data => {
		try {
			await forgotPasswordService.sendEmail(data, type)
			successMsg(t('common:message.send_success'))
			navigate(ROUTER_CMS.auth.forgot_password.confirm + `/${type}`)
		} catch (error) {
			if (typeof error?.response?.data?.message === 'string') {
				errorMsg(error?.response?.data?.message)
			} else {
				handleErrorFieldBackend(error, setError)
			}
		}
	})

	return (
		<div className="w-full mt-160 flex">
			<Helmet>
				<title>{t('title.forgot_password')}</title>
			</Helmet>
			<div className="m-auto w-full md:w-1/4 sm:w-1/2 text-center">
				<form onSubmit={onSubmit} className="text-center w-full block px-10 pt-10">
					<Box className="w-full">
						<Typography variant="h3" className="flex items-center whitespace-normal mb-4">
							{t('label.mail_address')}
						</Typography>
						<CoreInput
							className="py-10 flex-col"
							control={control}
							name="mail_address"
							labelStyle="mb-8"
							placeholder=""
						/>
					</Box>

					<Button
						variant="contained"
						color="primary"
						className="text-18 py-8 px-20 w-full rounded-4 font-bold mt-20"
						type="submit"
					>
						{t('btn.send_verification_code')}
					</Button>
				</form>

				<Typography
					onClick={() => navigate(`/auth/login/${type}`)}
					className="w-full mt-40 text-base underline cursor-pointer hover:text-primary"
				>
					{t('btn.login')}
				</Typography>
			</div>
		</div>
	)
}

// ForgotPassword.defaultProps = {}

// ForgotPassword.propTypes = {}

export default React.memo(ForgotPassword)
