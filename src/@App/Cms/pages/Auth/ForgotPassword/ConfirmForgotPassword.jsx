/*
 * Created Date: 12-12-2022, 9:27:48 pm
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import { ROUTER_CMS, TRANSLATE_CMS } from '@App/Cms/configs/constants'
import { forgotPasswordService } from '@App/Cms/services/forgotPasswordService'
import CoreInput from '@Core/components/Input/CoreInput'
import { errorMsg, handleErrorFieldBackend, successMsg } from '@Core/helper/Message'
import Yup from '@Core/helper/Yup'
import { yupResolver } from '@hookform/resolvers/yup'
import { LoadingButton } from '@mui/lab'
import { Button } from '@mui/material'
import React from 'react'
import { Helmet } from 'react-helmet'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { useNavigate, useParams } from 'react-router-dom'
// import PropTypes from 'prop-types'

const ConfirmForgotPassword = props => {
	const { t } = useTranslation(TRANSLATE_CMS.login)
	const { type } = useParams()
	const navigate = useNavigate()
	const {
		control,
		handleSubmit,
		formState: { isSubmitting, isDirty },
		setError
	} = useForm({
		mode: 'onTouched',
		defaultValues: {
			code: '',
			password: '',
			re_password: ''
		},
		resolver: yupResolver(
			Yup.object({
				code: Yup.string().required(t('common:validation.required')),
				password: Yup.string()
					.required(t('common:validation.required'))
					.min(8, t('common:validation.string.min_password', { value: 8 }))
					.password(),
				re_password: Yup.string()
					.required(t('common:validation.required'))
					.oneOf([Yup.ref('password')], t('common:validation.not_match'))
			})
		)
	})

	const onSubmit = handleSubmit(async data => {
		try {
			await forgotPasswordService.saveResetPassword(data, type)
			successMsg(t('common:message.send_success'))
			navigate(ROUTER_CMS.auth.login + `/${type}`)
		} catch (error) {
			console.log('============= error', error)
			if (typeof error?.response?.data?.message === 'string') {
				errorMsg(error?.response?.data?.message)
			} else {
				handleErrorFieldBackend(error, setError)
			}
		}
	})

	return (
		<div className="w-full mt-160 flex">
			<Helmet>
				<title>{t('title.forgot_password')}</title>
			</Helmet>
			<div className="m-auto w-full sm:w-1/4 text-center">
				<form onSubmit={onSubmit} className="text-center block px-10 pt-10">
					<CoreInput
						className="py-10 flex-col"
						control={control}
						name="code"
						label={t('label.authentication_code')}
						labelStyle="mb-8"
						required
						showTextRequired={false}
						// placeholder="メールアドレスを入力してください"
						placeholder=""
					/>

					<CoreInput
						className="py-10 flex-col"
						control={control}
						name="password"
						type="password"
						label={t('label.new_password')}
						labelStyle="mb-8"
						required
						showTextRequired={false}
						// placeholder="メールアドレスを入力してください"
						placeholder=""
					/>

					<CoreInput
						className="py-10 flex-col"
						control={control}
						name="re_password"
						type="password"
						label={t('label.confirm_new_password')}
						labelStyle="mb-8"
						required
						showTextRequired={false}
						// placeholder="メールアドレスを入力してください"
						placeholder=""
					/>

					<LoadingButton
						loading={isSubmitting}
						variant="contained"
						color="primary"
						className="text-18 py-8 px-20 w-full rounded-4 font-bold mt-20"
						type="submit"
					>
						{t('btn.send_verification_code')}
					</LoadingButton>
				</form>
			</div>
		</div>
	)
}

// ConfirmForgotPassword.defaultProps = {}

// ConfirmForgotPassword.propTypes = {}

export default React.memo(ConfirmForgotPassword)
