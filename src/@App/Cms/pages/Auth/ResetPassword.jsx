/*
 * Created Date: 21-12-2022, 2:50:47 pm
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import { ROUTER_CMS, TRANSLATE_CMS } from '@App/Cms/configs/constants'
import { resetPasswordService } from '@App/Cms/services/resetPasswordService'
import CoreInput from '@Core/components/Input/CoreInput'
import { errorMsg, handleErrorFieldBackend, successMsg } from '@Core/helper/Message'
import { getAuthTokenSession } from '@Core/helper/Session'
import Yup from '@Core/helper/Yup'
import { yupResolver } from '@hookform/resolvers/yup'
import { Box, Button, Typography } from '@mui/material'
import React from 'react'
import { Helmet } from 'react-helmet'
import { useForm } from 'react-hook-form'
import { useTranslation } from 'react-i18next'
import { useLocation, useNavigate, useParams } from 'react-router-dom'
// import PropTypes from 'prop-types'

const ResetPassword = props => {
	const navigate = useNavigate()
	const { type } = useParams()
	const { t } = useTranslation(TRANSLATE_CMS.login)
	const location = useLocation()

	const { control, handleSubmit, setError } = useForm({
		mode: 'onTouched',
		defaultValues: {
			password: '',
			login_id: location?.state?.token
		},
		resolver: yupResolver(
			Yup.object({
				password: Yup.string()
					.required(t('common:validation.required'))
					.password()
					.min(8, t('common:validation.string.min_password', { value: 8 }))
			})
		)
	})

	const onSubmit = handleSubmit(async data => {
		try {
			await resetPasswordService.sendPasswordReset(data, type)
			successMsg(t('message.send_success'))
			if (type !== 'admin') {
				navigate(`/cms/${type}/setting`)
			} else {
				navigate(ROUTER_CMS.auth.login + '/admin')
			}
		} catch (error) {
			if (typeof error?.response?.data?.message === 'string') {
				errorMsg(error?.response?.data?.message)
			} else {
				handleErrorFieldBackend(error, setError)
			}
		}
	})

	return (
		<div className="w-full mt-160 flex">
			<Helmet>
				<title>{t('btn.reset_password')}</title>
			</Helmet>
			<div className="m-auto w-full md:w-1/4 sm:w-1/2 text-center">
				<form onSubmit={onSubmit} className="text-center w-full block px-10 pt-10">
					<Box className="w-full">
						<Typography variant="h3" className="flex items-center whitespace-normal mb-4">
							{t('label.password')}
						</Typography>
						<CoreInput
							className="py-10 flex-col"
							control={control}
							name="password"
							type="password"
							labelStyle="mb-8"
							placeholder=""
						/>
					</Box>

					<Button
						variant="contained"
						color="primary"
						className="text-18 py-8 px-20 w-full rounded-4 font-bold mt-20"
						type="submit"
					>
						{t('btn.reset_password')}
					</Button>
				</form>
			</div>
		</div>
	)
}

// ResetPassword.defaultProps = {}

// ResetPassword.propTypes = {}

export default React.memo(ResetPassword)
