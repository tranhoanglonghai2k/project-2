import { useOptions } from '@App/Cms/hooks/useOptions'

const useLevelLabel = level_id => {
	const { levelIdOptions } = useOptions()

	const level = levelIdOptions.find(item => item.value == level_id)
	return level.label
}

export default useLevelLabel
