/*
 * Created Date: 20-12-2022, 10:04:32 am
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import { useMemo } from 'react'
import { useTranslation } from 'react-i18next'

export const useOptions = props => {
	const { t } = useTranslation('common')

	const eventWeekOptions = useMemo(
		() => [
			{
				value: 1,
				label: t('option.event_week.sunday')
			},
			{
				value: 2,
				label: t('option.event_week.monday')
			},
			{
				value: 3,
				label: t('option.event_week.tuesday')
			},
			{
				value: 4,
				label: t('option.event_week.wednesday')
			},
			{
				value: 5,
				label: t('option.event_week.thursday')
			},
			{
				value: 6,
				label: t('option.event_week.friday')
			},
			{
				value: 7,
				label: t('option.event_week.saturday')
			},
			{
				value: 8,
				label: t('option.event_week.holiday')
			}
		],
		[t]
	)

	const statusOptions = useMemo(
		() => [
			{
				value: 0,
				label: t('option.status.unreserved')
			},
			{
				value: 1,
				label: t('option.status.reserved')
			},
			{
				value: 2,
				label: t('option.status.reservation_confirm')
			},
			{
				value: 3,
				label: t('option.status.done')
			},
			{
				value: 4,
				label: t('option.status.not_done')
			}
		],
		[t]
	)

	const statusFilterOptions = useMemo(
		() => [
			{
				value: 1,
				label: t('option.status.reserved')
			},
			{
				value: 2,
				label: t('option.status.reservation_confirm')
			},
			{
				value: 3,
				label: t('option.status.done')
			},
			{
				value: 4,
				label: t('option.status.not_done')
			}
		],
		[t]
	)

	const statusTeacherOptions = [
		{
			value: 3,
			label: t('option.status.done')
		},
		{
			value: 4,
			label: t('option.status.not_done')
		}
	]

	const levelIdOptions = [
		{
			value: 1,
			label: 'Complete Beginner'
		},
		{
			value: 2,
			label: 'Beginner 1'
		},
		{
			value: 3,
			label: 'Beginner 2'
		},
		{
			value: 4,
			label: 'Beginner 3'
		},
		{
			value: 5,
			label: 'Beginner 4'
		},
		{
			value: 6,
			label: 'Pre-Intermediate'
		},
		{
			value: 7,
			label: 'Intermediate'
		},
		{
			value: 8,
			label: 'Advance'
		}
	]

	return {
		eventWeekOptions,
		statusOptions,
		statusFilterOptions,
		levelIdOptions,
		statusTeacherOptions
	}
}
