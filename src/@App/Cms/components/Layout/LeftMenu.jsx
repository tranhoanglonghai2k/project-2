/*
 * Created Date: 11-10-2022, 12:08:23 am
 * Author: Peter
 * Email: phantrung696@gmail.com
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import Cookies from 'js-cookie'
import { Box } from '@mui/system'
import React, { useState } from 'react'
import { useNavigate, useParams } from 'react-router-dom'
import Button from '@mui/material/Button'
import Dialog from '@mui/material/Dialog'
import DialogActions from '@mui/material/DialogActions'
import DialogContent from '@mui/material/DialogContent'
import DialogContentText from '@mui/material/DialogContentText'
import { Divider, List, ListItem, ListItemButton, ListItemText, Toolbar } from '@mui/material'

import LeftMenuItem from '../Menu/LeftMenuItem'
import { menuAdminConfig } from '../../configs/menuConfig'
import { authService } from '@App/Cms/services/authService'
import LeftMenuItemCollapse from '../Menu/LeftMenuItemCollapse'
import { clearSession, refreshPage } from '@Core/helper/Session'
import { errorMsg } from '@Core/helper/Message'
import { LoadingButton } from '@mui/lab'
import { useTranslation } from 'react-i18next'
import i18n from 'i18next'

const LeftMenu = props => {
	const { t } = useTranslation('common')
	const navigate = useNavigate()
	const [open, setOpen] = React.useState(false)
	const [isLoading, setIsLoading] = useState(false)
	const userLang = navigator.language || navigator.userLanguage

	const handleClickOpen = () => {
		setOpen(true)
	}
	const handleClose = () => {
		setOpen(false)
	}

	const handleLogout = async type => {
		setIsLoading(true)
		await authService.logoutAdmin(type)
		clearSession()
		// Remove all Cookie
		Object.keys(Cookies.get()).forEach(function (cookieName) {
			var neededAttributes = {}
			Cookies.remove(cookieName, neededAttributes)
		})

		i18n.changeLanguage(
			userLang?.split('-')?.shift()?.toLowerCase() === 'en' ||
				userLang?.split('-')?.shift()?.toLowerCase() === 'ja'
				? userLang?.split('-')?.shift()?.toLowerCase()
				: 'en'
		)
		navigate('/auth/login/admin')
		setIsLoading(false)
	}

	return (
		<Box>
			<Toolbar />
			<Divider />
			<List>
				{menuAdminConfig().map((item, index) => {
					if (item?.type === 'collapse') {
						return <LeftMenuItemCollapse key={index} item={item} />
					}
					return <LeftMenuItem key={index} item={item} />
				})}
				<ListItem disablePadding>
					<ListItemButton>
						<ListItemText primary={t('btn.logout')} onClick={handleClickOpen} />
					</ListItemButton>
				</ListItem>
			</List>
			<Dialog
				open={open}
				onClose={handleClose}
				aria-labelledby="alert-dialog-title"
				aria-describedby="alert-dialog-description"
			>
				<DialogContent>
					<DialogContentText id="alert-dialog-description">{t('message.confirm_logout')}</DialogContentText>
				</DialogContent>
				<DialogActions>
					<Button onClick={handleClose}>{t('btn.cancel')}</Button>
					<LoadingButton loading={isLoading} onClick={handleLogout} autoFocus>
						{t('btn.logout')}
					</LoadingButton>
				</DialogActions>
			</Dialog>
		</Box>
	)
}

//LeftMenu.defaultProps = {}

//LeftMenu.propTypes = {}

export default React.memo(LeftMenu)
