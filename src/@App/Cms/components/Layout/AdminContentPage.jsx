/*
 * Created Date: 11-10-2022, 12:29:57 am
 * Author: Peter
 * Email: phantrung696@gmail.com
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import { Box } from '@mui/system'
import React from 'react'
import PropTypes from 'prop-types'
import { Button, Card, CardContent, CardHeader, Divider, Typography } from '@mui/material'
import { Helmet } from 'react-helmet'

const AdminContentPage = props => {
	const { content, pageTitle, headerAction, tabHeader } = props
	return (
		<Card className="shadow-4 h-full">
			{pageTitle && (
				<>
					<Helmet>
						<title>{pageTitle}</title>
					</Helmet>
					<CardHeader
						title={
							<div className="flex">
								<Typography variant="h1" className="font-900">
									{pageTitle}
								</Typography>
							</div>
						}
						action={headerAction}
					/>
				</>
			)}
			<Box>{tabHeader}</Box>
			<Divider />
			<Box className="my-8">{content}</Box>
		</Card>
	)
}

//AdminContentPage.defaultProps = {}

AdminContentPage.propTypes = {
	content: PropTypes.any,
	pageTitle: PropTypes.any,
	headerAction: PropTypes.any,
	tabHeader: PropTypes.any
}

export default React.memo(AdminContentPage)
