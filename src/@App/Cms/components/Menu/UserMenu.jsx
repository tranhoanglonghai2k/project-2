/*
 * Created Date: 18-12-2022, 9:10:58 pm
 * Author: DESKTOP-SRKKG3M\Hello Keys
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */
import { ROUTER_STUDENT, TRANSLATE_CMS } from '@App/Cms/configs/constants'
import React, { useState } from 'react'
import { makeStyles } from '@mui/styles'
import { Dialog, DialogActions, DialogContent, DialogContentText, Divider, List, ListItem } from '@mui/material'
import Avatar from '@mui/material/Avatar'
import Button from '@mui/material/Button'
import ListItemText from '@mui/material/ListItemText'
import Popover from '@mui/material/Popover'
import Typography from '@mui/material/Typography'
import { useBoolean } from 'ahooks'
import { useTranslation } from 'react-i18next'
import { useNavigate } from 'react-router-dom'
import { clearSession, getAuthTokenSession } from '@Core/helper/Session'
import { authService } from '@App/Cms/services/authService'
import { errorMsg, successMsg } from '@Core/helper/Message'
import { LoadingButton } from '@mui/lab'

// import PropTypes from 'prop-types'

const useStyles = makeStyles(theme => ({
	root: {
		maxWidth: 200,
		// width: '100%',
		'& .list-item-icon': {
			width: 6,
			height: 6,
			background: theme.palette.primary.main,
			borderRadius: '100%',
			// alignSelf: 'flex-start',
			marginTop: 12
		}
	}
}))

const UserMenu = props => {
	const { t } = useTranslation(TRANSLATE_CMS.lesson_list_student)
	const menus = [
		{
			title: t('menu.student_info'),
			alias: 'info',
			path: ROUTER_STUDENT.info_account
		},
		{
			title: t('menu.list_of_lessons'),
			alias: 'schedular',
			path: ROUTER_STUDENT.schedule_calendar
		},
		{
			title: t('menu.list_of_reserved_lessons'),
			alias: 'reservation',
			path: ROUTER_STUDENT.lesson_list_student
		},
		{
			title: t('menu.inquiry'),
			alias: 'request',
			path: ROUTER_STUDENT.request_form
		},
		{
			title: t('menu.setting'),
			alias: 'setting',
			path: ROUTER_STUDENT.setting
		},
		{
			title: t('menu.logout'),
			alias: 'logout'
		}
	]
	const classes = useStyles()
	const [userMenu, setUserMenu] = useState(null)
	const navigate = useNavigate()
	const infoAccount = getAuthTokenSession()
	const [open, setOpen] = useState(false)
	const [isLoading, setIsLoading] = useState(false)
	const handleClickOpen = () => {
		setOpen(true)
	}
	const handleClose = () => {
		setOpen(false)
	}

	const userMenuClick = event => {
		setUserMenu(event.currentTarget)
	}

	const userMenuClose = () => {
		setUserMenu(null)
	}

	const handleLogout = async () => {
		setIsLoading(true)
		await authService.logout('user')
		clearSession()
		navigate('/auth/login/user')
		handleClose()
		successMsg(t('common:message.logout_success'))
		setIsLoading(false)
	}

	return (
		<>
			<Button className="px-0 py-0 min-h-40 min-w-40 md:px-16 md:py-6 ml-auto" onClick={userMenuClick}>
				<Avatar className="md:mx-4" alt="user photo" />
				<div className="flex-col items-end hidden mx-4 md:flex">
					<Typography component="span" className="flex font-bold">
						{infoAccount?.account?.name}
					</Typography>
					<Typography className="lowercase text-11" color="textSecondary">
						{infoAccount?.account?.mail_address}
					</Typography>
				</div>
			</Button>
			<Popover
				open={Boolean(userMenu)}
				anchorEl={userMenu}
				onClose={userMenuClose}
				anchorOrigin={{
					vertical: 'bottom',
					horizontal: 'center'
				}}
				transformOrigin={{
					vertical: 'top',
					horizontal: 'center'
				}}
				classes={{
					paper: classes.root
				}}
			>
				<List className="p-0 list-notif" component="nav" aria-label="main mailbox folders">
					{menus.map((item, index) => {
						return (
							<React.Fragment key={index}>
								<Divider component="li" />
								<ListItem
									button
									onClick={() => {
										userMenuClose()
										if (item?.alias === 'logout') {
											handleClickOpen()
										} else {
											navigate(item?.path)
										}
									}}
									alignItems="flex-start"
								>
									<div className="mx-8 list-item-icon" />
									<ListItemText primary={item.title} />
								</ListItem>
							</React.Fragment>
						)
					})}
					<Divider component="li" />
				</List>
			</Popover>
			<Dialog
				open={open}
				onClose={handleClose}
				aria-labelledby="alert-dialog-title"
				aria-describedby="alert-dialog-description"
			>
				<DialogContent>
					<DialogContentText id="alert-dialog-description">{t('message.sure_logout')}</DialogContentText>
				</DialogContent>
				<DialogActions>
					<Button onClick={handleClose}>{t('common:btn.cancel')}</Button>
					<LoadingButton loading={isLoading} onClick={handleLogout} autoFocus>
						{t('common:btn.logout')}
					</LoadingButton>
				</DialogActions>
			</Dialog>
		</>
	)
}

//UserMenu.defaultProps = {}

//UserMenu.propTypes = {}

export default React.memo(UserMenu)
