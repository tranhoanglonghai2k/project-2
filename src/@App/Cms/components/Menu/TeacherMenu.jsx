/*
 * Created Date: 23-12-2022, 10:17:45 am
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import { ROUTER_TEACHER, TRANSLATE_CMS } from '@App/Cms/configs/constants'
import { authService } from '@App/Cms/services/authService'
import { errorMsg, successMsg } from '@Core/helper/Message'
import { clearSession, getAuthTokenSession, refreshPage } from '@Core/helper/Session'
import { LoadingButton } from '@mui/lab'
import {
	Avatar,
	Button,
	Dialog,
	DialogActions,
	DialogContent,
	DialogContentText,
	Divider,
	List,
	ListItem,
	ListItemText,
	Popover,
	Typography
} from '@mui/material'
import { makeStyles } from '@mui/styles'
import React, { useState } from 'react'
import { useTranslation } from 'react-i18next'
import { useNavigate } from 'react-router-dom'
import i18n from 'i18next'
// import PropTypes from 'prop-types'

const useStyles = makeStyles(theme => ({
	root: {
		maxWidth: 200,
		// width: '100%',
		'& .list-item-icon': {
			width: 6,
			height: 6,
			background: theme.palette.primary.main,
			borderRadius: '100%',
			// alignSelf: 'flex-start',
			marginTop: 12
		}
	}
}))

const TeacherMenu = props => {
	const { t } = useTranslation(TRANSLATE_CMS.lesson_teacher)
	const userLang = navigator.language || navigator.userLanguage

	const menus = [
		{
			title: t('menu.teacher_info'),
			alias: 'info',
			path: ROUTER_TEACHER.info_account
		},
		{
			title: t('menu.list_of_lessons_in_charge'),
			alias: 'schedular',
			path: ROUTER_TEACHER.schedule_calendar
		},
		{
			title: t('menu.list_of_reserved_lessons'),
			alias: 'reservation',
			path: ROUTER_TEACHER.lesson_teacher
		},
		{
			title: t('menu.inquiry'),
			alias: 'request',
			path: ROUTER_TEACHER.request_form
		},
		// {
		// 	title: t('menu.setting'),
		// 	alias: 'setting',
		// 	path: ROUTER_TEACHER.setting
		// },
		{
			title: t('menu.logout'),
			alias: 'logout'
		}
	]
	const classes = useStyles()
	const [teacherMenu, setTeacherMenu] = useState(null)
	const navigate = useNavigate()
	const infoAccount = getAuthTokenSession()
	const [open, setOpen] = useState(false)
	const [isLoading, setIsLoading] = useState(false)

	const handleClickOpen = () => {
		setOpen(true)
	}
	const handleClose = () => {
		setOpen(false)
	}

	const teacherMenuClick = event => {
		setTeacherMenu(event.currentTarget)
	}

	const teacherMenuClose = () => {
		setTeacherMenu(null)
	}

	const handleLogout = async () => {
		setIsLoading(true)
		await authService.logout('teacher')
		clearSession()
		handleClose()
		i18n.changeLanguage(
			userLang?.split('-')?.shift()?.toLowerCase() === 'en' ||
				userLang?.split('-')?.shift()?.toLowerCase() === 'ja'
				? userLang?.split('-')?.shift()?.toLowerCase()
				: 'en'
		)
		navigate('/auth/login/teacher')
		successMsg(t('common:message.logout_success'))
		setIsLoading(false)
	}

	return (
		<>
			<Button className="px-0 py-0 min-h-40 min-w-40 md:px-16 md:py-6 ml-auto" onClick={teacherMenuClick}>
				<Avatar className="md:mx-4" alt="user photo" />
				<div className="flex-col items-end hidden mx-4 md:flex">
					<Typography component="span" className="flex font-bold">
						{infoAccount?.account?.name}
					</Typography>
					<Typography className="lowercase text-11" color="textSecondary">
						{infoAccount?.account?.mail_address}
					</Typography>
				</div>
			</Button>
			<Popover
				open={Boolean(teacherMenu)}
				anchorEl={teacherMenu}
				onClose={teacherMenuClose}
				anchorOrigin={{
					vertical: 'bottom',
					horizontal: 'center'
				}}
				transformOrigin={{
					vertical: 'top',
					horizontal: 'center'
				}}
				classes={{
					paper: classes.root
				}}
			>
				<List className="p-0 list-notif" component="nav" aria-label="main mailbox folders">
					{menus.map((item, index) => {
						return (
							<React.Fragment key={index}>
								<Divider component="li" />
								<ListItem
									button
									onClick={() => {
										teacherMenuClose()
										if (item?.alias === 'logout') {
											handleClickOpen()
										} else {
											navigate(item?.path)
										}
									}}
									alignItems="flex-start"
								>
									<div className="mx-8 list-item-icon" />
									<ListItemText primary={item.title} />
								</ListItem>
							</React.Fragment>
						)
					})}
					<Divider component="li" />
				</List>
			</Popover>
			<Dialog
				open={open}
				onClose={handleClose}
				aria-labelledby="alert-dialog-title"
				aria-describedby="alert-dialog-description"
			>
				<DialogContent>
					<DialogContentText id="alert-dialog-description">
						{t('common:message.confirm_logout')}
					</DialogContentText>
				</DialogContent>
				<DialogActions>
					<Button onClick={handleClose}>{t('common:btn.cancel')}</Button>
					<LoadingButton loading={isLoading} onClick={handleLogout} autoFocus>
						{t('common:btn.logout')}
					</LoadingButton>
				</DialogActions>
			</Dialog>
		</>
	)
}

// TeacherMenu.defaultProps = {}

// TeacherMenu.propTypes = {}

export default React.memo(TeacherMenu)
