/*
 * Created Date: 11-10-2022, 12:22:02 am
 * Author: Peter
 * Email: phantrung696@gmail.com
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import { useTranslation } from 'react-i18next'
import { ROUTER_EXE, ROUTER_STUDENT, ROUTER_TEACHER } from './constants'

export const menuAdminConfig = () => {
	const { t } = useTranslation('common')

	return [
		// {
		// 	title: '生徒側',
		// 	type: 'collapse',
		// 	children: [
		// 		{
		// 			title: '受講可能レッスン',
		// 			url: ROUTER_STUDENT.schedule_calendar
		// 		},
		// 		{
		// 			title: '生徒設定',
		// 			url: ROUTER_STUDENT.setting
		// 		},
		// 		{
		// 			title: 'サービス名',
		// 			url: ROUTER_STUDENT.lesson_list_student
		// 		}
		// 	]
		// },
		// {
		// 	title: '講師側',
		// 	type: 'collapse',
		// 	children: [
		// 		{
		// 			title: '受講可能レッスン',
		// 			url: ROUTER_TEACHER.schedule_calendar
		// 		},
		// 		{
		// 			title: '講師設定',
		// 			url: ROUTER_TEACHER.setting
		// 		},
		// 		{
		// 			title: 'サービス名',
		// 			url: ROUTER_TEACHER.lesson_teacher
		// 		}
		// 	]
		// },
		// {
		// 	title: '事務局側',
		// 	type: 'collapse',
		// 	children: [
		{
			title: t('title.lesson'),
			url: ROUTER_EXE.lesson.list
		},
		{
			title: t('title.student'),
			url: ROUTER_EXE.student.list
		},
		{
			title: t('title.reserve_lesson'),
			url: ROUTER_EXE.reserve_lesson.list
		},
		{
			title: t('title.executive_account'),
			url: ROUTER_EXE.executive_account.list
		},
		{
			title: t('title.classroom'),
			url: ROUTER_EXE.classroom.list
		},
		{
			title: t('title.teacher'),
			url: ROUTER_EXE.teacher.list
		}
		// 	]
		// }
	]
}
