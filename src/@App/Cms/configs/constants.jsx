/*
 * Created Date: 11-10-2022, 12:21:48 am
 * Author: Peter
 * Email: phantrung696@gmail.com
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

const basePath = ''
const basePathTran = '/admin'
const basePathStudent = '/user'
const basePathTeacher = '/teacher'
const basePathExe = '/admin'

export const TRANSLATE_CMS = {
	schedule_calendar: basePathTran + '/schedule-calendar',
	setting: basePathTran + '/setting',
	lesson: basePathTran + '/lesson',
	student: basePathTran + '/student',
	reserve_lesson: basePathTran + '/reserve-lesson',
	executive_account: basePathTran + '/executive-account',
	classroom: basePathTran + '/classroom',
	teacher: basePathTran + '/teacher',
	lesson_list_student: basePathTran + '/lesson-list-student',
	login: basePathTran + '/login',
	lesson_teacher: basePathTran + '/lesson-teacher',
	info_account: basePathTran + '/info-account'
}

export const ROUTER_CMS = {
	choose_type: basePath + '/choose-type',
	auth: {
		login: basePath + '/auth/login',
		reset_password: basePath + '/auth/reset-password',
		forgot_password: {
			forgot: basePath + '/auth/forgot-password',
			confirm: basePath + '/auth/forgot-password/confirm'
		}
	},
	schedule_calendar: 'schedule-calendar',
	setting: 'setting'
}

export const ROUTER_STUDENT = {
	schedule_calendar: basePathStudent + '/schedule-calendar',
	setting: basePathStudent + '/setting',
	lesson_list_student: basePathStudent + '/lesson-list-student',
	request_form: basePathStudent + '/form',
	info_account: basePathStudent + '/info-account'
}

export const ROUTER_TEACHER = {
	schedule_calendar: basePathTeacher + '/schedule-calendar',
	setting: basePathTeacher + '/setting',
	lesson_teacher: basePathTeacher + '/lesson-teacher',
	request_form: basePathTeacher + '/form',
	info_account: basePathTeacher + '/info-account'
}

export const ROUTER_EXE = {
	auth: {
		login: basePathExe + '/login'
	},
	lesson: {
		list: basePathExe + '/lesson'
	},
	student: {
		list: basePathExe + '/student'
	},
	reserve_lesson: {
		list: basePathExe + '/reserve-lesson'
	},
	executive_account: {
		list: basePathExe + '/executive-account'
	},
	classroom: {
		list: basePathExe + '/classroom'
	},
	teacher: {
		list: basePathExe + '/teacher'
	}
}
