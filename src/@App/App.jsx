/*
 * Created Date: 06-10-2022, 9:46:59 pm
 * Author: Peter
 * Email: phantrung696@gmail.com
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import React from 'react'
import CoreAppTheme from '../@Core/components/CoreAppTheme'
// import PropTypes from 'prop-types'
import { createBrowserRouter, RouterProvider, Route } from 'react-router-dom'
import { appRouterConfig } from './appConfig'
import { CoreConfirmProvider } from '@Core/components/Confirm/CoreConfirm'
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import 'react-datepicker/dist/react-datepicker.css'
import withAppProviders from './withAppProviders'
import CoreAuthProvider from '@Core/Provider/CoreAuthProvider'
import { CircularProgress } from '@mui/material'
const App = props => {
	return (
		// <CoreAuthProvider>
		<CoreAppTheme>
			<CoreConfirmProvider>
				<React.Suspense
					fallback={
						<div className="mt-200 text-center">
							<CircularProgress />
						</div>
					}
				>
					<RouterProvider router={appRouterConfig} />
				</React.Suspense>
			</CoreConfirmProvider>
			<ToastContainer />
		</CoreAppTheme>
		// </CoreAuthProvider>
	)
}

//App.defaultProps = {}

//App.propTypes = {}

export default App
