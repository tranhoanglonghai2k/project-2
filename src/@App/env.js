/*
 * Created Date: 11-10-2022, 8:45:53 pm
 * Author: Peter
 * Email: phantrung696@gmail.com
 * -----
 * Last Modified: Fri Dec 30 2022
 * Modified By: haitran
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import { isLocalhost } from '@Core/helper/Url'

export const env = {
	CMS_BASE_URL: import.meta.env.VITE_BE_BASE_URL
}
