/*
 * Created Date: 19-12-2022, 10:55:35 pm
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import { authService } from '@App/Cms/services/authService'
import { errorMsg, successMsg } from '@Core/helper/Message'
import { getAuthTokenSession, setAuthTokenSession } from '@Core/helper/Session'
import React, { createContext, useContext, useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { useTranslation } from 'react-i18next'

const CoreAuthContext = createContext()

export const useCoreAuth = () => useContext(CoreAuthContext)

const CoreAuthProvider = props => {
	const { t } = useTranslation('common')

	const loginByAdmin = async data => {
		try {
			const res = await authService.loginAdmin(data)
			setAuthTokenSession(res)
			successMsg(t('message.login_success'))
		} catch (e) {
			console.log('============= e', e)
			errorMsg(null, t('message.login_failed'))
		}
	}

	return <CoreAuthContext.Provider value={props}>{props.children}</CoreAuthContext.Provider>
}

// CoreAuthProvider.defaultProps = {}

CoreAuthProvider.propTypes = {
	children: PropTypes.node.isRequired
}

export default React.memo(CoreAuthProvider)
