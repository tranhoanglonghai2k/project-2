/*
 * Created Date: 16-08-2022, 2:14:11 am
 * Author: Peter
 * Email: phantrung696@gmail.com
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PT CORP, Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import { getAuthTokenSession } from '@Core/helper/Session'

const excludeAuthenApi = []
const authToken = async requestConfig => {
	let authToken = getAuthTokenSession()
	const { url, notAuth } = requestConfig
	if (excludeAuthenApi.includes(url) || notAuth) {
		return requestConfig
	}
	requestConfig.headers.Authorization = `Bearer ${authToken?.token}`

	// requestConfig.headers.Authorization = `Bearer eyJpdiI6InpGK3BNaWM1ZVdqMk1pdXpXaE0zR0E9PSIsInZhbHVlIjoicUhyU3ZhU0JtZ3F0bDhMSktES2I4UT09IiwibWFjIjoiOWYxN2E1YmFlOGE4MzY1NzBhOWY2MTg0YWM5OTM4ZThlYjE0NGVmNDI4Nzg3YTY3MTAwMDgxN2VmNzMwMmZkMCIsInRhZyI6IiJ9`

	return requestConfig
}

export const globalApiMiddleware = {
	auth: authToken
}
