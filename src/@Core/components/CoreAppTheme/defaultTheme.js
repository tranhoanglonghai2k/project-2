/*
 * Created Date: 06-10-2022, 9:57:20 pm
 * Author: Peter
 * Email: phantrung696@gmail.com
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */
import { createTheme } from '@mui/material/styles'
import { lightBlue, red } from '@mui/material/colors'

// Create a theme instance.
const defaultTheme = createTheme({
	palette: {
		primary: {
			main: '#a81316'
		},
		secondary: {
			main: '#f44336'
		},
		third: {
			main: '#E7E7D6'
		},
		accent: {
			main: '#FF4E02'
		},
		mission: {
			main: '#FF7438'
		},
		error: {
			main: '#E50000'
		},
		background: {
			default: '#F8F9F9'
		},
		text: {
			primary: '#1E222E'
		}
	},
	status: {
		danger: 'orange'
	},
	typography: {
		fontFamily: ['Noto Sans', 'sans-serif'].join(','),
		fontWeightLight: 300,
		fontWeightRegular: 400,
		fontWeightMedium: 500,
		htmlFontSize: 10,
		fontSize: 13,
		h1: {
			fontSize: '2.2rem',
			fontWeight: 700
		},
		h2: {
			fontSize: '2rem',
			fontWeight: 700
		},
		h3: {
			fontSize: '1.8rem',
			fontWeight: 700
		},
		h4: {
			fontSize: '1.6rem',
			fontWeight: 700
		},
		subtitle1: {
			fontSize: '1.5rem',
			fontWeight: 700
		},
		subtitle2: {
			fontSize: '1.4rem',
			fontWeight: 700
		},
		body1: {
			fontSize: '1.3rem'
		},
		body2: {
			fontSize: '1.4rem',
			fontWeight: 700
		},
		caption: {
			fontSize: '1.2rem'
		}
	},
	components: {
		// MuiTypography: {
		// 	styleOverrides: {
		// 		root: {
		// 			margin: 'unset'
		// 		}
		// 	}
		// }
	}
})

export default defaultTheme
