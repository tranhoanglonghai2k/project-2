/*
 * Created Date: 19-12-2022, 10:53:46 pm
 * Author: Hai Tran
 * Email: you@you.you
 * -----
 * Last Modified:
 * Modified By:
 * -----
 * Copyright (c) 2022 PROS+ Group , Inc
 * -----
 * HISTORY:
 * Date      	By	Comments
 * ----------	---	----------------------------------------------------------
 */

import { redirect } from 'react-router-dom'

export const SESSION_STORAGE = 'session'
export const LOCAL_STORAGE = 'local'
export function setDataSession(type, key, data) {
	if (typeof Storage !== 'undefined') {
		if (!key) return false
		// process data
		// save to storegae
		data = JSON.stringify(data)
		if (type === SESSION_STORAGE) {
			sessionStorage.setItem(key, data)
			return true
		}

		if (type === LOCAL_STORAGE) {
			localStorage.setItem(key, data)
			return true
		}
	}
	return null
	// console.log('This Browser dont supported storeage');
}

export function getDataSession(type, key) {
	if (typeof Storage !== 'undefined') {
		// process data
		let value = ''
		let data = ''
		if (type === SESSION_STORAGE) {
			value = sessionStorage.getItem(key)
		}

		if (type === LOCAL_STORAGE) {
			value = localStorage.getItem(key)
		}

		try {
			data = JSON.parse(value) || null
		} catch (err) {
			data = value
		}
		return data
	}
	// console.log('This browser does not support local storage');
	return null
}

export function removeDataSession(type, key) {
	if (typeof Storage !== 'undefined') {
		if (type === SESSION_STORAGE) {
			sessionStorage.removeItem(key)
		}
		if (type === LOCAL_STORAGE) {
			localStorage.removeItem(key)
		}
	}
	// console.log('This browser does not support local storage');
}

export const setAuthTokenSession = data => {
	setDataSession(LOCAL_STORAGE, 'authToken', data)
}

export const getAuthTokenSession = () => {
	return getDataSession(LOCAL_STORAGE, 'authToken') ?? null
}

export const setLangSession = data => {
	setDataSession(LOCAL_STORAGE, 'lang', data)
}

export const getLangSession = () => {
	return getDataSession(LOCAL_STORAGE, 'lang') ?? null
}

export const clearSession = () => {
	localStorage.clear()
	sessionStorage.clear()
}

export const refreshPage = () => {
	window.location.reload(false)
}

export const isUserLogin = () => {
	const user = getAuthTokenSession()
	if (user === null) {
		throw redirect('/cms')
	}
}
